﻿using System.Globalization;
using System.IO;
using System.Linq;
using ITHoot.Application.Core.Infrastructure;
using ITHoot.Application.Core.Settings;
using ITHoot.Application.Helpers;
using ITHoot.Application.Localization.Queries;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Options;

namespace ITHoot.Web.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void ConfigureRequestPipeline(this IApplicationBuilder application)
        {
            EngineContext.Current.ConfigureRequestPipeline(application);
        }

        /*public static void StartEngine(this IApplicationBuilder application)
        {
            //var engine = EngineContext.Current;

            //engine.Resolve<ILogger>().LogInformation("Application started");
        }*/

        public static void UseITHootStaticFiles(this IApplicationBuilder application)
        {
            var appSettings = EngineContext.Current.Resolve<AppSettings>();
            var currentPath = Directory.GetCurrentDirectory();
            var imagesFolderPath = Path.Combine(currentPath, "data", appSettings.ImagesFolder);
            Directory.CreateDirectory(imagesFolderPath);

            application.UseStaticFiles();
            application.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(imagesFolderPath),
                RequestPath = new PathString(MediaDefaults.ImagesRelativePath)
            });
        }

        /*/// <summary>
        ///     Configure middleware checking whether requested page is keep alive page
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public static void UseITHootKeepAlive(this IApplicationBuilder application)
        {
            //application.UseMiddleware<KeepAliveMiddleware>();
        }*/
        
        /// <summary>
        ///     Configure the request localization feature
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public static void UseITHootRequestLocalization(this IApplicationBuilder application)
        {
            application.UseRequestLocalization(async options =>
            {
                var mediator = EngineContext.Current.Resolve<IMediator>();

                var allLanguages = await mediator.Send(new GetAllLanguagesQuery());

                var cultures = allLanguages
                    .OrderBy(r => r.DisplayOrder)
                    .Select(r => new CultureInfo(r.LanguageCulture))
                    .ToList();

                options.SupportedCultures = cultures;
                options.DefaultRequestCulture = new RequestCulture(cultures.First());
                options.RequestCultureProviders.Clear();
                options.RequestCultureProviders.Add(new AcceptLanguageHeaderRequestCultureProvider());
                //options.ApplyCurrentCultureToResponseHeaders = true;
            });
        }

        public static void UseITHootSwagger(this IApplicationBuilder application)
        {
            var swaggerSettings = application.ApplicationServices.GetService<IOptions<SwaggerSettings>>().Value;
            
            application.UseSwagger(options => options.RouteTemplate = swaggerSettings.JsonRoute);
            
            application.UseSwaggerUI(options =>
                options.SwaggerEndpoint(swaggerSettings.UIEndpoint, swaggerSettings.Description));
        }
    }
}