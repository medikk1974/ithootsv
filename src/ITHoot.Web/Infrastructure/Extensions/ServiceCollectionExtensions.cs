﻿using ITHoot.Application.Core.Infrastructure;
using ITHoot.Application.Core.Infrastructure.Interfaces;
using ITHoot.Application.Core.Settings;
using ITHoot.Application.Driver.Services;
using ITHoot.Application.Driver.Services.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.Identity.Services;
using ITHoot.Application.Identity.Services.Interfaces;
using ITHoot.Application.User.Manager;
using ITHoot.Application.Vehicle.Services;
using ITHoot.Application.Vehicle.Services.Interfaces;
using ITHoot.EFContext;
using ITHoot.Models.Entities;
using ITHoot.Web.Infrastructure.Middlewares;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using ITHoot.Application.Driver.Hubs;

namespace ITHoot.Web.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        ///     Add services to the application and configure service provider
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration of the application</param>
        /// <param name="webHostEnvironment">Hosting environment</param>
        /// <returns>Configured engine and app settings</returns>
        public static (IEngine, AppSettings) ConfigureApplicationServices(this IServiceCollection services,
            IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            //let the operating system decide what TLS protocol version to use
            //see https://docs.microsoft.com/dotnet/framework/network-programming/tls
            ServicePointManager.SecurityProtocol = SecurityProtocolType.SystemDefault;

            //create default file provider
            CommonHelper.DefaultFileProvider = new ITHootFileProvider(webHostEnvironment);

            //add accessor to HttpContext
            services.AddHttpContextAccessor();

            services.AddTransient<IAppUserManager, AppUserManager>();

            var appSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSection);
            var appSettings = appSection.Get<AppSettings>();
            services.AddSingleton(appSettings);

            var jwtSettingsSection = configuration.GetSection("JwtSettings");
            services.Configure<JwtSettings>(jwtSettingsSection);
            var jwtSettings = jwtSettingsSection.Get<JwtSettings>();
            services.AddSingleton(jwtSettings);

            var swaggerSettingsSection = configuration.GetSection("SwaggerSettings");
            services.Configure<SwaggerSettings>(swaggerSettingsSection);
            var swaggerSettingsSettings = swaggerSettingsSection.Get<SwaggerSettings>();
            services.AddSingleton(swaggerSettingsSettings);

            var emailSenderSection = configuration.GetSection("EmailSender");
            services.Configure<EmailSenderSettings>(emailSenderSection);
            var emailSenderSettings = emailSenderSection.Get<EmailSenderSettings>();
            services.AddSingleton(emailSenderSettings);

            var smsSenderSection = configuration.GetSection("SMSSender");
            services.Configure<SMSSenderSettings>(smsSenderSection);
            var smsSenderSettings = smsSenderSection.Get<SMSSenderSettings>();
            services.AddSingleton(smsSenderSettings);

            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<IDriverService, DriverService>();
            services.AddTransient<IVehicleService, VehicleService>();

            //create engine and configure service provider
            var engine = EngineContext.Create();

            engine.ConfigureServices(services, configuration);

            return (engine, appSettings);
        }

        /// <summary>
        ///     Register HttpContextAccessor
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        public static void AddHttpContextAccessor(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        /// <summary>
        ///     Adds services required for application session state
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        public static void AddHttpSession(this IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.Cookie.Name = "ITHoot.Session";
                options.Cookie.HttpOnly = true;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            });
        }

        public static void AddITHootDistributedCache(this IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
        }

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "ITHoot API", Version = "v1" });
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the bearer scheme",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new List<string>()
                    }
                });
                options.DocumentFilter<SignalRSwaggerGen.SignalRSwaggerGen>(new List<Assembly> { typeof(DriverHub).Assembly });
            });
        }
        
        public static void ConfigureIdentity(this IServiceCollection services)
        {
            services.AddIdentity<User, IdentityRole<int>>(options =>
                {
                    options.Password.RequiredLength = 6;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    //options.SignIn.RequireConfirmedAccount = true;
                    options.Tokens.EmailConfirmationTokenProvider = TokenOptions.DefaultPhoneProvider;
                    //options.Stores.ProtectPersonalData = true;
                    //options.Stores.MaxLengthForKeys = 128;
                })
                .AddErrorDescriber<IdentityErrorDescriber>()
                .AddEntityFrameworkStores<EFDbContext>()
                .AddDefaultTokenProviders();
        }

        public static void ConfigureJwtAuthentication(this IServiceCollection services, byte[] key)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                //LifetimeValidator = (before, expires, token, param) => expires > DateTime.UtcNow
                ValidateLifetime = true,
                RequireExpirationTime = false,
                ClockSkew = TimeSpan.Zero
            };

            services.AddSingleton(tokenValidationParameters);

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = AccessTokenDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = AccessTokenDefaults.AuthenticationScheme;
                    x.DefaultScheme = AccessTokenDefaults.AuthenticationScheme;
                })
                .AddScheme<AuthenticationSchemeOptions, AccessTokenAuthenticationHandler>(
                    AccessTokenDefaults.AuthenticationScheme, _ => { });

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.TokenValidationParameters = tokenValidationParameters;
                });
        }
    }
}