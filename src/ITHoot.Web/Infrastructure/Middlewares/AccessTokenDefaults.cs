﻿namespace ITHoot.Web.Infrastructure.Middlewares
{
    public class AccessTokenDefaults
    {
        public const string AuthenticationScheme = "AccessToken";
    }
}
