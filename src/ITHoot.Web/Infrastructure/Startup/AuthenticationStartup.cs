﻿using System.Text;
using ITHoot.Application.Core.Infrastructure.Interfaces;
using ITHoot.Application.Core.Settings;
using ITHoot.Web.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ITHoot.Web.Infrastructure.Startup
{
    public class AuthenticationStartup : IITHootStartup
    {
        /// <summary>
        ///     Gets order of this startup configuration implementation
        /// </summary>
        public int Order => 500;

        /// <summary>
        ///     Add and configure any of the middleware
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration of the application</param>
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureIdentity();

            var jwtSettings = new JwtSettings();
            configuration.Bind(nameof(JwtSettings), jwtSettings);

            services.ConfigureJwtAuthentication(Encoding.ASCII.GetBytes(jwtSettings.SecretKey));
        }

        /// <summary>
        ///     Configure the using of added middleware
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseAuthentication();
        }
    }
}