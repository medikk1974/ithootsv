﻿using ITHoot.Application.Core.Infrastructure.Interfaces;
using ITHoot.Models;
using ITHoot.Web.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ITHoot.Web.Infrastructure.Startup
{
    public class AuthorizationStartup : IITHootStartup
    {
        public int Order => 600; // Authorization should be loaded before Endpoint and after authentication

        /// <summary>
        ///     Add and configure any of the middleware
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration of the application</param>
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicyAccessTokenRole(Constants.Policy.AccessTokenCustomer, Enums.RoleTypes.Customer);

                options.AddPolicyJwtRole(Constants.Policy.Admin, Enums.RoleTypes.Admin);
                options.AddPolicyJwtRole(Constants.Policy.Driver, Enums.RoleTypes.Driver);
                options.AddPolicyJwtRole(Constants.Policy.NaturalPerson, Enums.RoleTypes.NaturalPerson);
                options.AddPolicyJwtRole(Constants.Policy.Company, Enums.RoleTypes.Company);
                options.AddPolicyJwtRole(Constants.Policy.Customer, Enums.RoleTypes.Customer);

                options.AddPolicyJwtAnyRole(Constants.Policy.Carrier, Enums.RoleTypes.Company, Enums.RoleTypes.NaturalPerson);
            });

            //services.AddScoped<IAuthorizationHandler, PolicyAuthorizationHandler>();
        }

        /// <summary>
        ///     Configure the using of added middleware
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            application.UseAuthorization();
        }
    }
}