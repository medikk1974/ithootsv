﻿using ITHoot.Application.Core.Infrastructure.Interfaces;
using ITHoot.Web.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ITHoot.Web.Infrastructure.Startup
{
    public class CommonStartup : IITHootStartup
    {
        /// <summary>
        ///     Gets order of this startup configuration implementation
        /// </summary>
        public int Order => 100; //common services should be loaded after error handlers

        /// <summary>
        ///     Add and configure any of the middleware
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="configuration">Configuration of the application</param>
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //services.AddResponseCompression();

            services.AddOptions();

            services.AddITHootDistributedCache();

            services.AddHttpSession();

            services.AddLocalization();

            services.AddLogging();

            services.AddRouting();
        }

        /// <summary>
        ///     Configure the using of added middleware
        /// </summary>
        /// <param name="application">Builder for configuring an application's request pipeline</param>
        public void Configure(IApplicationBuilder application)
        {
            //application.UseResponseCompression();

            application.UseITHootStaticFiles();

            application.UseSession();

            application.UseITHootRequestLocalization();
        }
    }
}