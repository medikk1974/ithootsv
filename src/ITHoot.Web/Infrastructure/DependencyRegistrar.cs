﻿using ITHoot.Application;
using ITHoot.Application.Core.Infrastructure.Interfaces;
using ITHoot.Application.Core.Settings;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace ITHoot.Web.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 0;

        /// <summary>
        ///     Register services and interfaces
        /// </summary>
        /// <param name="services">Collection of service descriptors</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="appSettings">App settings</param>
        public virtual void Register(IServiceCollection services, ITypeFinder typeFinder, AppSettings appSettings)
        {
            services.AddScoped<IWorkContext, WebWorkContext>();

            //slug route transformer
            //services.AddScoped<SlugRouteTransformer>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
        }
    }
}