﻿using System;
using System.Collections.Generic;
using System.Linq;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ITHoot.Application.Driver.Queries;
using ITHoot.Application.OrderRequest.Commands;
using ITHoot.Application.OrderRequest.Queries;
using ITHoot.Models.Entities;
using ITHoot.Repository;
using ITHoot.Web.Contracts.V1.Responses.OrderRequest;

namespace ITHoot.Web.Controllers.V1
{
    public class OrderRequestController : BaseAuthApiController
    {
        private readonly IRepository<OrderRequest, int> _orderRequestRepository;
        private readonly IRepository<Customer, int> _customerRepository;
        private readonly IRepository<Vehicle, int> _vehicleRepository;

        public OrderRequestController(IRepository<OrderRequest, int> orderRequestRepository, IRepository<Customer, int> customerRepository, IRepository<Vehicle, int> vehicleRepository)
        {
            _orderRequestRepository = orderRequestRepository;
            _customerRepository = customerRepository;
            _vehicleRepository = vehicleRepository;
        }

        [Authorize(Policy = Constants.Policy.Driver)]
        [HttpGet(ApiRoutes.OrderRequest.DriverGet)]
        public async Task<IActionResult> DriverGet([FromQuery] int orderRequestId)
        {
            var query = new GetOrderRequestQuery
            {
                UserId = LoggedUserId,
                OrderRequestId = orderRequestId
            };

            var response = await Mediator.Send(query);
            var result = Mapper.Map<GetOrderRequestResponse>(response);

            return Ok(result);
        }

        [Authorize(Policy = Constants.Policy.Driver)]
        [HttpGet(ApiRoutes.OrderRequest.DriverAccept)]
        public async Task<IActionResult> DriverAccept([FromQuery] int orderRequestId)
        {
            var query = new AcceptOrderRequestCommand
            {
                UserId = LoggedUserId,
                OrderRequestId = orderRequestId
            };

            _ = await Mediator.Send(query);

            return Ok();
        }

        // todo remove
        [HttpGet("api/v1/orderRequest/makeFake")]
        public async Task<IActionResult> MakeFakeOrderRequest([FromQuery] int customerId, [FromQuery] int? vehicleId)
        {
            var customer = await _customerRepository.GetByIdAsync(customerId);
            if (customer == null)
            {
                return BadRequest("Customer not found");
            }

            var random = new Random(5);

            var orderRequest = new OrderRequest
            {
                CreatedAt = DateTime.UtcNow,
                CustomerId = customerId,
                DeliveryAt = DateTime.UtcNow.AddDays(random.Next(10)),
                RequestNumber = Guid.NewGuid().ToString(),
                Points = new List<Point>
                {
                    new()
                    {
                        Order = 1,
                        CreatedAt = DateTime.UtcNow,
                        Country = "Ukraine",
                        City = "Kiev",
                        Street = "Kovalenka",
                        Building = random.Next(55).ToString(),
                        Latitude = random.Next(255151),
                        Longitude = random.Next(155421),
                        PlaceId = "some_fake_place_id_" + random.Next(59)
                    },
                    new()
                    {
                        Order = 2,
                        CreatedAt = DateTime.UtcNow,
                        Country = "Ukraine",
                        City = "Kiev",
                        Street = "Kovalenka",
                        Building = random.Next(55).ToString(),
                        Latitude = random.Next(255151),
                        Longitude = random.Next(155421),
                        PlaceId = "some_fake_place_id_2"+ random.Next(94)
                    }
                },
                Distance = random.Next(5505)
            };

            if (vehicleId.HasValue)
            {
                var vehicle = await _vehicleRepository.GetByIdAsync(vehicleId.Value);
                if (vehicle == null)
                {
                    return BadRequest("Vehicle not found");
                }

                var price = vehicle.CityPrice * (decimal)orderRequest.Distance;

                orderRequest.Orders.Add(new Order
                {
                    CreatedAt = DateTime.UtcNow,
                    ArrivalAt = null,
                    Price = price,
                    StatusTypes = Enums.OrderStatusTypes.Accepted,
                    VehicleId = vehicleId.Value
                });
            }

            await _orderRequestRepository.CreateAsync(orderRequest);

            if (!orderRequest.Orders.Any())
            {
                var query = new NotifyDriversOrderRequestQuery
                {
                    OrderRequestId = orderRequest.Id,
                    CreateAt = orderRequest.CreatedAt
                };

                _ = await Mediator.Send(query);
            }

            return Ok();
        }
    }
}