﻿using System.Threading.Tasks;
using ITHoot.Application.Company.Queries;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Responses.Company;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITHoot.Web.Controllers.V1
{
    [Authorize(Policy = Constants.Policy.Company)]
    public class CompanyController : BaseAuthApiController
    {
        [HttpGet(ApiRoutes.Company.Details)]
        public async Task<IActionResult> Details()
        {
            var query = new CompanyDetailsQuery { UserId = LoggedUserId };
            var response = await Mediator.Send(query);
            if (response == null)
            {
                return BadRequest();
            }

            return Ok(Mapper.Map<CompanyDetailsResponse>(response));
        }
    }
}
