﻿using ITHoot.Application.Order.Queries;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Responses.Order;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITHoot.Web.Controllers.V1
{
    public class OrderController : BaseAuthApiController
    {
        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpGet(ApiRoutes.Order.CarrierDownloadCsv)]
        public async Task<IActionResult> CarrierDownloadCsv([FromQuery] Enums.OrderStatusTypes statusType = Enums.OrderStatusTypes.Accepted)
        {
            var query = new CarrierDownloadOrdersCsvQuery
            {
                UserId = LoggedUserId,
                StatusTypes = statusType
            };

            var response = await Mediator.Send(query);

            return File(response.Item, "text/csv", response.FileName);
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpGet(ApiRoutes.Order.CarrierGetAll)]
        public async Task<IActionResult> CarrierGetAll([FromQuery] Enums.OrderStatusTypes statusType = Enums.OrderStatusTypes.Accepted)
        {
            var query = new CarrierGetAllOrdersQuery
            {
                UserId = LoggedUserId,
                StatusTypes = statusType
            };

            var response = await Mediator.Send(query);

            var results = Mapper.Map<IEnumerable<GetAllOrdersDetailResponse>>(response.Items);

            return Ok(results);
        }

        [Authorize(Policy = Constants.Policy.Driver)]
        [HttpGet(ApiRoutes.Order.DriverGetAll)]
        public async Task<IActionResult> DriverGetAll([FromQuery] Enums.DriverOrdersSelector? filter)
        {
            var query = new DriverGetAllOrdersQuery
            {
                UserId = LoggedUserId,
                Filter = filter
            };

            var response = await Mediator.Send(query);

            var results = Mapper.Map<IEnumerable<DriverGetAllDetailResponse>>(response.Items);

            return Ok(results);
        }

        [Authorize(Policy = Constants.Policy.Driver)]
        [HttpGet(ApiRoutes.Order.DriverGet)]
        public async Task<IActionResult> DriverGet([FromQuery] int orderId)
        {
            var query = new DriverGetOrderQuery
            {
                UserId = LoggedUserId,
                OrderId = orderId
            };

            var response = await Mediator.Send(query);

            return Ok(Mapper.Map<DriverGetOrderResponse>(response));
        }
    }
}