﻿using System;
using ITHoot.Application.Data.Queries;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Responses.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITHoot.Web.Infrastructure.BaseControllers;

namespace ITHoot.Web.Controllers.V1
{
    public class DataController : BaseAuthApiController
    {
        [HttpGet(ApiRoutes.Data.DriverStatuses)]
        public async Task<IActionResult> DriverStatuses()
        {
            return Ok(await GetDataTypes<Enums.DriverStatusTypes>());
        }

        [HttpGet(ApiRoutes.Data.VehicleStatuses)]
        public async Task<IActionResult> VehicleStatuses()
        {
            return Ok(await GetDataTypes<Enums.VehicleStatusTypes>());
        }

        [HttpGet(ApiRoutes.Data.VehicleServices)]
        public async Task<IActionResult> VehicleServices()
        {
            return Ok(await GetDataTypes<Enums.VehicleServiceTypes>());
        }

        [HttpGet(ApiRoutes.Data.BulkCargoVehicles)]
        public async Task<IActionResult> BulkCargoVehicles()
        {
            return Ok(await GetDataTypes<Enums.BulkCargoVehicleTypes>());
        }

        [HttpGet(ApiRoutes.Data.TrawlingVehicles)]
        public async Task<IActionResult> TrawlingVehicles()
        {
            return Ok(await GetDataTypes<Enums.TrawlingVehicleTypes>());
        }

        [HttpGet(ApiRoutes.Data.ContractStatuses)]
        public async Task<IActionResult> ContractStatuses()
        {
            return Ok(await GetDataTypes<Enums.ContractStatusTypes>());
        }

        [HttpGet(ApiRoutes.Data.OrderStatuses)]
        public async Task<IActionResult> OrderStatuses()
        {
            return Ok(await GetDataTypes<Enums.OrderStatusTypes>());
        }


        private async Task<IEnumerable<DataTypeResponse>> GetDataTypes<TEnum>()
            where TEnum : struct, Enum
        {
            var query = new GetDataTypesQuery { EnumType = typeof(TEnum) };

            var response = await Mediator.Send(query);

            return Mapper.Map<IEnumerable<DataTypeResponse>>(response);
        }
    }
}
