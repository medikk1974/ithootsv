﻿using ITHoot.Application.Customer.Commands;
using ITHoot.Application.Customer.Responses;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Requests.Customer;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ITHoot.Web.Controllers.V1
{
    [Authorize(Policy = Constants.Policy.AccessTokenCustomer)]
    public class CustomerController : BaseAuthApiController
    {
        [HttpPost(ApiRoutes.Customer.CreateOrderRequest)]
        public async Task<IActionResult> CreateOrderRequest([FromBody] CreateOrderRequestRequest request)
        {
            var command = Mapper.Map<CreateOrderRequestCommand>(request);
            command.UserId = LoggedUserId;

            var response = await Mediator.Send(command);

            return Ok(Mapper.Map<CreateOrderRequestResponse>(response));
        }
    }
}
