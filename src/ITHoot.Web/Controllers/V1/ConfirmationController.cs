﻿using System.Threading.Tasks;
using ITHoot.Application.Confirmation.Commands;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Requests.Confirmation;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Mvc;

namespace ITHoot.Web.Controllers.V1
{
    public class ConfirmationController : BaseAuthApiController
    {
        [HttpPost(ApiRoutes.Confirmation.SendEmail)]
        public async Task<IActionResult> SendEmail()
        {
            var command = new SendEmailConfirmationCommand { UserId = LoggedUserId.ToString() };
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest();
            }

            return Ok();
        }
        
        [HttpPost(ApiRoutes.Confirmation.ConfirmEmail)]
        public async Task<IActionResult> ValidateEmail(ConfirmEmailRequest request)
        {
            var command = Mapper.Map<ConfirmEmailCommand>(request);
            command.UserId = LoggedUserId.ToString();
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(response.Errors);
            }

            return Ok();
        }

        [HttpPost(ApiRoutes.Confirmation.SendSms)]
        public async Task<IActionResult> SendSms()
        {
            var command = new SendSMSConfirmationCommand { UserId = LoggedUserId.ToString() };
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPost(ApiRoutes.Confirmation.ConfirmPhone)]
        public async Task<IActionResult> ValidatePhone(ConfirmPhoneRequest request)
        {
            var command = Mapper.Map<ConfirmPhoneCommand>(request);
            command.UserId = LoggedUserId.ToString();
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(response.Errors);
            }

            return Ok();
        }
    }
}
