﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Queries;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Requests.Driver;
using ITHoot.Web.Contracts.V1.Responses.Driver;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITHoot.Web.Controllers.V1
{
    public class DriverController : BaseAuthApiController
    {
        [Authorize(Policy = Constants.Policy.Driver)]
        [HttpGet(ApiRoutes.Driver.GetInfo)]
        public async Task<IActionResult> GetInfo()
        {
            var query = new GetDriverInfoQuery
            {
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);

            return Ok(Mapper.Map<GetDriverInfoResponse>(response));
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpGet(ApiRoutes.Driver.Get)]
        public async Task<IActionResult> Get([FromQuery] int driverId)
        {
            var query = new GetDriverQuery
            {
                UserId = LoggedUserId,
                DriverId = driverId
            };

            var response = await Mediator.Send(query);
            if (response == null)
            {
                return BadRequest();
            }

            return Ok(Mapper.Map<DriverDetailsResponse>(response));
        }

        [Authorize(Policy = Constants.Policy.Company)]
        [HttpGet(ApiRoutes.Driver.GetAll)]
        public async Task<IActionResult> GetAllDrivers(Enums.DriverStatusTypes? statusType = null)
        {
            var query = new GetAllCarrierDriversQuery
            {
                UserId = LoggedUserId,
                StatusType = statusType
            };

            var response = await Mediator.Send(query);
            var results = Mapper.Map<IEnumerable<DriverDetailsResponse>>(response.Items);

            return Ok(results);
        }

        [Authorize(Policy = Constants.Policy.Company)]
        [HttpPost(ApiRoutes.Driver.Add)]
        public async Task<IActionResult> Add([FromBody] AddDriverRequest request)
        {
            var command = Mapper.Map<AddDriverCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Company)]
        [HttpPut(ApiRoutes.Driver.Edit)]
        public async Task<IActionResult> Edit([FromBody] EditDriverRequest request)
        {
            var command = Mapper.Map<EditDriverCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Company)]
        [HttpPost(ApiRoutes.Driver.SetPassword)]
        public async Task<IActionResult> SetPassword([FromBody] SetDriverPasswordRequest request)
        {
            var command = Mapper.Map<SetDriverPasswordCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);
            return Ok();
        }

        /*[Authorize(Policy = Constants.Policy.Driver)]
        [HttpPut(ApiRoutes.Driver.AssignVehicle)]
        public async Task<IActionResult> AssignVehicle([FromQuery] int? vehicleId)
        {
            var command = new AssignVehicleCommand
            {
                VehicleId = vehicleId,
                UserId = LoggedUserId
            };
            _ = await Mediator.Send(command);
            return Ok();
        }*/

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpPut(ApiRoutes.Driver.AssignVehicles)]
        public async Task<IActionResult> AssignVehicles([FromBody] AssignVehiclesRequest request)
        {
            var command = Mapper.Map<AssignDriverVehiclesCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);
            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpPut(ApiRoutes.Driver.ChangeStatus)]
        public async Task<IActionResult> ChangeStatusType([FromBody] ChangeDriverStatusRequest request)
        {
            var command = Mapper.Map<ChangeDriverStatusCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);
            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpDelete(ApiRoutes.Driver.Delete)]
        public async Task<IActionResult> Delete([FromQuery] int driverId)
        {
            var command = new DeleteDriverCommand
            {
                UserId = LoggedUserId,
                DriverId = driverId
            };
            _ = await Mediator.Send(command);
            return Ok();
        }
    }
}
