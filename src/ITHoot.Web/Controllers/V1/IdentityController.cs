﻿using System.Threading.Tasks;
using ITHoot.Application.Identity.Commands;
using ITHoot.Application.Identity.Commands.Company;
using ITHoot.Application.Identity.Commands.Customer;
using ITHoot.Application.Identity.Commands.Driver;
using ITHoot.Application.User.Commands;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Requests.Identity;
using ITHoot.Web.Contracts.V1.Requests.Identity.Company;
using ITHoot.Web.Contracts.V1.Requests.Identity.Customer;
using ITHoot.Web.Contracts.V1.Requests.Identity.Driver;
using ITHoot.Web.Contracts.V1.Responses.Identity;
using ITHoot.Web.Infrastructure.BaseControllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITHoot.Web.Controllers.V1
{
    public class IdentityController : BaseAuthApiController
    {
        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.DriverRegister)]
        public async Task<IActionResult> DriverRegister([FromBody] DriverRegistrationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new AuthFailedResponse { Errors = GetModelStateErrors() });
            }

            var command = Mapper.Map<DriverRegistrationCommand>(request);
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(Mapper.Map<AuthFailedResponse>(response));
            }

            return Ok(Mapper.Map<AuthSuccessResponse>(response));
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.CustomerRegister)]
        public async Task<IActionResult> CustomerRegister([FromBody] CustomerRegistrationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new AuthFailedResponse { Errors = GetModelStateErrors() });
            }

            var command = Mapper.Map<CustomerRegistrationCommand>(request);
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(Mapper.Map<AuthFailedResponse>(response));
            }

            return Ok(Mapper.Map<AuthSuccessResponse>(response));
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.CompanyRegister)]
        public async Task<IActionResult> CompanyRegister([FromBody] CompanyRegistrationRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new AuthFailedResponse { Errors = GetModelStateErrors() });
            }

            var command = Mapper.Map<CompanyRegistrationCommand>(request);
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(Mapper.Map<AuthFailedResponse>(response));
            }

            return Ok(Mapper.Map<AuthSuccessResponse>(response));
        }
        
        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.MobileLogin)]
        public async Task<IActionResult> MobileLogin([FromBody] MobileLoginRequest request)
        {
            var command = Mapper.Map<MobileLoginCommand>(request);
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(Mapper.Map<AuthFailedResponse>(response));
            }

            return Ok(Mapper.Map<AuthSuccessResponse>(response));
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.WebLogin)]
        public async Task<IActionResult> WebLogin([FromBody] WebLoginRequest request)
        {
            var command = Mapper.Map<WebLoginCommand>(request);
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(Mapper.Map<AuthFailedResponse>(response));
            }

            return Ok(Mapper.Map<AuthSuccessResponse>(response));
        }

        [AllowAnonymous]
        [HttpPost(ApiRoutes.Identity.Refresh)]
        public async Task<IActionResult> Refresh([FromBody] RefreshTokenRequest request)
        {
            var command = Mapper.Map<RefreshTokenCommand>(request);
            var response = await Mediator.Send(command);
            if (!response.Success)
            {
                return BadRequest(Mapper.Map<AuthFailedResponse>(response));
            }

            return Ok(Mapper.Map<AuthSuccessResponse>(response));
        }

        [HttpPost(ApiRoutes.Identity.Logout)]
        public async Task<IActionResult> Logout([FromBody] LogoutRequest request)
        {
            var command = Mapper.Map<LogoutCommand>(request);
            command.UserId = LoggedUserId;
            var response = await Mediator.Send(command);
            if (!response)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpGet(ApiRoutes.Identity.Revoke)]
        public async Task<IActionResult> Revoke()
        {
            var command = new RevokeCommand { UserId = LoggedUserId };
            var response = await Mediator.Send(command);
            if (!response)
            {
                return BadRequest();
            }

            return NoContent();
        }
        

        [HttpPost(ApiRoutes.Identity.GenerateToken)]
        public async Task<IActionResult> GenerateToken()
        {
            var command = new CreateUserAccessTokenCommand()
            {
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(command);

            return Ok(response);
        }
    }
}