﻿using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Queries;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Requests.Vehicle;
using ITHoot.Web.Contracts.V1.Responses.Contract;
using ITHoot.Web.Contracts.V1.Responses.Vehicle;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITHoot.Web.Infrastructure.BaseControllers;

namespace ITHoot.Web.Controllers.V1
{
    public class VehicleController : BaseAuthApiController
    {
        [Authorize(Policy = Constants.Policy.Driver)]
        [HttpGet(ApiRoutes.Vehicle.DriverGetAllAvailable)]
        public async Task<IActionResult> DriverGetAllAvailable()
        {
            var query = new GetVehiclesForDriverQuery
            {
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);
            var result = Mapper.Map<IEnumerable<GetVehiclesForDriverResponse>>(response);

            return Ok(result);
        }

        [Authorize(Policy = Constants.Policy.Customer)]
        [HttpGet(ApiRoutes.Vehicle.CustomerGetAllForContract)]
        public async Task<IActionResult> CustomerGetAllForContract([FromQuery] int carrierId, Enums.VehicleServiceTypes? serviceType = null)
        {
            var query = new GetAllVehiclesForCustomerQuery
            {
                CarrierId = carrierId,
                ServiceType = serviceType,
                //UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);
            var result = Mapper.Map<IEnumerable<ContractVehicleResponse>>(response.Items);

            return Ok(result);
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpGet(ApiRoutes.Vehicle.Get)]
        public async Task<IActionResult> Get([FromQuery] int vehicleId)
        {
            var query = new GetVehicleQuery
            {
                VehicleId = vehicleId,
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);
            if (response == null)
            {
                return BadRequest();
            }

            return Ok(Mapper.Map<VehiclesDetailsResponse>(response));
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpGet(ApiRoutes.Vehicle.CompanyGetAll)]
        public async Task<IActionResult> GetAll([FromQuery] Enums.VehicleStatusTypes? statusType = null)
        {
            var query = new GetAllCarrierVehiclesQuery
            {
                StatusType = statusType,
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);
            var result = Mapper.Map<IEnumerable<VehiclesDetailsResponse>>(response.Items);

            return Ok(result);
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpPost(ApiRoutes.Vehicle.Add)]
        public async Task<IActionResult> Add([FromBody] AddVehicleRequests request)
        {
            var command = Mapper.Map<AddVehicleCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpPut(ApiRoutes.Vehicle.Edit)]
        public async Task<IActionResult> Edit([FromBody] EditVehicleRequests request)
        {
            var command = Mapper.Map<EditVehicleCommand>(request);
            command.UserId = LoggedUserId;
            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpDelete(ApiRoutes.Vehicle.Delete)]
        public async Task<IActionResult> Delete([FromQuery] int vehicleId)
        {
            var command = new DeleteVehicleCommand
            {
                UserId = LoggedUserId,
                VehicleId = vehicleId
            };

            command.UserId = LoggedUserId;
            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpPut(ApiRoutes.Vehicle.AssignDrivers)]
        public async Task<IActionResult> AssignDrivers([FromBody] AssignDriversRequests request)
        {
            var command = Mapper.Map<AssignVehicleDriversCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);
            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Carrier)]
        [HttpPut(ApiRoutes.Vehicle.ChangeStatus)]
        public async Task<IActionResult> ChangeStatusType([FromBody] ChangeVehicleStatusRequest request)
        {
            var command = Mapper.Map<ChangeVehicleStatusCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);
            return Ok();
        }
    }
}
