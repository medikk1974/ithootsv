﻿using ITHoot.Application.Contract.Commands;
using ITHoot.Application.Contract.Queries;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1;
using ITHoot.Web.Contracts.V1.Requests.Contract;
using ITHoot.Web.Contracts.V1.Responses.Contract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ITHoot.Web.Infrastructure.BaseControllers;

namespace ITHoot.Web.Controllers.V1
{
    public class ContractController : BaseAuthApiController
    {

        [Authorize(Policy = Constants.Policy.Customer)]
        [HttpGet(ApiRoutes.Contract.CustomerGetAll)]
        public async Task<IActionResult> CustomerGetAll([FromQuery] Enums.ContractStatusTypes? statusType = null)
        {
            var query = new GetAllCustomerContractsQuery
            {
                StatusType = statusType,
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);
            var result = Mapper.Map<IEnumerable<CustomerContractDetailsResponse>>(response.Items);

            return Ok(result);
        }

        [Authorize(Policy = Constants.Policy.Company)]
        [HttpPut(ApiRoutes.Contract.CompanySetStatus)]
        public async Task<IActionResult> SetStatus([FromBody] SetContractStatusRequest request)
        {
            var command = Mapper.Map<SetContractStatusCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Company)]
        [HttpGet(ApiRoutes.Contract.CompanyGetAll)]
        public async Task<IActionResult> CompanyGetAll([FromQuery] Enums.ContractStatusTypes? statusType = null)
        {
            var query = new GetAllCompanyContractsQuery
            {
                StatusType = statusType,
                UserId = LoggedUserId
            };

            var response = await Mediator.Send(query);
            var result = Mapper.Map<IEnumerable<CompanyContractDetailsResponse>>(response.Items);

            return Ok(result);
        }

        [Authorize(Policy = Constants.Policy.Customer)]
        [HttpPost(ApiRoutes.Contract.Add)]
        public async Task<IActionResult> Add([FromBody] AddContractRequests request)
        {
            var command = Mapper.Map<AddContractCommand>(request);
            command.UserId = LoggedUserId;

            _ = await Mediator.Send(command);

            return Ok();
        }

        [Authorize(Policy = Constants.Policy.Customer)]
        [HttpDelete(ApiRoutes.Contract.Delete)]
        public async Task<IActionResult> Delete([FromQuery] int contractId)
        {
            var command = new DeleteContractCommand
            {
                UserId = LoggedUserId,
                ContractId = contractId
            };

            _ = await Mediator.Send(command);

            return Ok();
        }
    }
}
