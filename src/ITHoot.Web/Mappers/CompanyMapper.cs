﻿using ITHoot.Application.Company.Responses;
using ITHoot.Application.Extensions;
using ITHoot.Web.Contracts.V1.Responses.Company;

namespace ITHoot.Web.Mappers
{
    public class CompanyMapper : MapperBase
    {
        public CompanyMapper()
        {
            CreateMap<CompanyDetailsResult, CompanyDetailsResponse>()
                .Map(r => r.ContractNumber, r => r.Id)
                .Map(r => r.ContractDate, r => r.CreatedAt);
        }
    }
}
