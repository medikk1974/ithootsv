﻿using ITHoot.Application.Order.Responses;
using ITHoot.Web.Contracts.V1.Responses.Order;

namespace ITHoot.Web.Mappers
{
    public class OrderMapper : MapperBase
    {
        public OrderMapper()
        {
            CreateMap<CarrierGetAllOrdersDetailResult, GetAllOrdersDetailResponse>();

            CreateMap<DriverGetAllOrdersDetailResult, DriverGetAllDetailResponse>();

            CreateMap<CarrierOrderPointDetailResult, CarrierOrderPointDetailResponse>();

            CreateMap<DriverOrderPointDetailResult, DriverOrderPointDetailResponse>();

            CreateMap<DriverGetOrderResult, DriverGetOrderResponse>();
        }
    }
}
