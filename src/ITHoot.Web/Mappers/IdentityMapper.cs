﻿using ITHoot.Application.Identity.Commands;
using ITHoot.Application.Identity.Commands.Company;
using ITHoot.Application.Identity.Commands.Customer;
using ITHoot.Application.Identity.Commands.Driver;
using ITHoot.Application.Identity.Responses;
using ITHoot.Web.Contracts.V1.Requests.Identity;
using ITHoot.Web.Contracts.V1.Requests.Identity.Company;
using ITHoot.Web.Contracts.V1.Requests.Identity.Customer;
using ITHoot.Web.Contracts.V1.Requests.Identity.Driver;
using ITHoot.Web.Contracts.V1.Responses.Identity;

namespace ITHoot.Web.Mappers
{
    public class IdentityMapper : MapperBase
    {
        public IdentityMapper()
        {
            CreateMap<AuthenticationResult, AuthFailedResponse>();
            CreateMap<AuthenticationResult, AuthSuccessResponse>();

            CreateMap<MobileLoginRequest, MobileLoginCommand>();
            CreateMap<DriverRegistrationRequest, DriverRegistrationCommand>();

            CreateMap<WebLoginRequest, WebLoginCommand>();
            CreateMap<CompanyRegistrationRequest, CompanyRegistrationCommand>();
            
            CreateMap<CustomerRegistrationRequest, CustomerRegistrationCommand>();

            CreateMap<RefreshTokenRequest, RefreshTokenCommand>();
            CreateMap<LogoutRequest, LogoutCommand>();

            CreateMap<AddressRequest, AddressCommand>();
        }
    }
}
