﻿using ITHoot.Application.Extensions;
using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Web.Contracts.V1.Requests.Vehicle;
using ITHoot.Web.Contracts.V1.Responses.Contract;
using ITHoot.Web.Contracts.V1.Responses.Vehicle;

namespace ITHoot.Web.Mappers
{
    public class VehicleMapper : MapperBase
    {
        public VehicleMapper()
        {
            CreateMap<GetVehiclesForDriverResult, GetVehiclesForDriverResponse>();

            CreateMap<EditVehicleRequests, EditVehicleCommand>();

            CreateMap<AddVehicleRequests, AddVehicleCommand>();

            CreateMap<VehicleDetailsResult, VehiclesDetailsResponse>()
                .Map(m => m.AssignedDriver, r => r.AssignedDriver);

            CreateMap<VehiclesDriverDetailsResult, VehiclesDriverDetailsResponse>();

            CreateMap<AssignDriversRequests, AssignVehicleDriversCommand>();

            CreateMap<ChangeVehicleStatusRequest, ChangeVehicleStatusCommand>();


            CreateMap<DimensionsRequest, DimensionsCommand>();

            CreateMap<DimensionsResult, DimensionsResponse>();


            CreateMap<CoverageAreaRequest, CoverageAreaCommand>();

            CreateMap<CoverageAreaResult, CoverageAreaResponse>();

            CreateMap<VehicleDetailsResult, ContractVehicleResponse>()
                .Map(x => x.ServiceType, x => x.VehicleServiceType)
                .Map(x => x.Dimensions, x => x.VehicleDimensions);
        }
    }
}