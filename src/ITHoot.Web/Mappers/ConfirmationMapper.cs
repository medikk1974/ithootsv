﻿using ITHoot.Application.Confirmation.Commands;
using ITHoot.Web.Contracts.V1.Requests.Confirmation;

namespace ITHoot.Web.Mappers
{
    public class ConfirmationMapper : MapperBase
    {
        public ConfirmationMapper()
        {
            CreateMap<ConfirmEmailRequest, ConfirmEmailCommand>();

            CreateMap<ConfirmPhoneRequest, ConfirmPhoneCommand>();
        }
    }
}
