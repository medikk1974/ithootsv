﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Extensions;
using ITHoot.Web.Contracts.V1.Requests.Driver;
using ITHoot.Web.Contracts.V1.Responses.Driver;

namespace ITHoot.Web.Mappers
{
    public class DriverMapper : MapperBase
    {
        public DriverMapper()
        {
            CreateMap<GetDriverResult, DriverDetailsResponse>()
                .Map(m => m.DriverVehicles, r => r.DriverVehicles)
                .Map(m => m.AssignedVehicle, r => r.AssignedVehicle);

            CreateMap<DriverVehicleResult, DriverVehicleDetailsResponse>();

            CreateMap<AddDriverRequest, AddDriverCommand>();

            CreateMap<EditDriverRequest, EditDriverCommand>();

            CreateMap<ChangeDriverStatusRequest, ChangeDriverStatusCommand>();

            CreateMap<SetDriverPasswordRequest, SetDriverPasswordCommand>();

            CreateMap<AssignVehiclesRequest, AssignDriverVehiclesCommand>();
            
            CreateMap<GetDriverInfoResult, GetDriverInfoResponse>();
        }
    }
}
