﻿using ITHoot.Application.OrderRequest.Responses;
using ITHoot.Web.Contracts.V1.Responses.OrderRequest;

namespace ITHoot.Web.Mappers
{
    public class OrderRequestMapper : MapperBase
    {
        public OrderRequestMapper()
        {
            CreateMap<GetOrderRequestResult, GetOrderRequestResponse>();
        }
    }
}
