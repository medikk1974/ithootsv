﻿using ITHoot.Application.Contract.Commands;
using ITHoot.Application.Contract.Responses;
using ITHoot.Web.Contracts.V1.Requests.Contract;
using ITHoot.Web.Contracts.V1.Responses.Contract;

namespace ITHoot.Web.Mappers
{
    public class ContractMapper : MapperBase
    {
        public ContractMapper()
        {
            CreateMap<AddContractRequests, AddContractCommand>();

            CreateMap<CustomerContractDetailsResult, CustomerContractDetailsResponse>();

            CreateMap<ContractVehicleResult, ContractVehicleResponse>();

            CreateMap<CompanyContractDetailsResult, CompanyContractDetailsResponse>();

            CreateMap<VehicleNumberResult, VehicleNumberResponse>();

            CreateMap<SetContractStatusRequest, SetContractStatusCommand>();
        }
    }
}
