﻿using ITHoot.Application.Data.Responses;
using ITHoot.Web.Contracts.V1.Responses.Data;

namespace ITHoot.Web.Mappers
{
    public class DataMapper : MapperBase
    {
        public DataMapper()
        {
            CreateMap<DataTypeResult, DataTypeResponse>();
        }
    }
}
