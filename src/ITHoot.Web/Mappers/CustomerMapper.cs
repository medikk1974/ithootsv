﻿using ITHoot.Application.Customer.Commands;
using ITHoot.Web.Contracts.V1.Requests.Customer;

namespace ITHoot.Web.Mappers
{
    public class CustomerMapper : MapperBase
    {
        public CustomerMapper()
        {
            CreateMap<CreateOrderRequestRequest, CreateOrderRequestCommand>();
        }
    }
}
