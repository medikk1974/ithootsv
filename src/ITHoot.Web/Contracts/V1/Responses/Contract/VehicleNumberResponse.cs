﻿namespace ITHoot.Web.Contracts.V1.Responses.Contract
{
    public class VehicleNumberResponse
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
    }
}
