﻿using ITHoot.Application.Vehicle.Responses;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1.Responses.Vehicle;

namespace ITHoot.Web.Contracts.V1.Responses.Contract
{
    public class ContractVehicleResponse
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public Enums.VehicleServiceTypes ServiceType { get; set; }
        public int MaxLoad { get; set; }
        public DimensionsResponse Dimensions { get; set; }
        public VehiclesDriverDetailsResult AssignedDriver { get; set; }
    }
}
