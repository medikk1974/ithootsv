﻿using ITHoot.Models;
using System;
using System.Collections.Generic;

namespace ITHoot.Web.Contracts.V1.Responses.Contract
{
    public class CustomerContractDetailsResponse
    {
        public int Id { get; set; }
        public string CarrierName { get; set; }
        public string ContractNumber { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public Enums.ContractStatusTypes StatusType { get; set; }

        public IEnumerable<ContractVehicleResponse> Vehicles { get; set; } = new List<ContractVehicleResponse>();
    }
}
