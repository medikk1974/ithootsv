﻿using System;
using System.Collections.Generic;
using ITHoot.Models;

namespace ITHoot.Web.Contracts.V1.Responses.Driver
{
    public class DriverDetailsResponse
    {
        public int Id { get; set; }
        public Enums.DriverStatusTypes StatusType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedAt { get; set; }

        public DriverVehicleDetailsResponse AssignedVehicle { get; set; }
        public IEnumerable<DriverVehicleDetailsResponse> DriverVehicles { get; set; } = new List<DriverVehicleDetailsResponse>();
    }
}
