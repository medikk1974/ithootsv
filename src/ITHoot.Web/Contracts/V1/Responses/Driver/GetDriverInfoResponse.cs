﻿namespace ITHoot.Web.Contracts.V1.Responses.Driver
{
    public class GetDriverInfoResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
