﻿namespace ITHoot.Web.Contracts.V1.Responses.Driver
{
    public class DriverVehicleDetailsResponse
    {
        public int Id { get; set; }

        public int MaxLoad { get; set; }
        public string RegistrationNumber { get; set; }
    }
}