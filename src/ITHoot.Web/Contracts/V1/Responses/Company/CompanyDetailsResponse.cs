﻿using System;

namespace ITHoot.Web.Contracts.V1.Responses.Company
{
    public class CompanyDetailsResponse
    {
        public int ContractNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonalCode { get; set; }
        public string VATCode { get; set; }
        public string RegistrationAddress { get; set; }
        //public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime ContractDate { get; set; }
    }
}
