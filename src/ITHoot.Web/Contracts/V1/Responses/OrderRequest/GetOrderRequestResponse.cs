﻿using System;
using ITHoot.Models;
using ITHoot.Web.Contracts.V1.Responses.Order;

namespace ITHoot.Web.Contracts.V1.Responses.OrderRequest
{
    public class GetOrderRequestResponse
    {
        public double Distance { get; set; }
        public double Price { get; set; }
        public DateTime DeliveryAt { get; set; }

        public CarrierOrderPointDetailResponse PointFrom { get; set; }
        public CarrierOrderPointDetailResponse PointTo { get; set; }

        public double Weight { get; set; }
        public Enums.VehicleServiceTypes ServiceType { get; set; }
    }
}
