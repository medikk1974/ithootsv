﻿using System;

namespace ITHoot.Web.Contracts.V1.Responses.Order
{
    public class DriverGetAllDetailResponse
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public double Price { get; set; }
        public DateTime DeliveryAt { get; set; }
    }
}
