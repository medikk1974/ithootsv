﻿using ITHoot.Models;
using System;

namespace ITHoot.Web.Contracts.V1.Responses.Order
{
    public class GetAllOrdersDetailResponse
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        public int VehicleId { get; set; }
        public string VehicleRegistrationNumber { get; set; }

        public double Distance { get; set; }
        public double Price { get; set; }

        public CarrierOrderPointDetailResponse PointFrom { get; set; }
        public CarrierOrderPointDetailResponse PointTo { get; set; }

        public DateTime DeliveryAt { get; set; }

        public Enums.OrderStatusTypes StatusTypes { get; set; }
    }
}
