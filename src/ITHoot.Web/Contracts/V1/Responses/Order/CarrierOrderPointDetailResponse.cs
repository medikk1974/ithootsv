﻿namespace ITHoot.Web.Contracts.V1.Responses.Order
{
    public class CarrierOrderPointDetailResponse
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
    }
}
