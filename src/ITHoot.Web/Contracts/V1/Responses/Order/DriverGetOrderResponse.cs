﻿using ITHoot.Application.Order.Responses;
using System;

namespace ITHoot.Web.Contracts.V1.Responses.Order
{
    public class DriverGetOrderResponse
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }

        public DriverOrderPointDetailResult PointFrom { get; set; }
        public DriverOrderPointDetailResult PointTo { get; set; }

        public DateTime DeliveryDate { get; set; }
    }
}
