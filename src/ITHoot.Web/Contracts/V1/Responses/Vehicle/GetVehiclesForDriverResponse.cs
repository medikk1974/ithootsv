﻿using ITHoot.Models;

namespace ITHoot.Web.Contracts.V1.Responses.Vehicle
{
    public class GetVehiclesForDriverResponse
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public int MaxLoad { get; set; }
        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }
    }
}
