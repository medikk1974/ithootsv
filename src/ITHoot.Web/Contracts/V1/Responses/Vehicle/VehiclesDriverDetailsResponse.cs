﻿namespace ITHoot.Web.Contracts.V1.Responses.Vehicle
{
    public class VehiclesDriverDetailsResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
