﻿namespace ITHoot.Web.Contracts.V1.Responses.Vehicle
{
    public class DimensionsResponse
    {
        public int Length { get; set; }
        public int Wight { get; set; }
        public int RampDegree { get; set; }
    }
}
