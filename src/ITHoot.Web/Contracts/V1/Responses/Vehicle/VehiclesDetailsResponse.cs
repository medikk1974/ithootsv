﻿using ITHoot.Models;
using System.Collections.Generic;

namespace ITHoot.Web.Contracts.V1.Responses.Vehicle
{
    public class VehiclesDetailsResponse
    {
        public int Id { get; set; }
        public Enums.VehicleStatusTypes StatusType { get; set; }
        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }
        public int MaxLoad { get; set; }
        public decimal CityPrice { get; set; }
        public decimal HighwayPrice { get; set; }
        public string RegistrationNumber { get; set; }

        public IEnumerable<VehiclesDriverDetailsResponse> VehicleDrivers { get; set; } = new List<VehiclesDriverDetailsResponse>();
        public VehiclesDriverDetailsResponse AssignedDriver { get; set; }
        public DimensionsResponse VehicleDimensions { get; set; }
        public CoverageAreaResponse VehicleCoverageArea { get; set; }
    }
}
