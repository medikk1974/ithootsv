﻿namespace ITHoot.Web.Contracts.V1.Responses.Vehicle
{
    public class CoverageAreaResponse
    {
        public string Country { get; set; }
        public string City { get; set; }
        public double WorkRadius { get; set; }
        public string PlaceId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
