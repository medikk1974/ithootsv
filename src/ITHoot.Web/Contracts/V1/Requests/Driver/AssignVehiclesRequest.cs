﻿using System.Collections.Generic;

namespace ITHoot.Web.Contracts.V1.Requests.Driver
{
    public class AssignVehiclesRequest
    {
        public int DriverId { get; set; }
        
        public IEnumerable<int> VehicleIds { get; set; } = new List<int>();
    }
}
