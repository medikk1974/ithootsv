﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ITHoot.Web.Contracts.V1.Requests.Driver
{
    public class EditDriverBaseRequest
    {
        public IEnumerable<int> VehicleIds { get; set; } = new List<int>();

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [MaxLength(50)]
        public string Email { get; set; }

        [Phone]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
