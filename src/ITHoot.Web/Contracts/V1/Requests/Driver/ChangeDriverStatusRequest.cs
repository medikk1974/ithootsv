﻿using ITHoot.Models;

namespace ITHoot.Web.Contracts.V1.Requests.Driver
{
    public class ChangeDriverStatusRequest
    {
        public int DriverId { get; set; }
        public Enums.DriverStatusTypes StatusType { get; set; }
    }
}
