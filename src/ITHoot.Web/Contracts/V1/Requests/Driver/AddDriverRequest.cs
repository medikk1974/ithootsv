﻿using System.ComponentModel.DataAnnotations;

namespace ITHoot.Web.Contracts.V1.Requests.Driver
{
    public class AddDriverRequest : EditDriverBaseRequest
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
