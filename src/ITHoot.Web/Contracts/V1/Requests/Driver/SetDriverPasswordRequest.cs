﻿using System.ComponentModel.DataAnnotations;

namespace ITHoot.Web.Contracts.V1.Requests.Driver
{
    public class SetDriverPasswordRequest
    {
        public int DriverId { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
