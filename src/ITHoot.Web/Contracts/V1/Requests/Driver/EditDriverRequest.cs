﻿namespace ITHoot.Web.Contracts.V1.Requests.Driver
{
    public class EditDriverRequest : EditDriverBaseRequest
    {
        public int DriverId { get; set; }
    }
}
