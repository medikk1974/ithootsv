﻿using ITHoot.Models;

namespace ITHoot.Web.Contracts.V1.Requests.Contract
{
    public class SetContractStatusRequest
    {
        public int Id { get; set; }
        public Enums.ContractStatusTypes StatusType { get; set; }
    }
}
