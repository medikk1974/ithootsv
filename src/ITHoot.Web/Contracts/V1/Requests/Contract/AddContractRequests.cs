﻿using System;
using System.Collections.Generic;

namespace ITHoot.Web.Contracts.V1.Requests.Contract
{
    public class AddContractRequests
    {
        public int CarrierId { get; set; }
        public string ContractNumber { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }

        public List<int> VehiclesId { get; set; } = new List<int>();
    }
}
