﻿using ITHoot.Models;

namespace ITHoot.Web.Contracts.V1.Requests.Vehicle
{
    public class ChangeVehicleStatusRequest
    {
        public int VehicleId { get; set; }
        public Enums.VehicleStatusTypes StatusType { get; set; }
    }
}
