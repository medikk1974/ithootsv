﻿using System.Collections.Generic;

namespace ITHoot.Web.Contracts.V1.Requests.Vehicle
{
    public class AssignDriversRequests
    {
        public int VehicleId { get; set; }

        public IEnumerable<int> DriverIds { get; set; } = new List<int>();
    }
}
