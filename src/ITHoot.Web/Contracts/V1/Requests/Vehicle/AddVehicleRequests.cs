﻿using ITHoot.Models;
using System.Collections.Generic;

namespace ITHoot.Web.Contracts.V1.Requests.Vehicle
{
    public class AddVehicleRequests
    {
        public IEnumerable<int> DriverIds { get; set; } = new List<int>();
        public decimal CityPrice { get; set; }
        public decimal HighwayPrice { get; set; }
        public int MaxLoad { get; set; }
        public string RegistrationNumber { get; set; }
        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }

        public DimensionsRequest VehicleDimensions { get; set; }
        public CoverageAreaRequest VehicleCoverageArea { get; set; }
    }
}
