﻿namespace ITHoot.Web.Contracts.V1.Requests.Vehicle
{
    public class EditVehicleRequests : AddVehicleRequests
    {
        public int VehicleId { get; set; }
    }
}
