﻿namespace ITHoot.Web.Contracts.V1.Requests.Vehicle
{
    public class DimensionsRequest
    {
        public int Length { get; set; }
        public int Wight { get; set; }
        public int RampDegree { get; set; }
    }
}
