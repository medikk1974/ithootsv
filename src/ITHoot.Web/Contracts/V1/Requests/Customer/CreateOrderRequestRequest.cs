﻿using ITHoot.Application.Customer.Commands;
using ITHoot.Models;
using System;

namespace ITHoot.Web.Contracts.V1.Requests.Customer
{
    public class CreateOrderRequestRequest
    {
        public Enums.VehicleServiceTypes ServiceType { get; set; }
        public string RequestNumber { get; set; }
        public CreatePointCommand PointFrom { get; set; }
        public CreatePointCommand PointTo { get; set; }
        public DateTime DeliveryAt { get; set; }
        public string Note { get; set; }
        public double Weight { get; set; }
    }
}
