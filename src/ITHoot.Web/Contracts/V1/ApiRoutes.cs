﻿namespace ITHoot.Web.Contracts.V1
{
    public static class ApiRoutes
    {
        private const string Root = "api";
        private const string Version = "v1";
        private const string Base = Root + "/" + Version;

        public static class Identity
        {
            private const string BaseIdentity = Base + "/identity";

            private const string BaseRegister = BaseIdentity + "/register";
            public const string DriverRegister = BaseRegister + "/driver";
            public const string CompanyRegister = BaseRegister + "/company";
            public const string CustomerRegister = BaseRegister + "/customer";

            private const string BaseLogin = BaseIdentity + "/login";
            public const string MobileLogin = BaseLogin + "/mobile";
            public const string WebLogin = BaseLogin + "/web";

            public const string Refresh = BaseIdentity + "/refresh";
            public const string Logout = BaseIdentity + "/logout";
            public const string Revoke = BaseIdentity + "/revoke";

            public const string GenerateToken = BaseIdentity + "/generateToken";
        }

        public static class Driver
        {
            private const string BaseDriver = Base + "/driver";

            public const string GetAll = BaseDriver + "/getAll";
            public const string Get = BaseDriver + "/get";
            public const string GetInfo = BaseDriver + "/getInfo";
            public const string Add = BaseDriver + "/add";
            public const string Edit = BaseDriver + "/edit";
            public const string Delete = BaseDriver + "/delete";
            //public const string AssignVehicle = BaseDriver + "/assignVehicle";
            public const string AssignVehicles = BaseDriver + "/assignVehicles";
            public const string ChangeStatus = BaseDriver + "/changeStatus";

            public const string SetPassword = BaseDriver + "/setPassword";
        }

        public static class Company
        {
            private const string BaseCompany = Base + "/company";

            public const string Details = BaseCompany + "/details";
        }

        public static class Confirmation
        {
            private const string BaseConfirmation = Base + "/confirmation";

            public const string SendEmail = BaseConfirmation + "/sendEmail";
            public const string ConfirmEmail = BaseConfirmation + "/confirmEmail";
            public const string SendSms = BaseConfirmation + "/sendSms";
            public const string ConfirmPhone = BaseConfirmation + "/confirmPhone";
        }

        public static class Vehicle
        {
            private const string BaseVehicle = Base + "/vehicle";

            private const string CustomerVehicle = BaseVehicle + "/customer";
            private const string CarrierVehicle = BaseVehicle + "/carrier";
            private const string DriverVehicle = BaseVehicle + "/driver";

            public const string CustomerGetAllForContract = CustomerVehicle + "/getAllForContract";
            public const string CompanyGetAll = CarrierVehicle + "/getAll";
            public const string DriverGetAllAvailable = DriverVehicle + "/getAllAvailable";
            public const string Get = BaseVehicle + "/get";
            public const string Add = BaseVehicle + "/add";
            public const string Edit = BaseVehicle + "/edit";
            public const string Delete = BaseVehicle + "/delete";
            public const string AssignDrivers = BaseVehicle + "/assignDrivers";
            public const string ChangeStatus = BaseVehicle + "/changeStatus";
        }

        public static class Contract
        {
            private const string BaseContract = Base + "/contract";

            private const string CustomerContract = BaseContract + "/customer";
            private const string CarrierContract = BaseContract + "/carrier";

            public const string CustomerGetAll = CustomerContract + "/getAll";
            public const string CompanyGetAll = CarrierContract + "/getAll";
            public const string CompanySetStatus = CarrierContract + "/setStatus";
            
            //public const string Get = BaseContract + "/get";
            public const string Add = BaseContract + "/add";
            //public const string Edit = BaseContract + "/edit";
            public const string Delete = BaseContract + "/delete";
        }
        
        public static class Data
        {
            private const string BaseData = Base + "/data";
            private const string BaseDataType = BaseData + "/types";

            public const string VehicleServices = BaseDataType + "/vehicleServices";
            public const string BulkCargoVehicles = BaseDataType + "/bulkCargoVehicles";
            public const string TrawlingVehicles = BaseDataType + "/trawlingVehicles";
            public const string DriverStatuses = BaseDataType + "/driverStatuses";
            public const string VehicleStatuses = BaseDataType + "/vehicleStatuses";
            public const string ContractStatuses = BaseDataType + "/contractStatuses";
            public const string OrderStatuses = BaseDataType + "/orderStatuses";
        }

        public static class Order
        {
            private const string BaseOrder = Base + "/order";
            private const string CarrierOrder = BaseOrder + "/carrier";
            private const string DriverOrder = BaseOrder + "/driver";

            public const string CarrierGetAll = CarrierOrder + "/getAll";
            public const string DriverGetAll = DriverOrder + "/getAll";
            public const string DriverGet = DriverOrder + "/get";
            public const string CarrierDownloadCsv = CarrierOrder + "/downloadCsv";
        }

        public static class Customer
        {
            private const string BaseCustomer = Base + "/customer";

            public const string CreateOrderRequest = BaseCustomer + "/createRequest";
        }

        public static class OrderRequest
        {
            private const string BaseOrder = Base + "/orderRequest";
            private const string DriverOrder = BaseOrder + "/driver";

            public const string DriverGet = DriverOrder + "/get";
            public const string DriverAccept = DriverOrder + "/accept";
        }
    }
}
