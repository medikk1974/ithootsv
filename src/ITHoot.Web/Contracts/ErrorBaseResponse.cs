﻿using System.Collections.Generic;

namespace ITHoot.Web.Contracts
{
    public class ErrorBaseResponse
    {
        public IEnumerable<string> Errors { get; set; }
    }
}
