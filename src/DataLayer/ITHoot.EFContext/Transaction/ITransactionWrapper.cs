﻿using System;
using ITHoot.EFContext.Transaction.Actions;

namespace ITHoot.EFContext.Transaction
{
    public interface ITransactionWrapper : IDisposable
    {
        void RegisterAfterCommitAction(ActionBase action);
        void RegisterAfterRollbackAction(ActionBase action);

        void Commit();
    }
}
