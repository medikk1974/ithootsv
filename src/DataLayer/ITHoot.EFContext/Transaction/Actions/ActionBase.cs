﻿namespace ITHoot.EFContext.Transaction.Actions
{
    public  abstract class ActionBase
    {
        public abstract void Execute();
    }
}
