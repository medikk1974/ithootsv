﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ITHoot.EFContext.Migrations
{
    public partial class AddCoverageAreasTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CoverageAreaId",
                table: "Vehicles",
                type: "int",
                nullable: false);

            migrationBuilder.CreateTable(
                name: "CoverageAreas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    Country = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "cp1257"),
                    District = table.Column<string>(type: "longtext", nullable: true)
                        .Annotation("MySql:CharSet", "cp1257"),
                    Area = table.Column<double>(type: "double", nullable: false),
                    Latitude = table.Column<double>(type: "double", nullable: false),
                    Longitude = table.Column<double>(type: "double", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoverageAreas", x => x.Id);
                })
                .Annotation("MySql:CharSet", "cp1257");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "caacb290-2ddc-41ee-a7fe-bc4486d864d5");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "2c00ec4e-9928-425a-9fd3-9654e6dd98c1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "9b9e21a8-faa9-4059-bea1-46bbc706857b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "640edb28-9db8-42f7-b88a-f22c593b00d1");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "17636b07-4f31-4963-9347-73abf96b851d");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_CoverageAreaId",
                table: "Vehicles",
                column: "CoverageAreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_CoverageAreas_CoverageAreaId",
                table: "Vehicles",
                column: "CoverageAreaId",
                principalTable: "CoverageAreas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_CoverageAreas_CoverageAreaId",
                table: "Vehicles");

            migrationBuilder.DropTable(
                name: "CoverageAreas");

            migrationBuilder.DropIndex(
                name: "IX_Vehicles_CoverageAreaId",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "CoverageAreaId",
                table: "Vehicles");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "58bb86c4-a15a-49cb-933b-9c13d88e878f");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "ffd7d953-a052-4896-9066-f10cc4447dc9");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "22cc30b6-9c54-438a-b6f8-51edc2d46e8c");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "7400a3c4-4356-41eb-a716-1695668a9d1e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "a11693ec-c4be-492f-b15a-023411b188a0");
        }
    }
}
