﻿using ITHoot.EFContext.Extensions;
using ITHoot.Models;
using ITHoot.Models.Entities.Localization;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.EFContext.Migrations.SeedData
{
    public class DefaultLanguagesCreator
    {
        private readonly ModelBuilder _modelBuilder;

        public DefaultLanguagesCreator(ModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Create()
        {
            CreateLanguages();
        }

        private void CreateLanguages()
        {
            foreach (var (key, value) in EnumExtensions.EnumToList<Enums.LanguageTypes>())
            {
                _modelBuilder.Entity<Language>()
                    .HasData(
                        new Language
                        {
                            Id = (int)key,
                            Name = key.ToString(),
                            LanguageCulture = value,
                            Published = true
                        }
                    );
            }
        }
    }
}