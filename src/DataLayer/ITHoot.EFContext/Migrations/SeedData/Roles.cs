﻿using ITHoot.EFContext.Extensions;
using ITHoot.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.EFContext.Migrations.SeedData
{
    public class DefaultRolesCreator
    {
        private readonly ModelBuilder _modelBuilder;

        public DefaultRolesCreator(ModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Create()
        {
            CreateRoles();
        }

        private void CreateRoles()
        {
            var enums = new[]
            {
                "caacb290-2ddc-41ee-a7fe-bc4486d864d5",
                "2c00ec4e-9928-425a-9fd3-9654e6dd98c1",
                "9b9e21a8-faa9-4059-bea1-46bbc706857b",
                "640edb28-9db8-42f7-b88a-f22c593b00d1",
                "17636b07-4f31-4963-9347-73abf96b851d",
                "bd633b3b-4c42-4268-9a6e-9063ed7c9dcd",
                "a3036098-6c1e-4ec6-99fc-9130f1f055fe",
                "fae7095f-96fe-4a46-8c19-7511af8a0491",
                "ee871114-2af9-44eb-9d32-951ee9dc36d3",
                "b53a66d4-b29b-4c1d-bd74-d517bbb75bda",
                "8260518b-8d7c-4110-8e87-2021eb70d238",
                "ba51f10b-e856-4fac-9023-5b251f8b6850",
                "46caf51d-d06e-49e6-be15-f017a7436857",
                "da273131-6e6b-43fb-a84d-71bac2767ba0",
                "91725b0c-dcd9-49d6-baa6-5cabb6df4d58",
                "974d23fa-bfa3-4ea6-90a7-e1f1596af5fd",
                "7488ac3a-4863-4363-a69c-5dab033580f5",
                "d6807517-924e-4788-b760-07462e76b53a",
                "0825d596-48bc-4733-9bdb-30917a8a8d3f"
            };
            var index = 0;
            var types = EnumExtensions.EnumToList<Enums.RoleTypes>();
            var normalizer = new UpperInvariantLookupNormalizer();

            foreach (var (key, _) in types)
            {
                _modelBuilder.Entity<IdentityRole<int>>()
                    .HasData(
                        new IdentityRole<int>
                        {
                            Id = (int)key,
                            Name = key.ToString(),
                            NormalizedName = normalizer.NormalizeName(key.ToString()),
                            ConcurrencyStamp = enums[index]
                        }
                    );

                index++;
            }
        }
    }
}