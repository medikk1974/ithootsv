﻿using System;
using System.Collections.Generic;
using ITHoot.Models;
using ITHoot.Models.Entities.Localization;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.EFContext.Migrations.SeedData
{
    public class DefaultTranslationsCreator
    {
        private readonly ModelBuilder _modelBuilder;

        private readonly DateTime _dateTime = new(2021, 5, 5, 5, 5, 5, DateTimeKind.Utc);
        private int _index;

        public DefaultTranslationsCreator(ModelBuilder modelBuilder)
        {
            _modelBuilder = modelBuilder;
        }

        public void Create()
        {
            CreateTranslations();
        }

        private void CreateTranslations()
        {
            var toEnglish = new Dictionary<string, string>
            {
                { Constants.Templates.SMSConfirmTextKey, "Your phone number verification code: {0}" },
                { Constants.Templates.EmailConfirmTextKey, "Hello, {0}!<br />Your email address verification code: {1}" },
                { Constants.Templates.EmailConfirmSubjectKey, "ITHoot email confirmation" }
            };

            var toLithuanian = new Dictionary<string, string>
            {
                { Constants.Templates.SMSConfirmTextKey, "Jūsų telefono numerio patvirtinimo kodas: {0}" },
                { Constants.Templates.EmailConfirmTextKey, "Sveiki, {0}!<br />Jūsų el. Pašto adreso patvirtinimo kodas: {1}" },
                { Constants.Templates.EmailConfirmSubjectKey, "„ITHoot“ el. Pašto patvirtinimas" }
            };

            HasData((int)Enums.LanguageTypes.English, toEnglish);
            HasData((int)Enums.LanguageTypes.Lithuanian, toLithuanian);
        }

        private void HasData(int languageId, Dictionary<string, string> items)
        {
            foreach (var (key, value) in items)
            {
                HasData(languageId, key, value);
            }
        }

        private void HasData(int languageId, string resourceName, string resourceValue)
        {
            _modelBuilder.Entity<LocaleStringResource>()
                .HasData(
                    new LocaleStringResource
                    {
                        Id = ++_index,
                        LanguageId = languageId,
                        ResourceName = resourceName,
                        ResourceValue = resourceValue,
                        CreatedAt = _dateTime,
                        UpdatedAt = _dateTime
                    });
        }
    }
}