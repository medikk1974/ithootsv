﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ITHoot.EFContext.Migrations
{
    public partial class ChangeCoverageAreaTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "District",
                table: "CoverageAreas",
                newName: "City");

            migrationBuilder.RenameColumn(
                name: "Area",
                table: "CoverageAreas",
                newName: "WorkRadius");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkRadius",
                table: "CoverageAreas",
                newName: "Area");

            migrationBuilder.RenameColumn(
                name: "City",
                table: "CoverageAreas",
                newName: "District");
        }
    }
}
