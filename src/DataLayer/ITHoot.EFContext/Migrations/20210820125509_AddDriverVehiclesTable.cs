﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ITHoot.EFContext.Migrations
{
    public partial class AddDriverVehiclesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DriverVehicle",
                columns: table => new
                {
                    DriverVehiclesId = table.Column<int>(type: "int", nullable: false),
                    VehicleDriversId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverVehicle", x => new { x.DriverVehiclesId, x.VehicleDriversId });
                    table.ForeignKey(
                        name: "FK_DriverVehicle_Drivers_VehicleDriversId",
                        column: x => x.VehicleDriversId,
                        principalTable: "Drivers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverVehicle_Vehicles_DriverVehiclesId",
                        column: x => x.DriverVehiclesId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "cp1257");

            migrationBuilder.CreateIndex(
                name: "IX_DriverVehicle_VehicleDriversId",
                table: "DriverVehicle",
                column: "VehicleDriversId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverVehicle");
        }
    }
}
