﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ITHoot.EFContext.Migrations
{
    public partial class AddTranslations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivityArea",
                table: "Vehicles");

            migrationBuilder.InsertData(
                table: "LocaleStringResources",
                columns: new[] { "Id", "CreatedAt", "LanguageId", "ResourceName", "ResourceValue", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc), 1, "SMSConfirmTextKey", "Your phone number verification code: {0}", new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc) },
                    { 2, new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc), 1, "EmailConfirmTextKey", "Hello, {0}!<br />Your email address verification code: {1}", new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc) },
                    { 3, new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc), 1, "EmailConfirmSubjectKey", "ITHoot email confirmation", new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc) },
                    { 4, new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc), 2, "SMSConfirmTextKey", "Jūsų telefono numerio patvirtinimo kodas: {0}", new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc) },
                    { 5, new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc), 2, "EmailConfirmTextKey", "Sveiki, {0}!<br />Jūsų el. Pašto adreso patvirtinimo kodas: {1}", new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc) },
                    { 6, new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc), 2, "EmailConfirmSubjectKey", "„ITHoot“ el. Pašto patvirtinimas", new DateTime(2021, 5, 5, 5, 5, 5, 0, DateTimeKind.Utc) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "LocaleStringResources",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "LocaleStringResources",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "LocaleStringResources",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "LocaleStringResources",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "LocaleStringResources",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "LocaleStringResources",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.AddColumn<int>(
                name: "ActivityArea",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
