﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.EFContext
{
    public class DbInitializer
    {
        public static async Task Initialize(DbContext context)
        {
            if ((await context.Database.GetPendingMigrationsAsync()).Any())
            {
                await context.Database.MigrateAsync();
            }

            await context.Database.EnsureCreatedAsync();
        }
    }
}
