﻿using System;
using ITHoot.EFContext.Migrations.SeedData;
using ITHoot.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ITHoot.EFContext.Extensions
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            new DefaultRolesCreator(modelBuilder).Create();
            new DefaultLanguagesCreator(modelBuilder).Create();
            new DefaultTranslationsCreator(modelBuilder).Create();
        }
        
        public static void UseDataProtection(this ModelBuilder modelBuilder, ILookupProtectorKeyRing keyRing, ILookupProtector protector)
        {
            var converter = new ValueConverter<string, string>(
                s => protector.Protect(keyRing.CurrentKeyId, s),
                s => protector.Unprotect(keyRing.CurrentKeyId, s));

            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                foreach (var property in entity.GetProperties())
                {
                    if (property.ClrType != typeof(string)
                        || !Attribute.IsDefined(property.PropertyInfo, typeof(ProtectedPersonalDataAttribute)))
                    {
                        continue;
                    }

                    modelBuilder
                        .Entity(entity.ClrType)
                        .Property(typeof(string), property.Name)
                        .HasConversion(converter);
                }
            }
        }

        public static void ConfigureEntities(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessToken>()
                .HasIndex(p => p.Token);

            modelBuilder.Entity<Vehicle>()
                .HasMany(r => r.VehicleDrivers)
                .WithMany(r => r.DriverVehicles);

            modelBuilder.Entity<Vehicle>()
                .HasOne(r => r.AssignedDriver)
                .WithOne(r => r.AssignedVehicle)
                .HasForeignKey<Vehicle>(r => r.AssignedDriverId);
        }
    }
}
