﻿using ITHoot.EFContext.Extensions;
using ITHoot.Models.Entities;
using ITHoot.Models.Entities.Localization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace ITHoot.EFContext
{
    public class EFDbContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public EFDbContext(DbContextOptions<EFDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public virtual DbSet<AccessToken> AccessTokens { get; set; }

        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<LocalizedProperty> LocalizedProperties { get; set; }
        public virtual DbSet<LocaleStringResource> LocaleStringResources { get; set; }

        public virtual DbSet<Carrier> Carriers { get; set; }
        public virtual DbSet<Driver> Drivers { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Dimensions> Dimensions { get; set; }
        public virtual DbSet<CoverageArea> CoverageAreas { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<OrderRequest> OrderRequests { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Point> Points { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("MySql:CharSet", "cp1257");
            
            base.OnModelCreating(modelBuilder);

            modelBuilder.ConfigureEntities();

            modelBuilder.UseDataProtection(
                this.GetService<ILookupProtectorKeyRing>(),
                this.GetService<ILookupProtector>());

           modelBuilder.Seed();
        }
    }
}