﻿using ITHoot.Models.Entities.Interfaces;
using System;

namespace ITHoot.Models.Entities
{
    public class Point : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        // Route point order
        public int Order { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }

        public string PlaceId { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
