﻿using System;
using ITHoot.Models.Entities.Interfaces;
using ITHoot.Models.Entities.Localization;
using Microsoft.AspNetCore.Identity;

namespace ITHoot.Models.Entities
{
    public class User : IdentityUser<int>, IEntity<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public virtual string FullName => $"{FirstName} {LastName}";

        // Signing a contract
        //public bool AgreementApproved { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        
        public int? LanguageId { get; set; }
        public virtual Language Language { get; set; }

        [ProtectedPersonalData]
        public override string NormalizedUserName { get; set; }

        [ProtectedPersonalData]
        public override string NormalizedEmail { get; set; }
    }
}