﻿using System;
using System.Collections.Generic;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities
{
    public class OrderRequest : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public string RequestNumber { get; set; }
        public string Note { get; set; }
        public DateTime DeliveryAt { get; set; }
        public double Distance { get; set; }

        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public double Weight { get; set; }

        public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();
        public virtual ICollection<Point> Points { get; set; } = new HashSet<Point>();
    }
}