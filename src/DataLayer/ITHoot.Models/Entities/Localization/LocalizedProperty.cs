﻿using System;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities.Localization
{
    public class LocalizedProperty : IEntity<int>
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int LanguageId { get; set; }

        public string LocaleKeyGroup { get; set; }
        public string LocaleKey { get; set; }
        public string LocaleValue { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}