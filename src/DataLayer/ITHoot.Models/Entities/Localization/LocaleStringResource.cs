﻿using System;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities.Localization
{
    public class LocaleStringResource : IEntity<int>
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }

        public string ResourceName { get; set; }
        public string ResourceValue { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}