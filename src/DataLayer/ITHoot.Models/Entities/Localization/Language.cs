﻿using System;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities.Localization
{
    public class Language : IEntity<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string LanguageCulture { get; set; }
        public string UniqueSeoCode { get; set; }

        public bool Rtl { get; set; }
        public bool Published { get; set; }
        public int DisplayOrder { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}