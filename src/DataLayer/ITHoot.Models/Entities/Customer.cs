﻿using System;
using System.Collections.Generic;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities
{
    public class Customer : IEntity<int>
    {
        public int Id { get; set; }
        //public string CompanyName { get; set; } // todo add this

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; } = new HashSet<Contract>();
    }
}