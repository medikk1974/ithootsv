﻿using System;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities
{
    public partial class Setting : IEntity<string>
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
