﻿using ITHoot.Models.Entities.Interfaces;
using System;

namespace ITHoot.Models.Entities
{
    public class Company : IEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public string Name { get; set; } // crawler PY script search https://rekvizitai.vz.lt/

        //[ProtectedPersonalData]
        public int Code { get; set; }
        public bool VATPayer { get; set; }
        public string VATCode { get; set; } // validate https://ec.europa.eu/taxation customs/vies/?locale=lt

        public int AddressId { get; set; }
        public virtual Address Address { get; set; }
    }
}