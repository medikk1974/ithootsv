﻿using ITHoot.Models.Entities.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;

namespace ITHoot.Models.Entities
{
    public class Address : IEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [ProtectedPersonalData]
        public string Country { get; set; }
        [ProtectedPersonalData]
        public string City { get; set; }
        [ProtectedPersonalData]
        public string Street { get; set; }
        [ProtectedPersonalData]
        public string Building { get; set; }
        [ProtectedPersonalData]
        public string Apartment { get; set; }

        //[ProtectedPersonalData]
        public string Latitude { get; set; }
        //[ProtectedPersonalData]
        public string Longitude { get; set; }
    }
}
