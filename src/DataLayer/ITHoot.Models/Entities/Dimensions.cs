﻿using ITHoot.Models.Entities.Interfaces;
using System;

namespace ITHoot.Models.Entities
{
    public class Dimensions : IDeletableEntity<int>
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public int Length { get; set; }
        public int Wight { get; set; }
        public int RampDegree { get; set; }
    }
}
