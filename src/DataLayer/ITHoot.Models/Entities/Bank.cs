﻿using System;
using ITHoot.Models.Entities.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace ITHoot.Models.Entities
{
    public class Bank : IEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        
        public string BankName { get; set; }

        [ProtectedPersonalData]
        public string AccountNumber { get; set; }

        public int CarrierId { get; set; }
        public virtual Carrier Carrier { get; set; }
    }
}