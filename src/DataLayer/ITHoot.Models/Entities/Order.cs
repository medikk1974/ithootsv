﻿using ITHoot.Models.Entities.Interfaces;
using System;

namespace ITHoot.Models.Entities
{
    public class Order : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }

        public int OrderRequestId { get; set; }
        public virtual OrderRequest OrderRequest { get; set; }

        public DateTime? ArrivalAt { get; set; }

        public decimal Price { get; set; }

        public Enums.OrderStatusTypes StatusTypes { get; set; } = Enums.OrderStatusTypes.Accepted;
    }
}
