﻿using System;
using System.Collections.Generic;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities
{
    public class Contract : IDeletableEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public string ContractNumber { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }

        public Enums.ContractStatusTypes StatusType { get; set; } = Enums.ContractStatusTypes.Offered;

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public int CarrierId { get; set; }
        public virtual Carrier Carrier { get; set; }

        public ICollection<Vehicle> Vehicles { get; set; } = new HashSet<Vehicle>();
    }
}
