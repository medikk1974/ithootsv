﻿using System;
using System.Collections.Generic;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities
{
    public class Vehicle : IDeletableEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }

        public decimal CityPrice { get; set; }
        public decimal HighwayPrice { get; set; }
        public int MaxLoad { get; set; }
        public string RegistrationNumber { get; set; }
        public Enums.VehicleStatusTypes StatusType { get; set; } = Enums.VehicleStatusTypes.Activated;

        public int CarrierId { get; set; }
        public virtual Carrier Carrier { get; set; }

        public int? AssignedDriverId { get; set; }
        public virtual Driver AssignedDriver { get; set; }

        public int? DimensionsId { get; set; }
        public virtual Dimensions Dimensions { get; set; }

        public int CoverageAreaId { get; set; }
        public virtual CoverageArea CoverageArea { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; } = new HashSet<Contract>();

        public virtual ICollection<Driver> VehicleDrivers { get; set; } = new HashSet<Driver>();

        //public virtual Order Order { get; set; }
    }
}