﻿using System;
using System.Collections.Generic;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Models.Entities
{
    public class Carrier : IEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        
        public Enums.CarrierTypes CarrierType { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public virtual ICollection<Driver> Drivers { get; set; } = new HashSet<Driver>();
        public virtual ICollection<Vehicle> Vehicles { get; set; } = new HashSet<Vehicle>();
        public virtual ICollection<Bank> Banks { get; set; } = new HashSet<Bank>();
    }
}