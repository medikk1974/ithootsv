﻿using ITHoot.Models.Entities.Interfaces;
using System;
using System.Collections.Generic;

namespace ITHoot.Models.Entities
{
    public class Driver : IDeletableEntity<int>
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        //[ProtectedPersonalData]
        //public string PersonalCode { get; set; }

        public Enums.DriverStatusTypes StatusType { get; set; } = Enums.DriverStatusTypes.Activated;

        //public string VATCode { get; set; } // validate https://ec.europa.eu/taxation_customs/vies/faq.html // LT999999999 or LT999999999999

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual Vehicle AssignedVehicle { get; set; }

        public int CarrierId { get; set; }
        public virtual Carrier Carrier { get; set; }

        public virtual ICollection<Vehicle> DriverVehicles { get; set; } = new HashSet<Vehicle>();
    }
}