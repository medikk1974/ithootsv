﻿using System.ComponentModel;

namespace ITHoot.Models
{
    public class Enums
    {
        public enum LanguageTypes
        {
            [Description("en-US")] English = 1,
            [Description("lt-LT")] Lithuanian = 2,
        }

        public enum RoleTypes
        {
            [Description("Admin")] Admin = 1,
            [Description("NaturalPerson")] NaturalPerson = 2,
            [Description("Company")] Company = 3,
            [Description("Driver")] Driver = 4,
            [Description("Customer")] Customer = 5,
        }

        public enum CarrierTypes
        {
            [Description("Company")] Company = 1,
            [Description("Driver")] Driver = 2,
        }

        public enum VehicleServiceTypes
        {
            [Description("Bulk cargo")] BulkCargo = 2,
            [Description("Trawling services")] TrawlingServices = 1,
        }

        public enum BulkCargoVehicleTypes
        {
            [Description("Truck with semitrailer")] TruckSemitrailer = 1,
            [Description("Four-wheel tipper")] FourWheel = 2,
            [Description("Three-axle tipper")] ThreeAxle = 3,
            [Description("Small two-axle tipper")] TwoAxle = 4,
            [Description("Two-axle tipper with semi-trailer")] TwoAxleSemitrailer = 5,
            [Description("Three-axle tipper with semi-trailer")] TreeAxleSemitrailer = 6,
            [Description("Five-axle tipper")] Semitrailer = 7,
        }

        public enum TrawlingVehicleTypes
        {
            [Description("Trawl - low beam")] LowBeam = 1,
            [Description("Trawl - tow truck")] TowTruck = 2,
        }

        public enum DriverStatusTypes
        {
            [Description("Activated")] Activated = 1,
            [Description("Deactivated")] Deactivated = 2,
            //[Description("Busy")] Busy = 3,
        }

        public enum VehicleStatusTypes
        {
            [Description("Activated")] Activated = 1,
            [Description("Deactivated")] Deactivated = 2,
            //[Description("InUse")] InUse = 3,
        }

        public enum ContractStatusTypes
        {
            [Description("Offered")] Offered = 1,
            [Description("Accepted")] Accepted = 2,
            [Description("Rejected")] Rejected = 3,
        }

        public enum OrderStatusTypes
        {
            [Description("Accepted")] Accepted = 1,
            [Description("Completed")] Completed = 2,
        }

        public enum DriverOrdersSelector
        {
            [Description("Today")] Today = 1,
            [Description("Coming")] Coming = 2,
        }
    }
}
