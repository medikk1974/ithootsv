﻿namespace ITHoot.Application.Confirmation.Responses
{
    public class SendPdfContractResult
    {
        public bool Success { get; set; }
    }
}
