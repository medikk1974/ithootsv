﻿namespace ITHoot.Application.Confirmation.Responses
{
    public class SendConfirmationResponse
    {
        public bool Success { get; set; }
    }
}
