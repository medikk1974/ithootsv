﻿using System.Collections.Generic;

namespace ITHoot.Application.Confirmation.Responses
{
    public class ConfirmationResult
    {
        public bool Success { get; set; }
        public IEnumerable<string> Errors { get; set; } = new List<string>();
    }
}