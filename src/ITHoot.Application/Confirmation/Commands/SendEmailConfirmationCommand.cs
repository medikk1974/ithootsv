﻿using ITHoot.Application.Confirmation.Responses;
using MediatR;

namespace ITHoot.Application.Confirmation.Commands
{
    public class SendEmailConfirmationCommand : IRequest<SendConfirmationResponse>
    {
        public string UserId { get; set; }
    }
}
