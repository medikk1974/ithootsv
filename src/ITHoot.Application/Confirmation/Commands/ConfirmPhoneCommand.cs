﻿using ITHoot.Application.Confirmation.Responses;
using MediatR;

namespace ITHoot.Application.Confirmation.Commands
{
    public class ConfirmPhoneCommand : IRequest<ConfirmationResult>
    {
        public string UserId { get; set; }
        public string Code { get; set; }
    }
}
