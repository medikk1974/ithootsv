﻿using ITHoot.Application.Confirmation.Responses;
using MediatR;

namespace ITHoot.Application.Confirmation.Commands
{
    public class SendPdfContractCommand : IRequest<SendPdfContractResult>
    {
        public int UserId { get; set; }
    }
}