﻿using System.Linq;
using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.Extensions;
using ITHoot.Application.SMS.Models;
using Microsoft.AspNetCore.Identity;

namespace ITHoot.Application.Confirmation.Mappers
{
    public class ConfirmationMapper : AppMapperBase
    {
        public ConfirmationMapper()
        {
            CreateMap<SendSMSResult, SendConfirmationResponse>();

            CreateMap<IdentityResult, ConfirmationResult>()
                .Map(r => r.Success, p => p.Succeeded)
                .Map(r => r.Errors, p => p.Errors.Select(r => $"{r.Code} {r.Description}"));
        }
    }
}
