﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Confirmation.Commands;
using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.Email;
using ITHoot.Application.Email.Models;
using ITHoot.Application.User.Manager;
using MediatR;

namespace ITHoot.Application.Confirmation.Handlers
{
    public class SendEmailConfirmationHandler : IRequestHandler<SendEmailConfirmationCommand, SendConfirmationResponse>
    {
        private readonly IAppUserManager _userManager;
        private readonly IEmailSender _emailSender;
        private readonly IMapper _mapper;

        public SendEmailConfirmationHandler(
            IAppUserManager userManager,
            IEmailSender emailSender,
            IMapper mapper)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _mapper = mapper;
        }

        public async Task<SendConfirmationResponse> Handle(SendEmailConfirmationCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var address = new EmailAddress(user.Email, user.FullName);
            var result = await _emailSender.SendEmailConfirmAsync(address, code);
            return _mapper.Map<SendConfirmationResponse>(result);
        }
    }
}
