﻿using System;
using System.IO;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Confirmation.Commands;
using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.Email;
using ITHoot.Application.Email.Models;
using ITHoot.Application.User.Manager;
using MediatR;

namespace ITHoot.Application.Confirmation.Handlers
{
    public class SendPdfContractHandler : IRequestHandler<SendPdfContractCommand, SendPdfContractResult>
    {
        private readonly IAppUserManager _userManager;
        private readonly IEmailSender _emailSender;

        public SendPdfContractHandler(
            IAppUserManager userManager,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
        }

        public async Task<SendPdfContractResult> Handle(SendPdfContractCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId.ToString());
            var address = new EmailAddress(user.Email, user.FullName);

            // todo request html template
            // todo use razor render
            // todo use to gen pdf -> Haukcode.WkHtmlToPdfDotNet / rdvojmoc/DinkToPdf
            var fileData = new byte[10];
            
            // todo request file name
            var filename = $"Contract for {user.FullName} from {user.CreatedAt}.pdf";
            
            var attachment = new Attachment
            {
                IsInline = true,
                Filename = filename,
                ContentType = MediaTypeNames.Application.Pdf,
                Data = new MemoryStream(fileData)
            };

            throw new NotImplementedException();

            //var result = await _emailSender.SendEmailPdfAsync(address, attachment);
            //return new SendPdfContractResult { Success = result.Success };
        }
    }
}