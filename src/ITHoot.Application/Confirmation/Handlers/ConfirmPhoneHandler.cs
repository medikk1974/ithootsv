﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Confirmation.Commands;
using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.User.Manager;
using MediatR;

namespace ITHoot.Application.Confirmation.Handlers
{
    public class ConfirmPhoneHandler : IRequestHandler<ConfirmPhoneCommand, ConfirmationResult>
    {
        private readonly IAppUserManager _userManager;
        private readonly IMapper _mapper;

        public ConfirmPhoneHandler(
            IAppUserManager userManager,
            IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<ConfirmationResult> Handle(ConfirmPhoneCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            var result = await _userManager.ChangePhoneNumberAsync(user, user.PhoneNumber, request.Code);
            return _mapper.Map<ConfirmationResult>(result);
        }
    }
}
