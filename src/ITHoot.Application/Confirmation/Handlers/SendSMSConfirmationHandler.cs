﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Confirmation.Commands;
using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.SMS;
using ITHoot.Application.User.Manager;
using MediatR;

namespace ITHoot.Application.Confirmation.Handlers
{
    public class SendSMSConfirmationHandler : IRequestHandler<SendSMSConfirmationCommand, SendConfirmationResponse>
    {
        private readonly IAppUserManager _userManager;
        private readonly ISMSSender _smsSender;
        private readonly IMapper _mapper;

        public SendSMSConfirmationHandler(
            IAppUserManager userManager,
            ISMSSender smsSender,
            IMapper mapper)
        {
            _userManager = userManager;
            _smsSender = smsSender;
            _mapper = mapper;
        }

        public async Task<SendConfirmationResponse> Handle(SendSMSConfirmationCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);
            var result = await _smsSender.SendSMSConfirmationAsync(user.PhoneNumber, code);
            return _mapper.Map<SendConfirmationResponse>(result);
        }
    }
}
