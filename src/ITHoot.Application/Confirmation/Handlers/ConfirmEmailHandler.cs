﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Confirmation.Commands;
using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.User.Manager;
using MediatR;

namespace ITHoot.Application.Confirmation.Handlers
{
    public class ConfirmEmailHandler : IRequestHandler<ConfirmEmailCommand, ConfirmationResult>
    {
        private readonly IAppUserManager _userManager;
        private readonly IMapper _mapper;

        public ConfirmEmailHandler(
            IAppUserManager userManager,
            IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<ConfirmationResult> Handle(ConfirmEmailCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(request.UserId);
            var result = await _userManager.ConfirmEmailAsync(user, request.Code);
            return _mapper.Map<ConfirmationResult>(result);
        }
    }
}
