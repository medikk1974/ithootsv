﻿using ITHoot.Application.Localization.Queries;
using ITHoot.EFContext.Extensions;
using ITHoot.Models;
using ITHoot.Models.Entities.Localization;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ITHoot.Application
{
    public partial class WebWorkContext : IWorkContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMediator _mediator;

        private Language _cachedLanguage; // todo it's not worked

        public WebWorkContext(
            IHttpContextAccessor httpContextAccessor,
            IMediator mediator)
        {
            _httpContextAccessor = httpContextAccessor;
            _mediator = mediator;
        }
        
        public virtual async Task<Language> GetWorkingLanguageAsync()
        {
            if (_cachedLanguage != null)
            {
                return _cachedLanguage;
            }

            var acceptLanguageHeader = _httpContextAccessor.HttpContext.Request
                .GetTypedHeaders()
                .AcceptLanguage;

            var languageCulture = acceptLanguageHeader == null || !acceptLanguageHeader.Any()
                ? Constants.DefaultLanguage.ToDescription()
                : acceptLanguageHeader.First().ToString();

            _cachedLanguage = await _mediator.Send(new GetLanguageByCultureQuery(languageCulture));
            return _cachedLanguage;
        }

        public async Task SetWorkingLanguageAsync(Language language)
        {
            throw new NotImplementedException();

            /*var user = await GetCurrentUserAsync();
            //await _genericAttributeService.SaveAttributeAsync(
            //    customer, 
            //    NopCustomerDefaults.LanguageIdAttribute, language?.Id ?? 0, store.Id);

            //then reset the cached value
            _cachedLanguage = null;*/
        }
    }
}
