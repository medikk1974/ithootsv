﻿using ITHoot.Models;

namespace ITHoot.Application.Vehicle.Responses
{
    public class GetVehiclesForDriverResult
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public int MaxLoad { get; set; }
        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }
    }
}
