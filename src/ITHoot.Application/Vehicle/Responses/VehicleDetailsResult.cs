﻿using ITHoot.Models;
using System.Collections.Generic;

namespace ITHoot.Application.Vehicle.Responses
{
    public class VehicleDetailsResult
    {
        public int Id { get; set; }
        public Enums.VehicleStatusTypes StatusType { get; set; }
        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }
        public int MaxLoad { get; set; }
        public decimal CityPrice { get; set; }
        public decimal HighwayPrice { get; set; }
        public string RegistrationNumber { get; set; }

        public IEnumerable<VehiclesDriverDetailsResult> VehicleDrivers { get; set; } = new List<VehiclesDriverDetailsResult>();
        public VehiclesDriverDetailsResult AssignedDriver { get; set; }
        public DimensionsResult VehicleDimensions { get; set; }
        public CoverageAreaResult VehicleCoverageArea { get; set; }
    }
}
