﻿namespace ITHoot.Application.Vehicle.Responses
{
    public class VehiclesDriverDetailsResult
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
