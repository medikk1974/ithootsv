﻿using System.Collections.Generic;

namespace ITHoot.Application.Vehicle.Responses
{
    public class GetAllVehiclesResult
    {
        public IEnumerable<VehicleDetailsResult> Items { get; set; } = new List<VehicleDetailsResult>();
    }
}
