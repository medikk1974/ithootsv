﻿namespace ITHoot.Application.Vehicle.Responses
{
    public class DimensionsResult
    {
        public int Length { get; set; }
        public int Wight { get; set; }
        public int RampDegree { get; set; }
    }
}
