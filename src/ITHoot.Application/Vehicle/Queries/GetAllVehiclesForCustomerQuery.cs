﻿using ITHoot.Application.Vehicle.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Vehicle.Queries
{
    public class GetAllVehiclesForCustomerQuery : IRequest<GetAllVehiclesResult>
    {
        public int CarrierId { get; set; }
        public Enums.VehicleServiceTypes? ServiceType { get; set; }
        //public int UserId { get; set; }
    }
}
