﻿using ITHoot.Application.Vehicle.Responses;
using MediatR;
using System.Collections.Generic;

namespace ITHoot.Application.Vehicle.Queries
{
    public  class GetVehiclesForDriverQuery : IRequest<IEnumerable<GetVehiclesForDriverResult>>
    {
        public int UserId { get; set; }
    }
}
