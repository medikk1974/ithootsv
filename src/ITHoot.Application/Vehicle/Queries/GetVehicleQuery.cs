﻿using ITHoot.Application.Vehicle.Responses;
using MediatR;

namespace ITHoot.Application.Vehicle.Queries
{
    public class GetVehicleQuery : IRequest<VehicleDetailsResult>
    {
        public int UserId { get; set; }
        public int VehicleId { get; set; }
    }
}
