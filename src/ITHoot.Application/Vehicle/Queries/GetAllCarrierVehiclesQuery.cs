﻿using ITHoot.Application.Vehicle.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Vehicle.Queries
{
    public class GetAllCarrierVehiclesQuery : IRequest<GetAllVehiclesResult>
    {
        public int UserId { get; set; }
        public Enums.VehicleStatusTypes? StatusType { get; set; }
    }
}