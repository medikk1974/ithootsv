﻿using ITHoot.Application.Extensions;
using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;

namespace ITHoot.Application.Vehicle.Mappers
{
    public class VehicleMapper : AppMapperBase
    {
        public VehicleMapper()
        {
            CreateMap<AddVehicleCommand, Models.Entities.Vehicle>()
                .Map(m => m.Dimensions, r => r.VehicleDimensions)
                .Map(m => m.CoverageArea, r => r.VehicleCoverageArea);

            CreateMap<EditVehicleCommand, Models.Entities.Vehicle>()
                .Map(m => m.Dimensions, r => r.VehicleDimensions)
                .Map(m => m.CoverageArea, r => r.VehicleCoverageArea);

            CreateMap<DimensionsCommand, Models.Entities.Dimensions>();

            CreateMap<CoverageAreaCommand, Models.Entities.CoverageArea>();

            //CreateMap<EditDimensionsCommand, Models.Entities.Dimensions>();

            CreateMap<Models.Entities.Vehicle, VehicleDetailsResult>()
                .Map(m => m.AssignedDriver, r => r.AssignedDriver)
                .Map(m => m.VehicleDrivers, r => r.VehicleDrivers)
                .Map(m => m.VehicleDimensions, r => r.Dimensions)
                .Map(m => m.VehicleCoverageArea, r => r.CoverageArea);

            CreateMap<Models.Entities.Driver, VehiclesDriverDetailsResult>()
                .Map(m => m.FirstName, r => r.User.FirstName)
                .Map(m => m.LastName, r => r.User.LastName)
                .Map(m => m.PhoneNumber, r => r.User.PhoneNumber);

            CreateMap<Models.Entities.Dimensions, DimensionsResult>();

            CreateMap<Models.Entities.CoverageArea, CoverageAreaResult>();

            CreateMap<Models.Entities.Vehicle, GetVehiclesForDriverResult>();

        }
    }
}
