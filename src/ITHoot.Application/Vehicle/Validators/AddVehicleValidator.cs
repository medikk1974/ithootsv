﻿using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Validators.Helpers;
using ITHoot.Repository;
using FluentValidation;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Validators
{
    public class AddVehicleValidator : AbstractValidator<AddVehicleCommand>
    {
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public AddVehicleValidator(IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;

            Include(new EditVehicleBaseValidator());
            Include(new ServiceTypeValidator());
            Include(new DimensionsValidator());

            RuleFor(c => c.RegistrationNumber)
                .NotEmpty()
                //.Matches("")
                .MustAsync(ValidateRegistrationNumberExistAsync)
                .WithMessage("Registration number already exists");

            RuleFor(c => c.VehicleCoverageArea)
                .SetValidator(new CoverageAreaValidator());
        }

        private async Task<bool> ValidateRegistrationNumberExistAsync(string registrationNumber, CancellationToken cancellationToken)
        {
            return !await _vehicleRepository.AnyAsync(r => r.RegistrationNumber == registrationNumber);
        }
    }
}