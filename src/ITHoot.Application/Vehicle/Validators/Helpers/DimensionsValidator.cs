﻿using ITHoot.Models;
using FluentValidation;
using System;
using ITHoot.Application.Vehicle.Commands;

namespace ITHoot.Application.Vehicle.Validators.Helpers
{
    public class DimensionsValidator : AbstractValidator<EditVehicleBaseCommand>
    {
        public DimensionsValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(c => c.VehicleDimensions)
                .Empty()
                .When(c => c.VehicleServiceType != Enums.VehicleServiceTypes.TrawlingServices);

            RuleFor(c => c.VehicleDimensions)
                .NotEmpty()
                .When(IsTrawling());

            RuleFor(c => c.VehicleDimensions.Length)
                .GreaterThan(0)
                .When(IsTrawling());
            RuleFor(c => c.VehicleDimensions.Wight)
                .GreaterThan(0)
                .When(IsTrawling());
            RuleFor(c => c.VehicleDimensions.RampDegree)
                .GreaterThan(0)
                .When(IsTrawling());
        }

        private static Func<EditVehicleBaseCommand, bool> IsTrawling()
        {
            return c => c.VehicleServiceType == Enums.VehicleServiceTypes.TrawlingServices;
        }
    }
}
