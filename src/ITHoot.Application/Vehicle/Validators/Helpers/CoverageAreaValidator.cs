﻿using ITHoot.Application.Vehicle.Commands;
using FluentValidation;

namespace ITHoot.Application.Vehicle.Validators.Helpers
{
    public class CoverageAreaValidator : AbstractValidator<CoverageAreaCommand>
    {
        public CoverageAreaValidator()
        {
            RuleFor(x => x)
                .NotEmpty();

            RuleFor(x => x.WorkRadius)
                .NotEmpty();

            RuleFor(x => x.Country)
                .NotEmpty();

            RuleFor(x => x.City)
                .NotEmpty();

            //RuleFor(x => x.PlaceId)
            //    .NotEmpty();

            //RuleFor(x => x.Latitude)
            //    .NotEmpty();

            //RuleFor(x => x.Longitude)
            //    .NotEmpty();
        }
    }
}
