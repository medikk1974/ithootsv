﻿using ITHoot.Application.Vehicle.Commands;
using ITHoot.Models;
using FluentValidation;
using System;

namespace ITHoot.Application.Vehicle.Validators.Helpers
{
    public class ServiceTypeValidator : AbstractValidator<EditVehicleBaseCommand>
    {
        public ServiceTypeValidator()
        {
            RuleFor(c => c.VehicleServiceType)
                .IsInEnum();
            RuleFor(c => c)
                .Must(ValidateVehicleType)
                .WithMessage("Vehicle type is invalid");
        }

        private static bool ValidateVehicleType(EditVehicleBaseCommand command)
        {
            return command.VehicleServiceType switch
            {
                Enums.VehicleServiceTypes.BulkCargo => Enum.IsDefined(typeof(Enums.BulkCargoVehicleTypes), command.VehicleType),
                Enums.VehicleServiceTypes.TrawlingServices => Enum.IsDefined(typeof(Enums.TrawlingVehicleTypes), command.VehicleType),
                _ => true,
            };
        }
    }
}
