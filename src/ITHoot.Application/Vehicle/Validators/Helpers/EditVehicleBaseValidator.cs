﻿using ITHoot.Application.Vehicle.Commands;
using FluentValidation;

namespace ITHoot.Application.Vehicle.Validators.Helpers
{
    public class EditVehicleBaseValidator : AbstractValidator<EditVehicleBaseCommand>
    {
        public EditVehicleBaseValidator()
        {
            RuleFor(c => c.UserId)
                .GreaterThan(0);

            RuleFor(c => c.CityPrice)
                .GreaterThan(0);

            RuleFor(c => c.HighwayPrice)
                .GreaterThan(0);

            RuleFor(c => c.MaxLoad)
                .GreaterThan(0);
        }
    }
}
