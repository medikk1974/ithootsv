﻿using ITHoot.Application.Vehicle.Commands;
using FluentValidation;

namespace ITHoot.Application.Vehicle.Validators
{
    public class ChangeVehicleStatusValidator : AbstractValidator<ChangeVehicleStatusCommand>
    {
        public ChangeVehicleStatusValidator()
        {
            RuleFor(c => c.UserId)
                .GreaterThan(0);

            RuleFor(c => c.VehicleId)
                .GreaterThan(0);

            RuleFor(c => c.StatusType)
                .IsInEnum();
        }
    }
}