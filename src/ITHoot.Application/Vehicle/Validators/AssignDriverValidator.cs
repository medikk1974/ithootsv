﻿using ITHoot.Application.Vehicle.Commands;
using FluentValidation;

namespace ITHoot.Application.Vehicle.Validators
{
    public class AssignDriverValidator : AbstractValidator<AssignVehicleDriversCommand>
    {
        public AssignDriverValidator()
        {
            RuleFor(c => c.UserId)
                .GreaterThan(0);

            RuleFor(c => c.VehicleId)
                .GreaterThan(0);
        }
    }
}