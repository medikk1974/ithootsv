﻿using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Validators.Helpers;
using ITHoot.Repository;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Validators
{
    public class EditVehicleValidator : AbstractValidator<EditVehicleCommand>
    {
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public EditVehicleValidator(IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;

            Include(new EditVehicleBaseValidator());
            Include(new ServiceTypeValidator());
            Include(new DimensionsValidator());

            RuleFor(c => c.VehicleId)
                .GreaterThan(0);

            RuleFor(c => c)
                .MustAsync(ValidateRegistrationNumberExistAsync)
                .WithMessage("Registration number already exists");

            RuleFor(c => c.VehicleCoverageArea)
                .SetValidator(new CoverageAreaValidator());
        }

        private async Task<bool> ValidateRegistrationNumberExistAsync(EditVehicleCommand command, CancellationToken cancellationToken)
        {
            return !await _vehicleRepository.Table
                        .Where(r => r.Id != command.VehicleId)
                        .AnyAsync(r => r.RegistrationNumber == command.RegistrationNumber,
                            cancellationToken);
        }
    }

}
