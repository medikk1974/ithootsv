﻿using ITHoot.Application.Vehicle.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Vehicle.Services
{
    public class VehicleService : IVehicleService
    {
        public void DimensionsProcessing(Models.Entities.Vehicle vehicle)
        {
            if (vehicle.VehicleServiceType != Models.Enums.VehicleServiceTypes.TrawlingServices)
            {
                //vehicle.Dimensions = null;
                return;
            }

            if (vehicle.Dimensions.Id == 0)
            {
                vehicle.Dimensions.CreatedAt = DateTime.UtcNow;
                return;
            }

            vehicle.Dimensions.UpdatedAt = DateTime.UtcNow;
        }
        
        public void AddVehicleDrivers(Models.Entities.Vehicle vehicle, IEnumerable<Models.Entities.Driver> drivers, IEnumerable<int> driverIds)
        {
            var selectedDrivers = drivers
                .Where(r => driverIds
                    .Contains(r.Id));

            if (!selectedDrivers.Select(r => r.Id).ItemsEqual(driverIds))
            {
                throw ValidationException.Build(nameof(driverIds), "Driver not found");
            }

            foreach (var item in selectedDrivers)
            {
                vehicle.VehicleDrivers.Add(item);
            }
        }
    }
}
