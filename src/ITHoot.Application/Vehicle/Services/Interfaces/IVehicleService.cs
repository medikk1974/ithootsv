﻿using System.Collections.Generic;

namespace ITHoot.Application.Vehicle.Services.Interfaces
{
    public interface IVehicleService
    {
        void DimensionsProcessing(Models.Entities.Vehicle vehicle);
        void AddVehicleDrivers(Models.Entities.Vehicle vehicle, IEnumerable<Models.Entities.Driver> drivers, IEnumerable<int> driverIds);
    }
}