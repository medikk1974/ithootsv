﻿using ITHoot.Application.Vehicle.Responses;
using MediatR;

namespace ITHoot.Application.Vehicle.Commands
{
    public class AddVehicleCommand : EditVehicleBaseCommand, IRequest<AddVehicleResult>
    {
    }
}
