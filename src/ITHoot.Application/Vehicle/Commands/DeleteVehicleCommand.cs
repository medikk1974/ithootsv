﻿using ITHoot.Application.Vehicle.Responses;
using MediatR;

namespace ITHoot.Application.Vehicle.Commands
{
    public class DeleteVehicleCommand : IRequest<DeleteVehicleResult>
    {
        public int UserId { get; set; }
        public int VehicleId { get; set; }
    }
}
