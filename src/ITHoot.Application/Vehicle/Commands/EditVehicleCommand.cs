﻿using ITHoot.Application.Vehicle.Responses;
using MediatR;

namespace ITHoot.Application.Vehicle.Commands
{
    public class EditVehicleCommand : EditVehicleBaseCommand, IRequest<EditVehicleResult>
    {
        public int VehicleId { get; set; }
    }
}
