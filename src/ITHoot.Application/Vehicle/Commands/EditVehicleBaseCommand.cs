﻿using ITHoot.Models;
using System.Collections.Generic;

namespace ITHoot.Application.Vehicle.Commands
{
    public class EditVehicleBaseCommand
    {
        public int UserId { get; set; }
        public IEnumerable<int> DriverIds { get; set; } = new List<int>();
        public decimal CityPrice { get; set; }
        public decimal HighwayPrice { get; set; }
        public int MaxLoad { get; set; }
        public string RegistrationNumber { get; set; }
        public Enums.VehicleServiceTypes VehicleServiceType { get; set; }
        public int VehicleType { get; set; }

        public DimensionsCommand VehicleDimensions { get; set; }
        public CoverageAreaCommand VehicleCoverageArea { get; set; }
    }
}