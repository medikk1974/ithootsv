﻿using System.Collections.Generic;
using ITHoot.Application.Vehicle.Responses;
using MediatR;

namespace ITHoot.Application.Vehicle.Commands
{
    public class AssignVehicleDriversCommand : IRequest<AssignVehicleDriversResult>
    {
        public int UserId { get; set; }
        public int VehicleId { get; set; }

        public IEnumerable<int> DriverIds { get; set; } = new List<int>();
    }
}
