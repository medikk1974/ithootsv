﻿using ITHoot.Application.Vehicle.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Vehicle.Commands
{
    public class ChangeVehicleStatusCommand : IRequest<ChangeVehicleStatusResult>
    {
        public int UserId { get; set; }
        public int VehicleId { get; set; }
        public Enums.VehicleStatusTypes StatusType { get; set; }
    }
}
