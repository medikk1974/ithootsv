﻿namespace ITHoot.Application.Vehicle.Commands
{
    public class DimensionsCommand
    {
        public int Length { get; set; }
        public int Wight { get; set; }
        public int RampDegree { get; set; }
    }
}
