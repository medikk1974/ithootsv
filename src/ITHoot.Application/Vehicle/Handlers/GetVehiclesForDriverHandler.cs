﻿using AutoMapper;
using ITHoot.Application.Vehicle.Queries;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class GetVehiclesForDriverHandler : IRequestHandler<GetVehiclesForDriverQuery, IEnumerable<GetVehiclesForDriverResult>>
    {
        private readonly IMapper _mapper;
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public GetVehiclesForDriverHandler(
            IMapper mapper,
            IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _mapper = mapper;
            _vehicleRepository = vehicleRepository;
        }

        public async Task<IEnumerable<GetVehiclesForDriverResult>> Handle(GetVehiclesForDriverQuery request, CancellationToken cancellationToken)
        {
            var driverVehicles = await _vehicleRepository.Table
                .Where(r => r.VehicleDrivers
                    .Any(d => d.UserId == request.UserId))
                .ToListAsync(cancellationToken);

            if (driverVehicles.Any())
            {
                return _mapper.Map<IEnumerable<GetVehiclesForDriverResult>>(driverVehicles);
            }

            var carrierVehicles = await _vehicleRepository.Table
                .Where(r => r.Carrier.Drivers
                    .Any(d => d.UserId == request.UserId))
                .ToListAsync(cancellationToken);

            return _mapper.Map<IEnumerable<GetVehiclesForDriverResult>>(carrierVehicles);
        }
    }
}
