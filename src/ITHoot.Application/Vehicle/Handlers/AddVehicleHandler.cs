﻿using AutoMapper;
using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Application.Vehicle.Services.Interfaces;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class AddVehicleHandler : IRequestHandler<AddVehicleCommand, AddVehicleResult>
    {
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;
        private readonly IRepository<Models.Entities.Carrier, int> _carrierRepository;

        public AddVehicleHandler(
            IMapper mapper,
            IVehicleService vehicleService,
            IRepository<Models.Entities.Carrier, int> carrierRepository)
        {
            _mapper = mapper;
            _vehicleService = vehicleService;
            _carrierRepository = carrierRepository;
        }

        public async Task<AddVehicleResult> Handle(AddVehicleCommand request, CancellationToken cancellationToken)
        {
            var carrier = await _carrierRepository.Table
                .Include(r => r.Drivers)
                .FirstOrDefaultAsync(r => r.UserId == request.UserId, cancellationToken);

            var vehicle = _mapper.Map<Models.Entities.Vehicle>(request);
            vehicle.CreatedAt = DateTime.UtcNow;
            vehicle.CoverageArea.CreatedAt = DateTime.UtcNow;

            _vehicleService.AddVehicleDrivers(vehicle, carrier.Drivers, request.DriverIds);

            if (vehicle.Dimensions != null)
                vehicle.Dimensions.CreatedAt = DateTime.UtcNow;

            carrier.Vehicles.Add(vehicle);
            await _carrierRepository.SaveChangesAsync();

            return new AddVehicleResult();
        }
    }
}