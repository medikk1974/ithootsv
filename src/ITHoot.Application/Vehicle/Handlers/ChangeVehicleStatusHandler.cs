﻿using ITHoot.Application.Extensions;
using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class ChangeVehicleStatusHandler : IRequestHandler<ChangeVehicleStatusCommand, ChangeVehicleStatusResult>
    {
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public ChangeVehicleStatusHandler(IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        public async Task<ChangeVehicleStatusResult> Handle(ChangeVehicleStatusCommand request, CancellationToken cancellationToken)
        {
            var vehicle = await _vehicleRepository.Table
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.VehicleId, cancellationToken);

            if (vehicle == null)
            {
               throw ValidationException.Build(nameof(request.VehicleId), "Vehicle not found");
            }

            vehicle.StatusType = request.StatusType;
            
            await _vehicleRepository.SaveChangesAsync();

            return new ChangeVehicleStatusResult();
        }
    }
}