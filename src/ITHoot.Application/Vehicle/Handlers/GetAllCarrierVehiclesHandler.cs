﻿using AutoMapper;
using ITHoot.Application.Vehicle.Queries;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class GetAllCarrierVehiclesHandler : IRequestHandler<GetAllCarrierVehiclesQuery, GetAllVehiclesResult>
    {
        private readonly IMapper _mapper;
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public GetAllCarrierVehiclesHandler(
            IMapper mapper,
            IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _mapper = mapper;
            _vehicleRepository = vehicleRepository;
        }

        public async Task<GetAllVehiclesResult> Handle(GetAllCarrierVehiclesQuery request, CancellationToken cancellationToken)
        {
            var q = _vehicleRepository.Table
                .Include(r => r.AssignedDriver)
                    .ThenInclude(r => r.User)
                .Include(r => r.VehicleDrivers)
                    .ThenInclude(r => r.User)
                .Include(r => r.CoverageArea)
                .Include(r => r.Dimensions)
                .Where(r => r.Carrier.UserId == request.UserId);

            if (request.StatusType.HasValue)
            {
                q = q.Where(r => r.StatusType == request.StatusType);
            }

            var vehicles = await q.ToListAsync(cancellationToken);

            return new GetAllVehiclesResult
            {
                Items = _mapper.Map<IEnumerable<VehicleDetailsResult>>(vehicles)
            };
        }
    }
}