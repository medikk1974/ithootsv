﻿using ITHoot.Application.Extensions;
using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Application.Vehicle.Services.Interfaces;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class AssignVehicleDriversHandler : IRequestHandler<AssignVehicleDriversCommand, AssignVehicleDriversResult>
    {
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;
        private readonly IVehicleService _vehicleService;

        public AssignVehicleDriversHandler(
            IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository,
            IVehicleService vehicleService)
        {
            _vehicleRepository = vehicleRepository;
            _vehicleService = vehicleService;
        }

        public async Task<AssignVehicleDriversResult> Handle(AssignVehicleDriversCommand request, CancellationToken cancellationToken)
        {
            var vehicle = await _vehicleRepository.Table
                .Include(r => r.VehicleDrivers)
                .Include(r => r.Carrier)
                .ThenInclude(r => r.Drivers)
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.VehicleId, cancellationToken);

            if (vehicle == null)
            {
                throw ValidationException.Build(nameof(request.VehicleId), "Vehicle not found");
            }

            vehicle.VehicleDrivers.Clear();
            _vehicleService.AddVehicleDrivers(vehicle, vehicle.Carrier.Drivers, request.DriverIds);

            await _vehicleRepository.SaveChangesAsync();

            return new AssignVehicleDriversResult();
        }
    }
}