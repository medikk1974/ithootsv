﻿using AutoMapper;
using ITHoot.Application.Vehicle.Queries;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class GetVehicleHandler : IRequestHandler<GetVehicleQuery, VehicleDetailsResult>
    {
        private readonly IMapper _mapper;
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public GetVehicleHandler(
            IMapper mapper,
            IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _mapper = mapper;
            _vehicleRepository = vehicleRepository;
        }

        public async Task<VehicleDetailsResult> Handle(GetVehicleQuery request, CancellationToken cancellationToken)
        {
            var vehicle = await _vehicleRepository.Table
                .Include(r => r.AssignedDriver)
                    .ThenInclude(r => r.User)
                .Include(r => r.VehicleDrivers)
                    .ThenInclude(r => r.User)
                .Include(r => r.Dimensions)
                .Include(r => r.CoverageArea)
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.VehicleId, cancellationToken);

            return _mapper.Map<VehicleDetailsResult>(vehicle);
        }
    }
}