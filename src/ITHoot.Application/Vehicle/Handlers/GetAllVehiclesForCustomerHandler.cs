﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Vehicle.Queries;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class GetAllVehiclesForCustomerHandler : IRequestHandler<GetAllVehiclesForCustomerQuery, GetAllVehiclesResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public GetAllVehiclesForCustomerHandler(
            IMapper mapper,
            IRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _mapper = mapper;
            _vehicleRepository = vehicleRepository;
        }

        public async Task<GetAllVehiclesResult> Handle(GetAllVehiclesForCustomerQuery request, CancellationToken cancellationToken)
        {
            var q = _vehicleRepository.Table
                .Include(r => r.AssignedDriver)
                    .ThenInclude(r => r.User)
                .Include(x => x.Dimensions)
                .Where(r => r.Carrier.Id == request.CarrierId);

            if (request.ServiceType.HasValue)
            {
                q = q.Where(r => r.VehicleServiceType == request.ServiceType);
            }

            var vehicles = await q.ToListAsync(cancellationToken); 

            return new GetAllVehiclesResult
            {
                Items = _mapper.Map<IEnumerable<VehicleDetailsResult>>(vehicles)
            };
        }
    }

}
