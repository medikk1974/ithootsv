﻿using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class DeleteVehicleHandler : IRequestHandler<DeleteVehicleCommand, DeleteVehicleResult>
    {
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public DeleteVehicleHandler(IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        public async Task<DeleteVehicleResult> Handle(DeleteVehicleCommand request, CancellationToken cancellationToken)
        {
            var vehicle = await _vehicleRepository.Table
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.VehicleId, cancellationToken);
            
            if (vehicle == null)
            {
                throw ValidationException.Build(nameof(request.VehicleId), "Vehicle not found");
            }

            await _vehicleRepository.DeleteAsync(vehicle);

            return new DeleteVehicleResult();
        }
    }
}
