﻿using AutoMapper;
using ITHoot.Application.Vehicle.Commands;
using ITHoot.Application.Vehicle.Responses;
using ITHoot.Application.Vehicle.Services.Interfaces;
using ITHoot.Repository;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Vehicle.Handlers
{
    public class EditVehicleHandler : IRequestHandler<EditVehicleCommand, EditVehicleResult>
    {
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;
        private readonly IDeletableRepository<Models.Entities.Vehicle, int> _vehicleRepository;

        public EditVehicleHandler(
            IMapper mapper,
            IVehicleService vehicleService,
            IDeletableRepository<Models.Entities.Vehicle, int> vehicleRepository)
        {
            _mapper = mapper;
            _vehicleService = vehicleService;
            _vehicleRepository = vehicleRepository;
        }

        public async Task<EditVehicleResult> Handle(EditVehicleCommand request, CancellationToken cancellationToken)
        {
            var vehicle = await _vehicleRepository.Table
                .Include(x => x.VehicleDrivers)
                .Include(r => r.Carrier)
                .ThenInclude(r => r.Drivers)
                .Include(x => x.Dimensions)
                .Include(x => x.CoverageArea)
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                        && r.Id == request.VehicleId, cancellationToken);

            if (vehicle == null)
            {
                throw ValidationException.Build(nameof(request.VehicleId), "Vehicle not found");
            }

            vehicle.VehicleDrivers.Clear();
            _vehicleService.AddVehicleDrivers(vehicle, vehicle.Carrier.Drivers, request.DriverIds);

            _mapper.Map(request, vehicle);
            vehicle.UpdatedAt = DateTime.UtcNow;

            _vehicleService.DimensionsProcessing(vehicle);

            await _vehicleRepository.UpdateAsync(vehicle);

            return new EditVehicleResult();
        }
    }
}
