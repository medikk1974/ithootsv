﻿using ITHoot.Application.Data.Queries;
using ITHoot.Application.Data.Responses;
using ITHoot.EFContext.Extensions;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Data.Handlers
{
    public class GetDriverHandler : IRequestHandler<GetDataTypesQuery, IEnumerable<DataTypeResult>>
    {
        public async Task<IEnumerable<DataTypeResult>> Handle(GetDataTypesQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(
                from Enum item in Enum.GetValues(request.EnumType)
                select new DataTypeResult
                {
                    Id = Convert.ToInt32(item),
                    Name = item.ToString(),
                    Description = item.ToDescription()
                });
        }
    }
}