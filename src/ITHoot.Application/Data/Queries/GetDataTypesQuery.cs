﻿using System;
using ITHoot.Application.Data.Responses;
using MediatR;
using System.Collections.Generic;

namespace ITHoot.Application.Data.Queries
{
    public class GetDataTypesQuery : IRequest<IEnumerable<DataTypeResult>>
    {
        public Type EnumType { get; set; }
    }
}
