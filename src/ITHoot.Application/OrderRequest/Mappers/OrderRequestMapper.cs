﻿using ITHoot.Application.OrderRequest.Responses;

namespace ITHoot.Application.OrderRequest.Mappers
{
    public class OrderRequestMapper : AppMapperBase
    {
        public OrderRequestMapper()
        {
            CreateMap<Models.Entities.OrderRequest, AcceptOrderRequestResult>();
        }
    }
}
