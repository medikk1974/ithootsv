﻿using ITHoot.Application.OrderRequest.Responses;
using MediatR;

namespace ITHoot.Application.OrderRequest.Commands
{
    public class AcceptOrderRequestCommand : IRequest<AcceptOrderRequestResult>
    {
        public int UserId { get; set; }
        public int OrderRequestId { get; set; }
    }
}
