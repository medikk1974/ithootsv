﻿using ITHoot.Application.OrderRequest.Commands;
using FluentValidation;

namespace ITHoot.Application.OrderRequest.Validators
{
    public class DriverAcceptOrderRequestValidator : AbstractValidator<AcceptOrderRequestCommand>
    {
        public DriverAcceptOrderRequestValidator()
        {
            RuleFor(x => x.OrderRequestId)
                .GreaterThan(0);
        }
    }
}