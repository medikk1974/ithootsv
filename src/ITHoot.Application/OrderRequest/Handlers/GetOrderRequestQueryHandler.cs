﻿using AutoMapper;
using ITHoot.Application.OrderRequest.Queries;
using ITHoot.Application.OrderRequest.Responses;
using ITHoot.Repository;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.OrderRequest.Handlers
{
    public class GetOrderRequestQueryHandler : IRequestHandler<GetOrderRequestQuery, GetOrderRequestResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.OrderRequest, int> _orderRequestRepository;
        public GetOrderRequestQueryHandler(
            IMapper mapper,
            IRepository<Models.Entities.OrderRequest, int> orderRequestRepository)
        {
            _mapper = mapper;
            _orderRequestRepository = orderRequestRepository;
        }

        public async Task<GetOrderRequestResult> Handle(GetOrderRequestQuery request, CancellationToken cancellationToken)
        {
            var orderRequest = await _orderRequestRepository.Table
                .Include(x => x.Points)
                .FirstOrDefaultAsync(x => 
                    // todo add check if it available for this driver
                    //x.AssignedDriver.UserId == request.UserId
                                    x.Id == request.OrderRequestId, cancellationToken);

            if (orderRequest == null)
                throw ValidationException.Build(nameof(request.OrderRequestId), "OrderRequest not found");

            throw new System.NotImplementedException();

            return _mapper.Map<GetOrderRequestResult>(orderRequest);
        }
    }
}
