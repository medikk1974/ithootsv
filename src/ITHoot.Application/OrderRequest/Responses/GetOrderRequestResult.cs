﻿using System;
using ITHoot.Application.Order.Responses;
using ITHoot.Models;

namespace ITHoot.Application.OrderRequest.Responses
{
    public class GetOrderRequestResult
    {
        public double Distance { get; set; }
        public double Price { get; set; }
        public DateTime DeliveryAt { get; set; }

        public DriverOrderPointDetailResult PointFrom { get; set; }
        public DriverOrderPointDetailResult PointTo { get; set; }
        public double Weight { get; set; }
        public Enums.VehicleServiceTypes ServiceType { get; set; }
    }
}
