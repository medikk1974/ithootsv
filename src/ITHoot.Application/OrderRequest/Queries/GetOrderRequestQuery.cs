﻿using ITHoot.Application.OrderRequest.Responses;
using MediatR;

namespace ITHoot.Application.OrderRequest.Queries
{
    public class GetOrderRequestQuery : IRequest<GetOrderRequestResult>
    {
        public int UserId { get; set; }
        public int OrderRequestId { get; set; }
    }
}
