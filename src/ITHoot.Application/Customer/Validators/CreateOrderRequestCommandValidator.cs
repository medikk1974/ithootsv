﻿using ITHoot.Application.Customer.Commands;
using FluentValidation;

namespace ITHoot.Application.Customer.Validators
{
    public class CreateOrderRequestCommandValidator : AbstractValidator<CreateOrderRequestCommand>
    {
        public CreateOrderRequestCommandValidator()
        {
            RuleFor(x => x.Weight)
                .GreaterThan(0);

            RuleFor(x => x.ServiceType)
                .IsInEnum();

            RuleFor(x => x.PointFrom)
                .NotNull();

            RuleFor(x => x.PointTo)
                .NotNull();
        }
    }
}
