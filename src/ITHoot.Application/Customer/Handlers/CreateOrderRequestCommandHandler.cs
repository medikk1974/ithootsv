﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Customer.Commands;
using ITHoot.Application.Customer.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Customer.Handlers
{
    public class CreateOrderRequestCommandHandler : IRequestHandler<CreateOrderRequestCommand, CreateOrderRequestResponse>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.OrderRequest, int> _orderRequestRepository;
        private readonly IRepository<Models.Entities.Customer, int> _customerRepository;

        public CreateOrderRequestCommandHandler(
            IRepository<Models.Entities.OrderRequest, int> orderRequestRepository,
            IMapper mapper,
            IRepository<Models.Entities.Customer, int> customerRepository)
        {
            _orderRequestRepository = orderRequestRepository;
            _mapper = mapper;
            _customerRepository = customerRepository;
        }

        public async Task<CreateOrderRequestResponse> Handle(CreateOrderRequestCommand request, CancellationToken cancellationToken)
        {
            var customer = await _customerRepository.Table
                .Where(x => x.UserId == request.UserId)
                .FirstOrDefaultAsync(cancellationToken);

            var orderRequest = _mapper.Map<Models.Entities.OrderRequest>(request);
            orderRequest.CreatedAt = DateTime.UtcNow;

            var pointFrom = _mapper.Map<Models.Entities.Point>(request.PointFrom);
            pointFrom.Order = 1;
            pointFrom.CreatedAt = DateTime.UtcNow;

            var pointTo = _mapper.Map<Models.Entities.Point>(request.PointTo);
            pointTo.Order = 2;
            pointTo.CreatedAt = DateTime.UtcNow;

            orderRequest.Points.Add(pointFrom);
            orderRequest.Points.Add(pointTo);
            
            orderRequest.CustomerId = customer.Id;

            await _orderRequestRepository.CreateAsync(orderRequest);

            return new CreateOrderRequestResponse();
        }
    }
}
