﻿using ITHoot.Application.Customer.Commands;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Customer.Mappers
{
    public class CustomerMapper : AppMapperBase
    {
        public CustomerMapper()
        {
            CreateMap<CreateOrderRequestCommand, Models.Entities.OrderRequest>()
                .Map(x=>x.VehicleServiceType, x=>x.ServiceType);

            CreateMap<CreatePointCommand, Models.Entities.Point>();
        }
    }
}
