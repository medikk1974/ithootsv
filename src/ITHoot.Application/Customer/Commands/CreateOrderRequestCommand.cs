﻿using ITHoot.Models;
using System;
using ITHoot.Application.Customer.Responses;
using MediatR;

namespace ITHoot.Application.Customer.Commands
{
    public class CreateOrderRequestCommand :IRequest<CreateOrderRequestResponse>
    {
        public int UserId { get; set; }
        public Enums.VehicleServiceTypes ServiceType { get; set; }
        public string RequestNumber { get; set; }
        public CreatePointCommand PointFrom { get; set; }
        public CreatePointCommand PointTo { get; set; }
        public DateTime DeliveryAt { get; set; }
        public string Note { get; set; }
        public double Weight { get; set; }
    }
}
