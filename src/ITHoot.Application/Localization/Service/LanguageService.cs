﻿using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Localization.Service.Interfaces;
using ITHoot.Models.Entities.Localization;
using ITHoot.Repository;
using System;
using System.Globalization;

namespace ITHoot.Application.Localization.Service
{
    internal class LanguageService : ILanguageService
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IRepository<Language, int> _languageRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public LanguageService(
            IRepository<Language, int> languageRepository,
            IStaticCacheManager staticCacheManager,
            ICacheKeyService cacheKeyService)
        {
            _languageRepository = languageRepository;
            _staticCacheManager = staticCacheManager;
            _cacheKeyService = cacheKeyService;
        }

        #region Methods

        /// <summary>
        ///     Deletes a language
        /// </summary>
        /// <param name="language">Language</param>
        public virtual void DeleteLanguage(Language language)
        {
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }

            _languageRepository.DeleteAsync(language);
        }

        /// <summary>
        ///     Inserts a language
        /// </summary>
        /// <param name="language">Language</param>
        public virtual void InsertLanguage(Language language)
        {
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }

            _languageRepository.CreateAsync(language);
        }

        /// <summary>
        ///     Updates a language
        /// </summary>
        /// <param name="language">Language</param>
        public virtual void UpdateLanguage(Language language)
        {
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }

            //update language
            _languageRepository.UpdateAsync(language);
        }

        /// <summary>
        ///     Get 2 letter ISO language code
        /// </summary>
        /// <param name="language">Language</param>
        /// <returns>ISO language code</returns>
        public virtual string GetTwoLetterIsoLanguageName(Language language)
        {
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }

            if (string.IsNullOrEmpty(language.LanguageCulture))
            {
                return "en";
            }

            var culture = new CultureInfo(language.LanguageCulture);
            var code = culture.TwoLetterISOLanguageName;

            return string.IsNullOrEmpty(code) ? "en" : code;
        }

        #endregion
    }
}