﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ITHoot.Application.Caching.Extensions;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.Localization.Queries;
using ITHoot.Application.Localization.Service.Interfaces;
using ITHoot.Models.Entities.Interfaces;
using ITHoot.Models.Entities.Localization;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Localization.Service
{
    internal class LocalizationService : ILocalizationService
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IRepository<LocaleStringResource, int> _lsrRepository;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IWorkContext _workContext;
        private readonly IMediator _mediator;

        public LocalizationService(
            ICacheKeyService cacheKeyService,
            ILocalizedEntityService localizedEntityService,
            IRepository<LocaleStringResource, int> lsrRepository,
            IStaticCacheManager staticCacheManager,
            IWorkContext workContext,
            IMediator mediator)
        {
            _cacheKeyService = cacheKeyService;
            _localizedEntityService = localizedEntityService;
            _lsrRepository = lsrRepository;
            _staticCacheManager = staticCacheManager;
            _workContext = workContext;
            _mediator = mediator;
        }

        /// <summary>
        ///     Deletes a locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        public virtual Task DeleteLocaleStringResource(LocaleStringResource localeStringResource)
        {
            if (localeStringResource == null)
            {
                throw new ArgumentNullException(nameof(localeStringResource));
            }

            return _lsrRepository.DeleteAsync(localeStringResource);
        }

        /// <summary>
        ///     Gets a locale string resource
        /// </summary>
        /// <param name="localeStringResourceId">Locale string resource identifier</param>
        /// <returns>Locale string resource</returns>
        public virtual Task<LocaleStringResource> GetLocaleStringResourceByIdAsync(int localeStringResourceId)
        {
            if (localeStringResourceId == 0)
            {
                return null;
            }

            return _lsrRepository.ToCachedGetById(_staticCacheManager, localeStringResourceId);
        }

        /// <summary>
        ///     Gets a locale string resource
        /// </summary>
        /// <param name="resourceName">A string representing a resource name</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <returns>Locale string resource</returns>
        public virtual Task<LocaleStringResource> GetLocaleStringResourceByName(string resourceName, int languageId,
            bool logIfNotFound = true)
        {
            var query = from lsr in _lsrRepository.Table
                orderby lsr.ResourceName
                where lsr.LanguageId == languageId && lsr.ResourceName == resourceName
                select lsr;

            var localeStringResource = query.FirstOrDefaultAsync();

            return localeStringResource;
        }

        /// <summary>
        ///     Inserts a locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        public virtual Task InsertLocaleStringResourceAsync(LocaleStringResource localeStringResource)
        {
            if (localeStringResource == null)
            {
                throw new ArgumentNullException(nameof(localeStringResource));
            }

            return _lsrRepository.CreateAsync(localeStringResource);
        }

        /// <summary>
        ///     Updates the locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        public virtual Task UpdateLocaleStringResourceAsync(LocaleStringResource localeStringResource)
        {
            if (localeStringResource == null)
            {
                throw new ArgumentNullException(nameof(localeStringResource));
            }

            return _lsrRepository.UpdateAsync(localeStringResource);
        }

        /// <summary>
        ///     Gets all locale string resources by language identifier
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="loadPublicLocales">
        ///     A value indicating whether to load data for the public store only (if "false", then for
        ///     admin area only. If null, then load all locales. We use it for performance optimization of the site startup
        /// </param>
        /// <returns>Locale string resources</returns>
        public virtual Dictionary<string, KeyValuePair<int, string>> GetAllResourceValuesAsync(int languageId,
            bool? loadPublicLocales)
        {
            var key = _cacheKeyService.PrepareKeyForDefaultCache(LocalizationDefaults.LocaleStringResourcesAllCacheKey,
                languageId);

            //get all locale string resources by language identifier
            if (!loadPublicLocales.HasValue || _staticCacheManager.IsSet(key))
            {
                var rez = _staticCacheManager.Get(key, () =>
                {
                    var query = from l in _lsrRepository.Table
                        orderby l.ResourceName
                        where l.LanguageId == languageId
                        select l;

                    return ResourceValuesToDictionary(query);
                });

                //remove separated resource 
                _staticCacheManager.Remove(
                    _cacheKeyService.PrepareKeyForDefaultCache(
                        LocalizationDefaults.LocaleStringResourcesAllPublicCacheKey, languageId));
                _staticCacheManager.Remove(
                    _cacheKeyService.PrepareKeyForDefaultCache(
                        LocalizationDefaults.LocaleStringResourcesAllAdminCacheKey, languageId));

                return rez;
            }

            //performance optimization of the site startup
            key = _cacheKeyService.PrepareKeyForDefaultCache(
                loadPublicLocales.Value
                    ? LocalizationDefaults.LocaleStringResourcesAllPublicCacheKey
                    : LocalizationDefaults.LocaleStringResourcesAllAdminCacheKey,
                languageId);

            return _staticCacheManager.Get(key, () =>
            {
                var query = from l in _lsrRepository.Table
                    orderby l.ResourceName
                    where l.LanguageId == languageId
                    select l;
                query = loadPublicLocales.Value
                    ? query.Where(
                        r => !r.ResourceName.StartsWith(LocalizationDefaults.AdminLocaleStringResourcesPrefix))
                    : query.Where(r =>
                        r.ResourceName.StartsWith(LocalizationDefaults.AdminLocaleStringResourcesPrefix));
                return ResourceValuesToDictionary(query);
            });
        }

        /// <summary>
        ///     Gets a resource string based on the specified ResourceKey property.
        /// </summary>
        /// <param name="resourceKey">A string representing a ResourceKey.</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="returnEmptyIfNotFound">
        ///     A value indicating whether an empty string will be returned if a resource is not
        ///     found and default value is set to empty string
        /// </param>
        /// <returns>A string representing the requested resource string.</returns>
        public virtual async Task<string> GetResourceAsync(
            string resourceKey,
            int languageId,
            bool logIfNotFound = true,
            string defaultValue = "",
            bool returnEmptyIfNotFound = false)
        {
            var result = string.Empty;
            if (resourceKey == null)
                resourceKey = string.Empty;
            resourceKey = resourceKey.Trim().ToLowerInvariant();

            //gradual loading
            var key = _cacheKeyService.PrepareKeyForDefaultCache(
                LocalizationDefaults.LocaleStringResourcesByResourceNameCacheKey
                , languageId, resourceKey);

            var query = from l in _lsrRepository.Table
                where l.ResourceName == resourceKey
                      && l.LanguageId == languageId
                select l.ResourceValue;

            var lsr = key == null
                ? await query.FirstOrDefaultAsync()
                : await _staticCacheManager.GetAsync(key, async () => await query.FirstOrDefaultAsync());

            if (lsr != null)
                result = lsr;

            if (!string.IsNullOrEmpty(result))
                return result;

            if (!string.IsNullOrEmpty(defaultValue))
            {
                result = defaultValue;
            }
            else
            {
                if (!returnEmptyIfNotFound)
                    result = resourceKey;
            }

            return result;
        }

        /// <summary>
        ///     Export language resources to XML
        /// </summary>
        /// <param name="language">Language</param>
        /// <returns>Result in XML format</returns>
        public virtual async Task<string> ExportResourcesToXmlAsync(Language language)
        {
            if (language == null)
                throw new ArgumentNullException(nameof(language));
            using var stream = new MemoryStream();
            using (var xmlWriter = new XmlTextWriter(stream, Encoding.UTF8))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("Language");
                xmlWriter.WriteAttributeString("Name", language.Name);

                var resources = await GetAllResources(language.Id);
                foreach (var resource in resources)
                {
                    xmlWriter.WriteStartElement("LocaleResource");
                    xmlWriter.WriteAttributeString("Name", resource.ResourceName);
                    xmlWriter.WriteElementString("Value", null, resource.ResourceValue);
                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }

            return Encoding.UTF8.GetString(stream.ToArray());
        }

        /// <summary>
        ///     Get localized property of an entity
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">
        ///     Language identifier; pass null to use the current working language; pass 0 to get standard
        ///     language value
        /// </param>
        /// <param name="returnDefaultValue">A value indicating whether to return default value (if localized is not found)</param>
        /// <param name="ensureTwoPublishedLanguages">
        ///     A value indicating whether to ensure that we have at least two published
        ///     languages; otherwise, load only default value
        /// </param>
        /// <returns>Localized property</returns>
        public virtual async Task<TPropType> GetLocalizedAsync<TEntity, TPropType>(TEntity entity,
            Expression<Func<TEntity, TPropType>> keySelector,
            int? languageId = null, bool returnDefaultValue = true, bool ensureTwoPublishedLanguages = true)
            where TEntity : IEntity<int>
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (!(keySelector.Body is MemberExpression member))
                throw new ArgumentException($"Expression '{keySelector}' refers to a method, not a property.");

            if (!(member.Member is PropertyInfo propInfo))
                throw new ArgumentException($"Expression '{keySelector}' refers to a field, not a property.");

            var result = default(TPropType);
            var resultStr = string.Empty;

            var localeKeyGroup = CommonHelper.GetEntityNameKey(entity.GetType().Name);
            var localeKey = propInfo.Name;

            var workingLanguage = await _workContext.GetWorkingLanguageAsync();
            languageId ??= workingLanguage.Id;

            if (languageId > 0)
            {
                //ensure that we have at least two published languages
                var loadLocalizedValue = true;
                if (ensureTwoPublishedLanguages)
                {
                    var languages = await _mediator.Send(new GetAllLanguagesQuery());
                    loadLocalizedValue = languages.Count >= 2;
                }

                //localized value
                if (loadLocalizedValue)
                {
                    resultStr = await _localizedEntityService
                        .GetLocalizedValue(languageId.Value, entity.Id, localeKeyGroup, localeKey);
                    if (!string.IsNullOrEmpty(resultStr))
                        result = CommonHelper.To<TPropType>(resultStr);
                }
            }

            //set default value if required
            if (!string.IsNullOrEmpty(resultStr) || !returnDefaultValue)
            {
                return result;
            }

            var localizer = keySelector.Compile();
            result = localizer(entity);

            return result;
        }

        /// <summary>
        ///     Get localized value of enum
        /// </summary>
        /// <typeparam name="TEnum">Enum type</typeparam>
        /// <param name="enumValue">Enum value</param>
        /// <param name="languageId">Language identifier; pass null to use the current working language</param>
        /// <returns>Localized value</returns>
        public virtual async Task<string> GetLocalizedEnumAsync<TEnum>(TEnum enumValue, int? languageId = null)
            where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            //localized value
            var workingLanguage = await _workContext.GetWorkingLanguageAsync();
            var resourceName = $"{LocalizationDefaults.EnumLocaleStringResourcesPrefix}{typeof(TEnum)}.{enumValue}";
            var result = await GetResourceAsync(resourceName, languageId ?? workingLanguage.Id, false, string.Empty,
                true);

            //set default value if required
            if (string.IsNullOrEmpty(result))
            {
                result = CommonHelper.ConvertEnum(enumValue.ToString());
            }

            return result;
        }

        Task ILocalizationService.ExportResourcesToXmlAsync(Language language, StreamReader xmlStreamReader,
            bool updateExistingResources)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Insert resources
        /// </summary>
        /// <param name="resources">Resources</param>
        protected virtual void InsertLocaleStringResources(IList<LocaleStringResource> resources)
        {
            if (resources == null)
            {
                throw new ArgumentNullException(nameof(resources));
            }

            _lsrRepository.CreateManyAsync(resources);
        }

        /// <summary>
        ///     Gets all locale string resources by language identifier
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <returns>Locale string resources</returns>
        protected virtual async Task<IList<LocaleStringResource>> GetAllResources(int languageId)
        {
            var query = from l in _lsrRepository.Table
                orderby l.ResourceName
                where l.LanguageId == languageId
                select l;

            var locales = await query.ToListAsync();

            return locales;
        }

        /// <summary>
        ///     Update resources
        /// </summary>
        /// <param name="resources">Resources</param>
        protected virtual void UpdateLocaleStringResources(IList<LocaleStringResource> resources)
        {
            if (resources == null)
                throw new ArgumentNullException(nameof(resources));

            _lsrRepository.UpdateRangeAsync(resources);
        }

        protected virtual HashSet<(string name, string value)> LoadLocaleResourcesFromStream(
            StreamReader xmlStreamReader, string language)
        {
            var result = new HashSet<(string name, string value)>();

            using (var xmlReader = XmlReader.Create(xmlStreamReader))
            {
                while (xmlReader.ReadToFollowing("Language"))
                {
                    if (xmlReader.NodeType != XmlNodeType.Element)
                        continue;

                    using var languageReader = xmlReader.ReadSubtree();
                    while (languageReader.ReadToFollowing("LocaleResource"))
                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.GetAttribute("Name") is string name)
                        {
                            using var lrReader = languageReader.ReadSubtree();
                            if (lrReader.ReadToFollowing("Value") && lrReader.NodeType == XmlNodeType.Element)
                                result.Add((name, lrReader.ReadString()));
                        }

                    break;
                }
            }

            return result;
        }

        private static Dictionary<string, KeyValuePair<int, string>> ResourceValuesToDictionary(
            IEnumerable<LocaleStringResource> locales)
        {
            //format: <name, <id, value>>
            var dictionary = new Dictionary<string, KeyValuePair<int, string>>();
            foreach (var locale in locales)
            {
                var resourceName = locale.ResourceName.ToLowerInvariant();
                if (!dictionary.ContainsKey(resourceName))
                    dictionary.Add(resourceName, new KeyValuePair<int, string>(locale.Id, locale.ResourceValue));
            }

            return dictionary;
        }

        /// <summary>
        ///     Import language resources from XML file
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="xmlStreamReader">Stream reader of XML file</param>
        /// <param name="updateExistingResources">A value indicating whether to update existing resources</param>
        public virtual void ExportResourcesToXmlAsync(Language language, StreamReader xmlStreamReader,
            bool updateExistingResources = true)
        {
            throw new NotImplementedException();
            //if (language == null)
            //	throw new ArgumentNullException(nameof(language));

            //if (xmlStreamReader.EndOfStream)
            //	return;

            //var lsNamesList = _lsrRepository.Table
            //	.Where(lsr => lsr.LanguageId == language.Id)
            //	.ToDictionary(lsr => lsr.ResourceName, lsr => lsr);


            //var qurye = _lsrRepository.
            //	GetListAsync(lsr => lsr.LanguageId == language.Id)
            //	.ToDictionary(lsr => lsr.ResourceName, lsr => lsr);

            //var lrsToUpdateList = new List<LocaleStringResource>();
            //var lrsToInsertList = new Dictionary<string, LocaleStringResource>();

            //foreach (var (name, value) in LoadLocaleResourcesFromStream(xmlStreamReader, language.Name))
            //{
            //	if (lsNamesList.ContainsKey(name))
            //	{
            //		if (!updateExistingResources)
            //			continue;

            //		var lsr = lsNamesList[name];
            //		lsr.ResourceValue = value;
            //		lrsToUpdateList.Add(lsr);
            //	}
            //	else
            //	{
            //		var lsr = new LocaleStringResource { LanguageId = language.Id, ResourceName = name, ResourceValue = value };
            //		if (lrsToInsertList.ContainsKey(name))
            //			lrsToInsertList[name] = lsr;
            //		else
            //			lrsToInsertList.Add(name, lsr);
            //	}
            //}

            //foreach (var lrsToUpdate in lrsToUpdateList)
            //	_lsrRepository.UpdateAsync(lrsToUpdate);

            //_lsrRepository.CreateManyAsync(lrsToInsertList.Values);

            ////clear cache
            //_staticCacheManager.RemoveByPrefix(LocalizationDefaults.LocaleStringResourcesPrefixCacheKey);
        }
    }
}