﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.Localization.Queries;
using ITHoot.Application.Localization.Service.Interfaces;
using ITHoot.Models.Entities.Interfaces;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Localization.Service
{
    internal class LocalizedEntityService : ILocalizedEntityService
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IMediator _mediator;
        private readonly IRepository<Models.Entities.Localization.LocalizedProperty, int> _localizedPropertyRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public LocalizedEntityService(
            ICacheKeyService cacheKeyService,
            IRepository<Models.Entities.Localization.LocalizedProperty, int> localizedPropertyRepository,
            IStaticCacheManager staticCacheManager,
            IMediator mediator)
        {
            _cacheKeyService = cacheKeyService;
            _localizedPropertyRepository = localizedPropertyRepository;
            _staticCacheManager = staticCacheManager;
            _mediator = mediator;
        }

        /// <summary>
        ///     Deletes a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        public virtual void DeleteLocalizedProperty(Models.Entities.Localization.LocalizedProperty localizedProperty)
        {
            if (localizedProperty == null)
            {
                throw new ArgumentNullException(nameof(localizedProperty));
            }

            _localizedPropertyRepository.DeleteAsync(localizedProperty);
        }

        /// <summary>
        ///     Gets a localized property
        /// </summary>
        /// <param name="localizedPropertyId">Localized property identifier</param>
        /// <returns>Localized property</returns>
        public virtual async Task<Models.Entities.Localization.LocalizedProperty> GetLocalizedPropertyById(
            int localizedPropertyId)
        {
            if (localizedPropertyId == 0)
            {
                return null;
            }

            return await _localizedPropertyRepository.GetByIdAsync(localizedPropertyId);
        }

        /// <summary>
        ///     Find localized value
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="defaultValueIfEmpty">Defult value if localized value is empty</param>
        /// <returns>Found localized value</returns>
        public virtual async Task<string> GetLocalizedValue(
            int languageId,
            int entityId,
            string localeKeyGroup,
            string localeKey,
            string defaultValueIfEmpty = null)
        {
            var key = _cacheKeyService.PrepareKeyForDefaultCache(LocalizationDefaults.LocalizedPropertyCacheKey,
                languageId, entityId, localeKeyGroup, localeKey);

            var result = await _staticCacheManager.GetAsync(key, async () =>
            {
                var source = _localizedPropertyRepository.Table;

                //little hack here. nulls aren't cacheable so set it to ""
                var query = from lp in source
                    where lp.LanguageId == languageId &&
                          lp.EntityId == entityId &&
                          lp.LocaleKeyGroup == localeKeyGroup &&
                          lp.LocaleKey == localeKey
                    select lp.LocaleValue;

                var localeValue = await query.FirstOrDefaultAsync() ?? string.Empty;

                return localeValue;
            });

            if (string.IsNullOrEmpty(result) && defaultValueIfEmpty != null) return defaultValueIfEmpty;
            return result;
        }

        /// <summary>
        ///     Inserts a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        public virtual void InsertLocalizedProperty(
            Models.Entities.Localization.LocalizedProperty localizedProperty)
        {
            if (localizedProperty == null)
            {
                throw new ArgumentNullException(nameof(localizedProperty));
            }

            _localizedPropertyRepository.CreateAsync(localizedProperty);
        }

        /// <summary>
        ///     Updates the localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        public virtual void UpdateLocalizedProperty(
            Models.Entities.Localization.LocalizedProperty localizedProperty)
        {
            if (localizedProperty == null)
                throw new ArgumentNullException(nameof(localizedProperty));

            _localizedPropertyRepository.UpdateAsync(localizedProperty);

            var key = _cacheKeyService.PrepareKeyForDefaultCache(LocalizationDefaults.LocalizedPropertyCacheKey
                , localizedProperty.LanguageId, localizedProperty.EntityId, localizedProperty.LocaleKeyGroup,
                localizedProperty.LocaleKey);

            if (_staticCacheManager.IsSet(key)) _staticCacheManager.Set(key, localizedProperty.LocaleValue);
            ;
        }

        /// <summary>
        ///     Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        public virtual Task SaveLocalizedValue<T>(T entity,
            Expression<Func<T, string>> keySelector,
            string localeValue,
            int languageId) where T : IEntity<int>
        {
            return SaveLocalizedValue<T, string>(entity, keySelector, localeValue, languageId);
        }

        /// <summary>
        ///     Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        public virtual async Task SaveLocalizedValue<T, TPropType>(T entity,
            Expression<Func<T, TPropType>> keySelector,
            TPropType localeValue,
            int languageId) where T : IEntity<int>
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (languageId == 0)
                throw new ArgumentOutOfRangeException(nameof(languageId), "Language ID should not be 0");

            if (!(keySelector.Body is MemberExpression member))
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    keySelector));

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    keySelector));

            //load localized value (check whether it's a cacheable entity. In such cases we load its original entity type)
            var localeKeyGroup = CommonHelper.GetEntityNameKey(entity.GetType().Name);
            var localeKey = propInfo.Name;

            var props = GetLocalizedProperties(entity.Id, localeKeyGroup).Result;
            var prop = props.FirstOrDefault(lp => lp.LanguageId == languageId &&
                                                  lp.LocaleKey.Equals(localeKey,
                                                      StringComparison
                                                          .InvariantCultureIgnoreCase)); //should be culture invariant

            var localeValueStr = CommonHelper.To<string>(localeValue);

            if (prop != null)
            {
                if (string.IsNullOrWhiteSpace(localeValueStr))
                {
                    //delete
                    DeleteLocalizedProperty(prop);
                }
                else
                {
                    //update
                    prop.LocaleValue = localeValueStr;
                    UpdateLocalizedProperty(prop);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(localeValueStr))
                    return;

                //insert
                prop = new Models.Entities.Localization.LocalizedProperty
                {
                    EntityId = entity.Id,
                    LanguageId = languageId,
                    LocaleKey = localeKey,
                    LocaleKeyGroup = localeKeyGroup,
                    LocaleValue = localeValueStr,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                };
                InsertLocalizedProperty(prop);
            }
        }

        /// <summary>
        ///     Update or create translation values (translate to all langues & save)
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <returns>All language localized properties</returns>
        public virtual async Task<List<Models.Entities.Localization.LocalizedProperty>>
            UpdateTranslationValues<T>(T entity,
                Expression<Func<T, string>> keySelector,
                string sourceText) where T : IEntity<int>
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (!(keySelector.Body is MemberExpression member))
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    keySelector));

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    keySelector));

            var resultList = new List<Models.Entities.Localization.LocalizedProperty>();

            throw new NotImplementedException();
            // todo impl without googleTranslate service

            try
            {
                var languages = await _mediator.Send(new GetAllLanguagesQuery());

                /*var sourceTextLanguage = _googleTranslate.DetectLanguageAsync(sourceText).Result;

                if (sourceTextLanguage.Equals("iw")) sourceTextLanguage = "he";

                var sourceLang = new Language();
                foreach (var lang in languages)
                {
                    var twoLeterIsoLang = _languageService.GetTwoLetterIsoLanguageName(lang);
                    if (twoLeterIsoLang.Equals(sourceTextLanguage)) sourceLang = lang;
                }

                //load localized value (check whether it's a cacheable entity. In such cases we load its original entity type)
                var localeKeyGroup = CommonHelper.GetEntityNameKey(entity.GetType().Name);
                var localeKey = propInfo.Name;

                var allList = GetLocalizedProperties(entity.Id, localeKeyGroup).Result;
                var propKeyList = allList.Where(lp =>
                    lp.LocaleKey.Equals(localeKey, StringComparison.InvariantCultureIgnoreCase)).ToList();

                if (sourceLang.Id != 0)
                {
                    var savedProperty = propKeyList.FirstOrDefault(pkl => pkl.LanguageId == sourceLang.Id);

                    if (savedProperty != null && savedProperty.LocaleValue.Equals(sourceText)) return propKeyList;
                }

                string translatedText = null;
                var newPropKeyList = new List<Core.Entities.Localization.LocalizedProperty>();

                foreach (var lang in languages)
                {
                    var twoLeterIsoLangCode = _languageService.GetTwoLetterIsoLanguageName(lang);
                    if (twoLeterIsoLangCode.Equals(sourceTextLanguage))
                        translatedText = sourceText;
                    else
                        translatedText = await _googleTranslate.TranslateAsync(sourceText, lang.UniqueSeoCode);

                    var propKey = propKeyList.FirstOrDefault(pk => pk.LanguageId == lang.Id);
                    if (propKey != null)
                    {
                        propKey.LocaleValue = translatedText;
                        propKey = await _localizedPropertyRepository.UpdateAsync(propKey);
                        resultList.Add(propKey);
                    }
                    else
                    {
                        newPropKeyList.Add(new Core.Entities.Localization.LocalizedProperty
                        {
                            LocaleValue = translatedText,
                            LanguageId = lang.Id,
                            EntityId = entity.Id,
                            LocaleKey = localeKey,
                            LocaleKeyGroup = localeKeyGroup,
                            CreatedAt = DateTime.Now,
                            UpdatedAt = DateTime.Now
                        });
                    }
                }

                if (newPropKeyList.Count > 0)
                {
                    var createdMany = await _localizedPropertyRepository.CreateManyAsync(newPropKeyList);
                    resultList.AddRange(createdMany.ToList());
                }*/

                return resultList;
            }
            catch
            {
                return resultList;
            }
        }

        /// <summary>
        ///     Gets localized properties
        /// </summary>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <returns>Localized properties</returns>
        protected virtual async Task<IList<Models.Entities.Localization.LocalizedProperty>>
            GetLocalizedProperties(int entityId, string localeKeyGroup)
        {
            if (entityId == 0 || string.IsNullOrEmpty(localeKeyGroup))
                return new List<Models.Entities.Localization.LocalizedProperty>();

            var query = from lp in _localizedPropertyRepository.Table
                orderby lp.Id
                where lp.EntityId == entityId &&
                      lp.LocaleKeyGroup == localeKeyGroup
                select lp;
            var sqlString = query.ToQueryString();
            var props = await query.ToListAsync();

            return props;
        }

        /// <summary>
        ///     Gets all cached localized properties
        /// </summary>
        /// <returns>Cached localized properties</returns>
        protected virtual async Task<IList<Models.Entities.Localization.LocalizedProperty>>
            GetAllLocalizedProperties()
        {
            var query = from lp in _localizedPropertyRepository.Table
                select lp;

            //cacheable copy
            var key = _cacheKeyService.PrepareKeyForDefaultCache(LocalizationDefaults.LocalizedPropertyAllCacheKey);

            var properties = await _staticCacheManager.GetAsync(key, async () =>
            {
                var props = await query.ToListAsync();

                return props;
            });

            return properties;
        }
    }
}