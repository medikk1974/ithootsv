﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ITHoot.Models.Entities.Interfaces;
using ITHoot.Models.Entities.Localization;

namespace ITHoot.Application.Localization.Service.Interfaces
{
    /// <summary>
    ///     Localization manager interface
    /// </summary>
    public interface ILocalizationService
    {
        /// <summary>
        ///     Deletes a locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        Task DeleteLocaleStringResource(LocaleStringResource localeStringResource);

        /// <summary>
        ///     Gets a locale string resource
        /// </summary>
        /// <param name="localeStringResourceId">Locale string resource identifier</param>
        /// <returns>Locale string resource</returns>
        Task<LocaleStringResource> GetLocaleStringResourceByIdAsync(int localeStringResourceId);

        /// <summary>
        ///     Gets a locale string resource
        /// </summary>
        /// <param name="resourceName">A string representing a resource name</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <returns>Locale string resource</returns>
        Task<LocaleStringResource> GetLocaleStringResourceByName(
            string resourceName,
            int languageId,
            bool logIfNotFound = true);

        /// <summary>
        ///     Inserts a locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        Task InsertLocaleStringResourceAsync(LocaleStringResource localeStringResource);

        /// <summary>
        ///     Updates the locale string resource
        /// </summary>
        /// <param name="localeStringResource">Locale string resource</param>
        Task UpdateLocaleStringResourceAsync(LocaleStringResource localeStringResource);

        /// <summary>
        ///     Gets all locale string resources by language identifier
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="loadPublicLocales">
        ///     A value indicating whether to load data for the public store only (if "false", then for
        ///     admin area only. If null, then load all locales. We use it for performance optimization of the site startup
        /// </param>
        /// <returns>Locale string resources</returns>
        Dictionary<string, KeyValuePair<int, string>> GetAllResourceValuesAsync(int languageId, bool? loadPublicLocales);
        
        /// <summary>
        ///     Gets a resource string based on the specified ResourceKey property.
        /// </summary>
        /// <param name="resourceKey">A string representing a ResourceKey.</param>
        /// <param name="languageId">Language identifier</param>
        /// <param name="logIfNotFound">A value indicating whether to log error if locale string resource is not found</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="returnEmptyIfNotFound">
        ///     A value indicating whether an empty string will be returned if a resource is not
        ///     found and default value is set to empty string
        /// </param>
        /// <returns>A string representing the requested resource string.</returns>
        Task<string> GetResourceAsync(string resourceKey, int languageId, bool logIfNotFound = true, string defaultValue = "", bool returnEmptyIfNotFound = false);

        /// <summary>
        ///     Export language resources to XML
        /// </summary>
        /// <param name="language">Language</param>
        /// <returns>Result in XML format</returns>
        Task<string> ExportResourcesToXmlAsync(Language language);

        /// <summary>
        ///     Import language resources from XML file
        /// </summary>
        /// <param name="language">Language</param>
        /// <param name="xmlStreamReader">Stream reader of XML file</param>
        /// <param name="updateExistingResources">A value indicating whether to update existing resources</param>
        Task ExportResourcesToXmlAsync(Language language, StreamReader xmlStreamReader, bool updateExistingResources = true);

        /// <summary>
        ///     Get localized property of an entity
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="languageId">
        ///     Language identifier; pass null to use the current working language; pass 0 to get standard
        ///     language value
        /// </param>
        /// <param name="returnDefaultValue">A value indicating whether to return default value (if localized is not found)</param>
        /// <param name="ensureTwoPublishedLanguages">
        ///     A value indicating whether to ensure that we have at least two published
        ///     languages; otherwise, load only default value
        /// </param>
        /// <returns>Localized property</returns>
        Task<TPropType> GetLocalizedAsync<TEntity, TPropType>(TEntity entity,
            Expression<Func<TEntity, TPropType>> keySelector,
            int? languageId = null, bool returnDefaultValue = true, bool ensureTwoPublishedLanguages = true)
            where TEntity : IEntity<int>;

        /// <summary>
        ///     Get localized value of enum
        /// </summary>
        /// <typeparam name="TEnum">Enum type</typeparam>
        /// <param name="enumValue">Enum value</param>
        /// <param name="languageId">Language identifier; pass null to use the current working language</param>
        /// <returns>Localized value</returns>
        Task<string> GetLocalizedEnumAsync<TEnum>(TEnum enumValue, int? languageId = null) where TEnum : struct;
    }
}