﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using ITHoot.Models.Entities.Interfaces;

namespace ITHoot.Application.Localization.Service.Interfaces
{
    public interface ILocalizedEntityService
    {
        /// <summary>
        ///     Deletes a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        void DeleteLocalizedProperty(Models.Entities.Localization.LocalizedProperty localizedProperty);

        /// <summary>
        ///     Gets a localized property
        /// </summary>
        /// <param name="localizedPropertyId">Localized property identifier</param>
        /// <returns>Localized property</returns>
        Task<Models.Entities.Localization.LocalizedProperty> GetLocalizedPropertyById(int localizedPropertyId);

        /// <summary>
        ///     Find localized value
        /// </summary>
        /// <param name="languageId">Language identifier</param>
        /// <param name="entityId">Entity identifier</param>
        /// <param name="localeKeyGroup">Locale key group</param>
        /// <param name="localeKey">Locale key</param>
        /// <param name="defaultValueIfEmpty">Defult value if localized value is empty</param>
        /// <returns>Found localized value</returns>
        Task<string> GetLocalizedValue(int languageId, int entityId, string localeKeyGroup, string localeKey,
            string defaultValueIfEmpty = null);

        /// <summary>
        ///     Inserts a localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        void InsertLocalizedProperty(Models.Entities.Localization.LocalizedProperty localizedProperty);

        /// <summary>
        ///     Updates the localized property
        /// </summary>
        /// <param name="localizedProperty">Localized property</param>
        void UpdateLocalizedProperty(Models.Entities.Localization.LocalizedProperty localizedProperty);

        /// <summary>
        ///     Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        Task SaveLocalizedValue<T>(T entity,
            Expression<Func<T, string>> keySelector,
            string localeValue,
            int languageId) where T : IEntity<int>;

        /// <summary>
        ///     Save localized value
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <typeparam name="TPropType">Property type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        /// <param name="localeValue">Locale value</param>
        /// <param name="languageId">Language ID</param>
        Task SaveLocalizedValue<T, TPropType>(T entity,
            Expression<Func<T, TPropType>> keySelector,
            TPropType localeValue,
            int languageId) where T : IEntity<int>;

        /// <summary>
        ///     Update or create translation values (translate to all langues & save)
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="entity">Entity</param>
        /// <param name="keySelector">Key selector</param>
        Task<List<Models.Entities.Localization.LocalizedProperty>> UpdateTranslationValues<T>(T entity,
            Expression<Func<T, string>> keySelector,
            string sourceText) where T : IEntity<int>;
    }
}