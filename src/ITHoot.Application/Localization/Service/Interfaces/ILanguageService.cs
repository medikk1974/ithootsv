﻿using ITHoot.Models.Entities.Localization;

namespace ITHoot.Application.Localization.Service.Interfaces
{
    public interface ILanguageService
    {
        /// <summary>
        ///     Deletes a language
        /// </summary>
        /// <param name="language">Language</param>
        void DeleteLanguage(Language language);

        /// <summary>
        ///     Inserts a language
        /// </summary>
        /// <param name="language">Language</param>
        void InsertLanguage(Language language);

        /// <summary>
        ///     Updates a language
        /// </summary>
        /// <param name="language">Language</param>
        void UpdateLanguage(Language language);

        /// <summary>
        ///     Get 2 letter ISO language code
        /// </summary>
        /// <param name="language">Language</param>
        /// <returns>ISO language code</returns>
        string GetTwoLetterIsoLanguageName(Language language);
    }
}