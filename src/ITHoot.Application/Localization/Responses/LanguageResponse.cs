﻿namespace ITHoot.Application.Localization.Responses
{
    public class LanguageResponse
    {
        public int Id { get; set; }
        public int DisplayOrder { get; set; }

        public string Name { get; set; }
        public string LanguageCulture { get; set; }
        public string UniqueSeoCode { get; set; }

        public bool Rtl { get; set; }
    }
}