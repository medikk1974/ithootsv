﻿using MediatR;

namespace ITHoot.Application.Localization.Queries
{
    public class GetResourceQuery : IRequest<string>
    {
        public GetResourceQuery(string resourceKey)
        {
            ResourceKey = resourceKey;
        }

        public string ResourceKey { get; set; }
    }
}