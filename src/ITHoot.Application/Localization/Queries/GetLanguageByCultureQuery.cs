﻿using ITHoot.Models.Entities.Localization;
using MediatR;

namespace ITHoot.Application.Localization.Queries
{
    public class GetLanguageByCultureQuery : IRequest<Language>
    {
        public GetLanguageByCultureQuery(string languageCulture)
        {
            LanguageCulture = languageCulture;
        }

        public string LanguageCulture { get; }
    }
}