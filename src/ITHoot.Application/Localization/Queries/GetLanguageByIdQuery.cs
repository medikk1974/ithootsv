﻿using ITHoot.Models.Entities.Localization;
using MediatR;

namespace ITHoot.Application.Localization.Queries
{
    public class GetLanguageByIdQuery : IRequest<Language>
    {
        public GetLanguageByIdQuery(int languageId)
        {
            LanguageId = languageId;
        }

        public int LanguageId { get; }
    }
}