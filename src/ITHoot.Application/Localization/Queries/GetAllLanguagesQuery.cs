﻿using System.Collections.Generic;
using ITHoot.Models.Entities.Localization;
using MediatR;

namespace ITHoot.Application.Localization.Queries
{
    public class GetAllLanguagesQuery : IRequest<List<Language>>
    {
        public GetAllLanguagesQuery(bool showHidden = false)
        {
            ShowHidden = showHidden;
        }

        public bool ShowHidden { get; }
    }
}