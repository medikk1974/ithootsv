﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Caching;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.Localization.Queries;
using ITHoot.Models.Entities.Localization;
using ITHoot.Repository;
using MediatR;

namespace ITHoot.Application.Localization.Handlers
{
    public class GetLanguageByCultureHandler : IRequestHandler<GetLanguageByCultureQuery, Language>
    {
        private readonly IRepository<Language, int> _languageRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public GetLanguageByCultureHandler(
            IRepository<Language, int> languageRepository,
            IStaticCacheManager staticCacheManager)
        {
            _languageRepository = languageRepository;
            _staticCacheManager = staticCacheManager;
        }

        public async Task<Language> Handle(GetLanguageByCultureQuery request, CancellationToken cancellationToken)
        {
            var cacheKey = new CacheKey(CommonHelper.GetEntityCacheKey(typeof(Language), request.LanguageCulture));
            var language = await _staticCacheManager
                .GetAsync(cacheKey, async () =>
                    await _languageRepository
                        .GetByFilter(r => r.LanguageCulture == request.LanguageCulture));

            return language;
        }
    }
}