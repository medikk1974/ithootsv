﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Caching;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.Localization.Queries;
using ITHoot.Models.Entities.Localization;
using ITHoot.Repository;
using MediatR;

namespace ITHoot.Application.Localization.Handlers
{
    public class GetLanguageByIdHandler : IRequestHandler<GetLanguageByIdQuery, Language>
    {
        private readonly IRepository<Language, int> _languageRepository;
        private readonly IStaticCacheManager _staticCacheManager;

        public GetLanguageByIdHandler(
            IRepository<Language, int> languageRepository,
            IStaticCacheManager staticCacheManager)
        {
            _languageRepository = languageRepository;
            _staticCacheManager = staticCacheManager;
        }

        public async Task<Language> Handle(GetLanguageByIdQuery request, CancellationToken cancellationToken)
        {
            if (request.LanguageId == 0)
            {
                return null;
            }

            var cacheKey = new CacheKey(CommonHelper.GetEntityCacheKey(typeof(Language), request.LanguageId));
            var language = await _staticCacheManager
                .GetAsync(cacheKey, async () => 
                    await _languageRepository
                        .GetByIdAsync(request.LanguageId));

            return language;
        }
    }
}