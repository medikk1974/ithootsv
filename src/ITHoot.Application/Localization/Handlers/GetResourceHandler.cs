﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Localization.Queries;
using ITHoot.Application.Localization.Service.Interfaces;
using MediatR;

namespace ITHoot.Application.Localization.Handlers
{
    public class GetResourceHandler : IRequestHandler<GetResourceQuery, string>
    {
        private readonly IWorkContext _workContext;
        private readonly ILocalizationService _localizationService;

        public GetResourceHandler(
            IWorkContext workContext,
            ILocalizationService localizationService)
        {
            _workContext = workContext;
            _localizationService = localizationService;
        }

        public async Task<string> Handle(GetResourceQuery request, CancellationToken cancellationToken)
        {
            var workingLanguage = await _workContext.GetWorkingLanguageAsync();
            if (workingLanguage != null)
            {
                return await _localizationService.GetResourceAsync(request.ResourceKey, workingLanguage.Id);
            }

            return string.Empty;
        }
    }
}