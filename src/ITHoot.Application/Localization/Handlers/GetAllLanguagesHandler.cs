﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.Localization.Queries;
using ITHoot.Models.Entities.Localization;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Localization.Handlers
{
    public class GetAllLanguagesHandler : IRequestHandler<GetAllLanguagesQuery, List<Language>>
    {
        private readonly IRepository<Language, int> _languageRepository;
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IStaticCacheManager _staticCacheManager;

        public GetAllLanguagesHandler(
            IRepository<Language, int> languageRepository,
            ICacheKeyService cacheKeyService,
            IStaticCacheManager staticCacheManager)
        {
            _languageRepository = languageRepository;
            _cacheKeyService = cacheKeyService;
            _staticCacheManager = staticCacheManager;
        }

        public async Task<List<Language>> Handle(GetAllLanguagesQuery request, CancellationToken cancellationToken)
        {
            var key = _cacheKeyService.PrepareKeyForDefaultCache(LocalizationDefaults.LanguagesAllCacheKey, 0, request.ShowHidden);
            var languages = await _staticCacheManager
                .GetAsync(key, async () => await GetLanguages(request));

            return languages;
        }

        private async Task<List<Language>> GetLanguages(GetAllLanguagesQuery request)
        {
            var query = _languageRepository.Table;
            if (!request.ShowHidden)
            {
                query = query.Where(l => l.Published);
            }

            return await query
                .OrderBy(l => l.DisplayOrder)
                .ThenBy(l => l.Id)
                .ToListAsync();
        }
    }
}