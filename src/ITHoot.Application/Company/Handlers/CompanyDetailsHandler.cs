﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Company.Queries;
using ITHoot.Application.Company.Responses;
using ITHoot.Models.Entities;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Company.Handlers
{
    public class CompanyDetailsHandler : IRequestHandler<CompanyDetailsQuery, CompanyDetailsResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Carrier, int> _carrierRepository;

        public CompanyDetailsHandler(
            IMapper mapper,
            IRepository<Carrier, int> carrierRepository)
        {
            _mapper = mapper;
            _carrierRepository = carrierRepository;
        }

        public async Task<CompanyDetailsResult> Handle(CompanyDetailsQuery request, CancellationToken cancellationToken)
        {
            var carrier = await _carrierRepository.Table
                .Include(r => r.User)
                .Include(r => r.Company)
                .Include(r => r.Banks)
                .FirstOrDefaultAsync(r => r.UserId == request.UserId, cancellationToken);

            var result = _mapper.Map<CompanyDetailsResult>(carrier.User);
            _mapper.Map(carrier.Company, result);
            _mapper.Map(carrier.Banks.First(), result);

            return result;
        }
    }
}