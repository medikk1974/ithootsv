﻿using System;

namespace ITHoot.Application.Company.Responses
{
    public class CompanyDetailsResult
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonalCode { get; set; }
        public string VATCode { get; set; }
        public string RegistrationAddress { get; set; }
        //public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}