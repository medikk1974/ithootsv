﻿using ITHoot.Application.Company.Responses;
using MediatR;

namespace ITHoot.Application.Company.Queries
{
    public class CompanyDetailsQuery : IRequest<CompanyDetailsResult>
    {
        public int UserId { get; set; }
    }
}