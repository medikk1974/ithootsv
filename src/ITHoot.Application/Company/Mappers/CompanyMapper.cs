﻿using ITHoot.Application.Company.Responses;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Company.Mappers
{
    public class CompanyMapper : AppMapperBase
    {
        public CompanyMapper()
        {
            CreateMap<Models.Entities.User, CompanyDetailsResult>();

            CreateMap<Models.Entities.Company, CompanyDetailsResult>()
                .Map(m => m.PersonalCode, r => r.Code);

            CreateMap<Models.Entities.Bank, CompanyDetailsResult>();
        }
    }
}
