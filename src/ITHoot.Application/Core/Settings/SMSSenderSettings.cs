﻿namespace ITHoot.Application.Core.Settings
{
    public class SMSSenderSettings
    {
        public string AccountSID { get; set; }
        public string AuthToken { get; set; }
        public string ServiceSID { get; set; }
    }
}