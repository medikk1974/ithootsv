﻿namespace ITHoot.Application.Core.Settings
{
    public class EmailSenderSettings
    {
        public string From { get; set; }
        public string DomainName { get; set; }
        public string ApiKey { get; set; }
    }
}