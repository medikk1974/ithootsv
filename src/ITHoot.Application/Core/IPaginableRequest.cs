﻿namespace ITHoot.Application.Core
{
    public interface IPaginableRequest
    {
        public int Take { get; set; }
        public int Page { get; set; }
    }
}