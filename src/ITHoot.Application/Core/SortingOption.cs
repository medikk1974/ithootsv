﻿namespace ITHoot.Application.Core
{
    public class SortingOption
    {
        public string Field { get; set; }
        public string Text { get; set; }
        //public SortOrder Order { get; set; }
    }
}