﻿namespace ITHoot.Application.Core
{
    public interface IIPRequest
    {
        public string IP { get; set; }
    }
}