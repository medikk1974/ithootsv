﻿using ITHoot.Application.Caching;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Core.Infrastructure.Interfaces;
using ITHoot.Application.Core.Settings;
using ITHoot.Application.Email;
using ITHoot.Application.Localization.Service;
using ITHoot.Application.Localization.Service.Interfaces;
using ITHoot.Application.Pipelines;
using ITHoot.Application.SMS;
using ITHoot.Repository;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace ITHoot.Application.Core.Infrastructure
{
    public class AddDependenciesRegister : IDependencyRegistrar
    {
        public int Order => 1;

        public void Register(IServiceCollection services, ITypeFinder typeFinder, AppSettings appSettings)
        {
            services.AddScoped<IITHootFileProvider, ITHootFileProvider>();

            services.AddScoped(typeof(IRepository<,>), typeof(EFRepository<,>));
            services.AddScoped(typeof(IDeletableRepository<,>), typeof(EFDeletableRepository<,>));

            services.AddSingleton<ICacheKeyService, CacheKeyService>();
            services.AddSingleton<ILocker, MemoryCacheManager>();
            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();

            services.AddScoped<ILocalizationService, LocalizationService>();
            services.AddScoped<ILocalizedEntityService, LocalizedEntityService>();
            services.AddScoped<ILanguageService, LanguageService>();

            services.AddScoped<IEmailSender, EmailSender>();
            services.AddScoped<ISMSSender, SMSSender>();

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationPipe<,>));

            var assembly = typeof(AddDependenciesRegister).Assembly;
            services.AddValidatorsFromAssembly(assembly);
            services.AddMediatR(assembly);
        }
    }
}