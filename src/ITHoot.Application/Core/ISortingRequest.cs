﻿namespace ITHoot.Application.Core
{
    public interface ISortingRequest
    {
        public SortingOption Sorting { get; set; }
    }
}