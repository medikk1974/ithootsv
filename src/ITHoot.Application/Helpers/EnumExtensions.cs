﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITHoot.Application.Core.Infrastructure;
using ITHoot.Application.Localization.Service.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ITHoot.Application.Helpers
{
    public static class EnumExtensions
    {
        public static async Task<SelectList> ToSelectListAsync<TEnum>(this TEnum enumObj,
            string promptText = null,
            bool markCurrentAsSelected = true,
            int[] valuesToExclude = null,
            bool useLocalization = true) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("An Enumeration type is required.", nameof(enumObj));

            var localizationService = EngineContext.Current.Resolve<ILocalizationService>();

            var values = new List<dynamic>();
            if (!string.IsNullOrEmpty(promptText))
                values.Add(new {ID = -1, Name = promptText});

            var enumValues = Enum.GetValues(typeof(TEnum)).OfType<TEnum>()
                .Where(enumValue => valuesToExclude == null || !valuesToExclude.Contains(Convert.ToInt32(enumValue)));

            foreach (var enumValue in enumValues)
                values.Add(new
                {
                    ID = Convert.ToInt32(enumValue),
                    Name = useLocalization
                        ? await localizationService.GetLocalizedEnumAsync(enumValue)
                        : CommonHelper.ConvertEnum(enumValue.ToString())
                });

            object selectedValue = null;
            if (markCurrentAsSelected)
                selectedValue = Convert.ToInt32(enumObj);
            return new SelectList(values, "ID", "Name", selectedValue);
        }

        public static async Task<SelectList> ToSelectListAsync<TEnum>(
            string promptText = null,
            bool useLocalization = true) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("An Enumeration type is required.", nameof(TEnum));

            var localizationService = EngineContext.Current.Resolve<ILocalizationService>();

            var values = new List<dynamic>();

            if (!string.IsNullOrEmpty(promptText))
                values.Add(new {ID = -1, Name = promptText});

            var enumValues = Enum.GetValues(typeof(TEnum)).OfType<TEnum>();

            foreach (var enumValue in enumValues)
                values.Add(new
                {
                    ID = Convert.ToInt32(enumValue),
                    Name = useLocalization
                        ? await localizationService.GetLocalizedEnumAsync(enumValue)
                        : CommonHelper.ConvertEnum(enumValue.ToString())
                });

            return new SelectList(values, "ID", "Name");
        }
    }
}