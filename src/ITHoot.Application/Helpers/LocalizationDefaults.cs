﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class LocalizationDefaults
    {
        public static string AdminLocaleStringResourcesPrefix => "Admin.";

        public static string EnumLocaleStringResourcesPrefix => "Enums.";

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : store ID
        ///     {1} : show hidden records?
        /// </remarks>
        public static CacheKey LanguagesAllCacheKey =>
            new("ITHoot.language.all-{0}-{1}", LanguagesByStoreIdPrefixCacheKey, LanguagesAllPrefixCacheKey);

        /// <summary>
        ///     Gets a key pattern to clear cache
        /// </summary>
        /// <remarks>
        ///     {0} : store ID
        /// </remarks>
        public static string LanguagesByStoreIdPrefixCacheKey => "ITHoot.language.all-{0}";

        /// <summary>
        ///     Gets a key pattern to clear cache
        /// </summary>
        public static string LanguagesAllPrefixCacheKey => "ITHoot.language.all";

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : language ID
        /// </remarks>
        public static CacheKey LocaleStringResourcesAllPublicCacheKey =>
            new("ITHoot.lsr.all.public-{0}", LocaleStringResourcesPrefixCacheKey);

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : language ID
        /// </remarks>
        public static CacheKey LocaleStringResourcesAllAdminCacheKey =>
            new("ITHoot.lsr.all.admin-{0}", LocaleStringResourcesPrefixCacheKey);

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : language ID
        /// </remarks>
        public static CacheKey LocaleStringResourcesAllCacheKey =>
            new("ITHoot.lsr.all-{0}", LocaleStringResourcesPrefixCacheKey);

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : language ID
        ///     {1} : resource key
        /// </remarks>
        public static CacheKey LocaleStringResourcesByResourceNameCacheKey =>
            new("ITHoot.lsr.{0}-{1}", LocaleStringResourcesByResourceNamePrefixCacheKey,
                LocaleStringResourcesPrefixCacheKey);

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : language ID
        /// </remarks>
        public static string LocaleStringResourcesByResourceNamePrefixCacheKey => "ITHoot.lsr.{0}";

        /// <summary>
        ///     Gets a key pattern to clear cache
        /// </summary>
        public static string LocaleStringResourcesPrefixCacheKey => "ITHoot.lsr.";

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        /// <remarks>
        ///     {0} : language ID
        ///     {1} : entity ID
        ///     {2} : locale key group
        ///     {3} : locale key
        /// </remarks>
        public static CacheKey LocalizedPropertyCacheKey => new("ITHoot.localizedproperty.value-{0}-{1}-{2}-{3}");

        /// <summary>
        ///     Gets a key for caching
        /// </summary>
        public static CacheKey LocalizedPropertyAllCacheKey => new("ITHoot.localizedproperty.all");
    }
}