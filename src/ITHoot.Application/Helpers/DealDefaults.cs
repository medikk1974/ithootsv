﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class DealDefaults
    {
        public static CacheKey DealByIdPrefixCacheKey => new("ITHoot.deal-by-id-{0}-{1}");
        public static CacheKey DealListByUserIdPrefixCacheKey => new("ITHoot.deal-list-by-user-id-{0}-{1}");
    }
}