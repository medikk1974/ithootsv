﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class PlanDefaults
    {
        public static CacheKey SellerPlanAllPrefixCacheKey => new("ITHoot.plan-all-{0}");
        public static CacheKey OfferPlanListByFreePrefixCacheKey => new("ITHoot.offer-plan-list-by-free-{0}-{1}");
        public static CacheKey PlanByIdPrefixCacheKey => new("ITHoot.plan-by-id-{0}-{1}");
    }
}