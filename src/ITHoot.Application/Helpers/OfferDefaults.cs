﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class OfferDefaults
    {
        public static CacheKey OffersByUserPrefixCacheKey => new("ITHoot.offers-by-user-{0}-{1}");

        public static CacheKey OfferPackageByPlanIdPrefixCacheKey =>
            new("ITHoot.offer-package-by-plan-id-{0}-{1}-{2}");

        public static CacheKey OfferPackageByUserIdPrefixCacheKey => new("ITHoot.offer-package-by-user-id-{0}-{1}");
        public static CacheKey OfferByIdPrefixCacheKey => new("ITHoot.offers-by-id-{0}-{1}");
    }
}