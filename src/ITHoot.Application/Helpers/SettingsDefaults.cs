﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class SettingsDefaults
    {
        public static CacheKey SettingsByKeyPrefixCacheKey => new("ITHoot.settings-by-key-{0}-{1}");
    }
}