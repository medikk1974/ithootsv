﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class PropertyDefaults
    {
        public static CacheKey PropertyTypeAllPrefixCacheKey => new("ITHoot.property-type-all-{0}-{1}-{2}");
        public static CacheKey PropertyListByUserIdPrefixCacheKey => new("ITHoot.property-list-by-user-id-{0}-{1}");

        public static CacheKey SimilarPropertyListByMainPropertyIdPrefixCacheKey =>
            new("ITHoot.similar-property-list-by-main-property-id-{0}-{1}");

        public static CacheKey PropertyByIdPrefixCacheKey => new("ITHoot.property-by-id-{0}-{1}");

        public static CacheKey SimilarAreaPropertyListByMainPropertyIdPrefixCacheKey =>
            new("ITHoot.similar-area-property-list-by-main-property-id-{0}-{1}");
    }
}