﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    internal static class UserDefaults
    {
        public static CacheKey UserByIdPrefixCacheKey => new("ITHoot.user-by-id-{0}");
        public static CacheKey UserByAccessTokenPrefixCacheKey => new("ITHoot.user-access-token-{0}");
    }
}