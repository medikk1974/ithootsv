﻿using ITHoot.Application.Caching;

namespace ITHoot.Application.Helpers
{
    public static class MediaDefaults
    {
        public static string ImagesRelativePath => "/images";

        public static string ImagesVirtualPath => $"{ImagesRelativePath}";

        public static CacheKey ImageEntityPrefixCacheKey => new("ITHoot.image.{0}-{1}");
    }
}