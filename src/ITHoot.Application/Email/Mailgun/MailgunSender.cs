﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ITHoot.Application.Email.Mailgun.Interfaces;
using ITHoot.Application.Email.Mailgun.Models;
using ITHoot.Application.Extensions;
using Newtonsoft.Json;

namespace ITHoot.Application.Email.Mailgun
{
    public class MailgunSender : IMailgunSender
    {
        private readonly string _domainName;
        private readonly string _apiKey;
        private readonly HttpClient _httpClient;

        public MailgunSender(string domainName, string apiKey, MailgunRegionTypes mailGunRegion = MailgunRegionTypes.USA)
        {
            _domainName = domainName;
            _apiKey = apiKey;
            _httpClient = new HttpClient { BaseAddress = GetBasUri(mailGunRegion) };
            _httpClient.DefaultRequestHeaders.Authorization = GetAuthenticationHeader();
        }
        
        public async Task<MailgunSendResponse> SendAsync(IMailgunEmail email)
        {
            var content = GetMultipartFormDataContent(GetParams(email), GetFiles(email));
            var response = await _httpClient.PostAsync($"v3/{_domainName}/messages", content);

            return await DeserializeObject<MailgunSendResponse>(response);
        }

        public async Task<MailgunValidateResponse> ValidateAsync(string emailAddress)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new("address", emailAddress)
            };

            var content = GetMultipartFormDataContent(parameters);
            var response = await _httpClient.PostAsync("v4/address/validate", content);

            return await DeserializeObject<MailgunValidateResponse>(response);
        }

        private static async Task<T> DeserializeObject<T>(HttpResponseMessage response)
        {
            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(responseBody);
        }

        private static HttpContent GetMultipartFormDataContent(IEnumerable<KeyValuePair<string, string>> parameters, IEnumerable<MailgunFile> files = null)
        {
            var mpContent = new MultipartFormDataContent();

            parameters.ForEach(p =>
            {
                mpContent.Add(new StringContent(p.Value), p.Key);
            });

            files?.ForEach(file =>
            {
                using var memoryStream = new MemoryStream();
                file.Data.CopyTo(memoryStream);
                mpContent.Add(new ByteArrayContent(memoryStream.ToArray()), file.ParameterName, file.Filename);
            });

            return mpContent;
        }

        private static IEnumerable<KeyValuePair<string, string>> GetParams(IMailgunEmail email)
        {
            var parameters = new List<KeyValuePair<string, string>>
            {
                new("from", email.FromAddress.ToString())
            };
            
            email.ToAddresses.ForEach(x => { parameters.Add(new KeyValuePair<string, string>("to", x.ToString())); });

            email.CcAddresses.ForEach(x => { parameters.Add(new KeyValuePair<string, string>("cc", x.ToString())); });

            email.BccAddresses.ForEach(x => { parameters.Add(new KeyValuePair<string, string>("bcc", x.ToString())); });

            email.ReplyToAddresses.ForEach(x =>
            {
                parameters.Add(new KeyValuePair<string, string>("h:Reply-To", x.ToString()));
            });

            parameters.Add(new KeyValuePair<string, string>("subject", email.Subject));

            parameters.Add(new KeyValuePair<string, string>(email.IsHtml ? "html" : "text", email.Body));

            if (!string.IsNullOrEmpty(email.PlaintextAlternativeBody))
            {
                parameters.Add(new KeyValuePair<string, string>("text", email.PlaintextAlternativeBody));
            }

            email.Tags.ForEach(x => { parameters.Add(new KeyValuePair<string, string>("o:tag", x)); });

            foreach (var emailHeader in email.Headers)
            {
                var key = emailHeader.Key;
                if (!key.StartsWith("h:"))
                {
                    key = "h:" + emailHeader.Key;
                }

                parameters.Add(new KeyValuePair<string, string>(key, emailHeader.Value));
            }

            return parameters;
        }

        private static IEnumerable<MailgunFile> GetFiles(IMailgunEmail email)
        {
            var files = new List<MailgunFile>();
            email.Attachments.ForEach(x =>
            {
                files.Add(new MailgunFile
                {
                    ParameterName = x.IsInline ? "inline" : "attachment",
                    Data = x.Data,
                    Filename = x.Filename,
                    ContentType = x.ContentType
                });
            });

            return files;
        }

        private AuthenticationHeaderValue GetAuthenticationHeader()
        {
            var keyBytes = Encoding.ASCII.GetBytes($"api:{_apiKey}");
            var keyBase64 = Convert.ToBase64String(keyBytes);
            return new AuthenticationHeaderValue("Basic", keyBase64);
        }

        private static Uri GetBasUri(MailgunRegionTypes mailGunRegion)
        {
            var region = GetBaseUrlRegion(mailGunRegion);
            var baseUrl = $"https://api{region}.mailgun.net/";
            return new Uri(baseUrl);
        }

        private static string GetBaseUrlRegion(MailgunRegionTypes mailGunRegion)
        {
            return mailGunRegion switch
            {
                MailgunRegionTypes.USA => null,
                MailgunRegionTypes.EU => ".eu",
                _ => throw new ArgumentException($"'{mailGunRegion}' is not a valid value for {nameof(mailGunRegion)}")
            };
        }
    }
}
