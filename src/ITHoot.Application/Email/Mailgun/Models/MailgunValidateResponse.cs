﻿using System.Collections.Generic;

namespace ITHoot.Application.Email.Mailgun.Models
{
    public class MailgunValidateResponse
    {
        public string Message { get; set; }
        public string Address { get; set; }
        public bool IsDisposableAddress { get; set; }
        public bool IsRoleAddress { get; set; }
        public IEnumerable<string> Reason { get; set; } = new List<string>();
        public MailgunValidateTypes Result { get; set; }
        public MailgunRiskTypes Risk { get; set; }
    }
}