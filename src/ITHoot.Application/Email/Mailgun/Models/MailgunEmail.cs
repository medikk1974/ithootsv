﻿using System.Collections.Generic;
using ITHoot.Application.Email.Mailgun.Interfaces;
using ITHoot.Application.Email.Models;

namespace ITHoot.Application.Email.Mailgun.Models
{
    public class MailgunEmail : IMailgunEmail
    {
        public string Address { get; set; }
        public IList<EmailAddress> ToAddresses { get; set; } = new List<EmailAddress>();
        public IList<EmailAddress> CcAddresses { get; set; } = new List<EmailAddress>();
        public IList<EmailAddress> BccAddresses { get; set; } = new List<EmailAddress>();
        public IList<EmailAddress> ReplyToAddresses { get; set; } = new List<EmailAddress>();
        public IList<Attachment> Attachments { get; set; } = new List<Attachment>();
        public EmailAddress FromAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string PlaintextAlternativeBody { get; set; }
        public MailgunPriorityType Priority { get; set; }
        public IList<string> Tags { get; set; } = new List<string>();

        public bool IsHtml { get; set; }
        public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();
    }
}
