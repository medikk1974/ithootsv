﻿using System.IO;

namespace ITHoot.Application.Email.Mailgun.Models
{
    public class MailgunFile
    {
        public string ParameterName { get; set; }
        public string Filename { get; set; }
        public Stream Data { get; set; }
        public string ContentType { get; set; }
    }
}
