﻿namespace ITHoot.Application.Email.Mailgun.Models
{
    public class MailgunSendResponse
    {
        public string Id { get; set; }
        public string Message { get; set; }
    }
}