﻿namespace ITHoot.Application.Email.Mailgun
{
    public enum MailgunRegionTypes
    {
        USA = 1,
        EU = 2
    }

    public enum MailgunPriorityType
    {
        High = 1,
        Normal = 2,
        Low = 3
    }

    public enum MailgunValidateTypes
    {
        Deliverable = 1,
        Undeliverable = 2,
        CatchAll = 3,
        Unknown = 4
    }

    public enum MailgunRiskTypes
    {
        High = 1,
        Medium = 2,
        Low = 3,
        Unknown = 4
    }
}
