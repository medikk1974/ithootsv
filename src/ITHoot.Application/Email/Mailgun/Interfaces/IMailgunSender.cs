﻿using System.Threading.Tasks;
using ITHoot.Application.Email.Mailgun.Models;

namespace ITHoot.Application.Email.Mailgun.Interfaces
{
    public interface IMailgunSender
    {
        Task<MailgunSendResponse> SendAsync(IMailgunEmail email);
        Task<MailgunValidateResponse> ValidateAsync(string emailAddress);
    }
}
