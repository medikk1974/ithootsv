﻿using System.Collections.Generic;
using ITHoot.Application.Email.Models;

namespace ITHoot.Application.Email.Mailgun.Interfaces
{
    public interface IMailgunEmail
    {
        string Address { get; set; }
        IList<EmailAddress> ToAddresses { get; set; }
        IList<EmailAddress> CcAddresses { get; set; }
        IList<EmailAddress> BccAddresses { get; set; }
        IList<EmailAddress> ReplyToAddresses { get; set; }
        IList<Attachment> Attachments { get; set; }
        EmailAddress FromAddress { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
        string PlaintextAlternativeBody { get; set; }
        MailgunPriorityType Priority { get; set; }
        IList<string> Tags { get; set; }
        bool IsHtml { get; set; }
        IDictionary<string, string> Headers { get; set; }
    }
}