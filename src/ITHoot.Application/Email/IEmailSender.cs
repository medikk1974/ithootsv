﻿using System.Threading.Tasks;
using ITHoot.Application.Email.Models;

namespace ITHoot.Application.Email
{
    public interface IEmailSender
    {
        Task<ValidationEmailResult> ValidateEmailAsync(string emailAddress);
        Task<SendEmailResult> SendEmailConfirmAsync(EmailAddress address, string code);
        //Task<SendEmailResult> SendEmailPdfAsync(EmailAddress address, Attachment attachment);
    }
}