﻿namespace ITHoot.Application.Email.Models
{
    public class SendEmailResult
    {
        public bool Success { get; set; }
    }
}
