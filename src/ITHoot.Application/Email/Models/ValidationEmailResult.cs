﻿namespace ITHoot.Application.Email.Models
{
    public class ValidationEmailResult
    {
        public bool Success { get; set; }
    }
}
