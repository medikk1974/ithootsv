﻿namespace ITHoot.Application.Email.Models
{
    public class EmailAddress
    {
        public EmailAddress(string emailAddress, string name = null)
        {
            Email = emailAddress;
            Name = name;
        }

        public string Name { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Name))
            {
                if (!string.IsNullOrEmpty(Email))
                {
                    return $"{Name} <{Email}>";
                }

                return null;
            }

            return Email;
        }
    }
}
