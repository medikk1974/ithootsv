﻿using System.Threading.Tasks;
using ITHoot.Application.Core.Settings;
using ITHoot.Application.Email.Mailgun;
using ITHoot.Application.Email.Mailgun.Interfaces;
using ITHoot.Application.Email.Mailgun.Models;
using ITHoot.Application.Email.Models;
using ITHoot.Application.Localization.Queries;
using ITHoot.Models;
using MediatR;
using Microsoft.Extensions.Hosting;

namespace ITHoot.Application.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSenderSettings _emailSettings;
        private readonly IMailgunSender _mailgun;
        private readonly IHostEnvironment _environment;
        private readonly IMediator _mediator;

        public EmailSender(
            EmailSenderSettings emailSettings,
            IHostEnvironment environment, IMediator mediator)
        {
            _emailSettings = emailSettings;
            _mailgun = new MailgunSender(_emailSettings.DomainName, _emailSettings.ApiKey);
            _environment = environment;
            _mediator = mediator;
        }

        public async Task<ValidationEmailResult> ValidateEmailAsync(string emailAddress)
        {
            if (_environment.IsDevelopment())
            {
                return new ValidationEmailResult { Success = true };
            }

            var response = await _mailgun.ValidateAsync(emailAddress);
            
            return new ValidationEmailResult
            {
                Success = response.Result == MailgunValidateTypes.Deliverable
            };
        }

        public async Task<SendEmailResult> SendEmailConfirmAsync(EmailAddress address, string code)
        {
            if (_environment.IsDevelopment())
            {
                return new SendEmailResult { Success = true };
            }

            var email = new MailgunEmail();
            email.ToAddresses.Add(address);
            email.FromAddress = new EmailAddress(_emailSettings.From);
            email.Subject = await GetLocalizedText(Constants.Templates.EmailConfirmSubjectKey);
            email.Body = await GetLocalizedText(Constants.Templates.EmailConfirmTextKey, code);
            email.IsHtml = true;
            var response = await _mailgun.SendAsync(email);

            return CheckSending(response);
        }

        /*public async Task<SendEmailResult> SendEmailPdfAsync(EmailAddress address, Attachment attachment)
        {
            if (_environment.IsDevelopment())
            {
                return new SendEmailResult { Success = true };
            }

            var email = new MailgunEmail();
            email.ToAddresses.Add(address);
            email.FromAddress = new EmailAddress(_emailSettings.From);
            email.Subject = await GetLocalizedText(Constants.Templates.);
            //email.Body = 
            //email.IsHtml = true;
            email.Attachments.Add(attachment);
            var response = await _mailgun.SendAsync(email);

            return CheckSending(response);
        }*/

        private static SendEmailResult CheckSending(MailgunSendResponse response)
        {
            return new()
            {
                Success = !string.IsNullOrEmpty(response.Id)
            };
        }

        private async Task<string> GetLocalizedText(string key, params object[] args)
        {
            var query = new GetResourceQuery(key);
            var template = await _mediator.Send(query);

            return string.Format(template, args);
        }
    }
}