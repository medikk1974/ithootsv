﻿using ITHoot.Application.Confirmation.Responses;
using ITHoot.Application.Email.Models;

namespace ITHoot.Application.Email.Mappers
{
    public class EmailMapper : AppMapperBase
    {
        public EmailMapper()
        {
            CreateMap<SendEmailResult, SendConfirmationResponse>();
        }
    }
}
