﻿using ITHoot.Application.User.Responses;
using MediatR;

namespace ITHoot.Application.User.Notifications
{
    public class UserClearCacheNotification : INotification
    {
        public UserClearCacheNotification(int id, UserResponse user = null)
        {
            Id = id;
            User = user;
        }

        public int Id { get; }
        public UserResponse User { get; }
    }
}