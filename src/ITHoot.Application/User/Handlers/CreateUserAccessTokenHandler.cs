﻿using System;
using ITHoot.Application.User.Commands;
using ITHoot.Application.User.Responses;
using ITHoot.Models.Entities;
using ITHoot.Repository;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.User.Handlers
{
    public class GenerateUserAccessTokenHandler : IRequestHandler<CreateUserAccessTokenCommand, UserAccessTokenResult>
    {
        private readonly IRepository<AccessToken, int> _accessTokenRepository;

        public GenerateUserAccessTokenHandler(IRepository<AccessToken, int> accessTokenRepository)
        {
            _accessTokenRepository = accessTokenRepository;
        }

        public async Task<UserAccessTokenResult> Handle(CreateUserAccessTokenCommand request, CancellationToken cancellationToken)
        {
            var accessToken = new AccessToken
            {
                UserId = request.UserId,
                Token = Guid.NewGuid().ToString(),
                CreatedAt = DateTime.UtcNow,
                UpdatedAt = DateTime.UtcNow
            };

            if (request.ExpireDays.HasValue)
            {
                accessToken.ExpiryAt = DateTime.UtcNow.AddDays(request.ExpireDays.Value);
            }

            var result = await _accessTokenRepository.CreateAsync(accessToken);

            return new UserAccessTokenResult { Token = result.Token };
        }
    }
}