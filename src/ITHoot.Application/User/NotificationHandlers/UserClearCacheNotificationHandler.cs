﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.Helpers;
using ITHoot.Application.User.Notifications;
using MediatR;

namespace ITHoot.Application.User.NotificationHandlers
{
    public class UserClearCacheNotificationHandler : INotificationHandler<UserClearCacheNotification>
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IStaticCacheManager _staticCacheManager;

        public UserClearCacheNotificationHandler(
            ICacheKeyService cacheKeyService,
            IStaticCacheManager staticCacheManager)
        {
            _cacheKeyService = cacheKeyService;
            _staticCacheManager = staticCacheManager;
        }

        public Task Handle(UserClearCacheNotification notification, CancellationToken cancellationToken)
        {
            var key = _cacheKeyService.PrepareKeyForDefaultCache(
                UserDefaults.UserByIdPrefixCacheKey,
                notification.Id);

            if (notification.User != null)
            {
                _staticCacheManager.Set(key, notification.User);
            }
            else
            {
                if (_staticCacheManager.IsSet(key)) _staticCacheManager.Remove(key);
            }

            return Task.CompletedTask;
        }
    }
}