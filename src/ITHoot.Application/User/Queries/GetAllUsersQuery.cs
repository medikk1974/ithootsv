﻿using System.Collections.Generic;
using ITHoot.Application.User.Responses;
using MediatR;

namespace ITHoot.Application.User.Queries
{
    public class GetAllUsersQuery : IRequest<List<UserResponse>>
    {
    }
}