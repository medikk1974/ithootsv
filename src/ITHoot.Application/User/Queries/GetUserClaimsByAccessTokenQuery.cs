﻿using ITHoot.Application.User.Responses;
using MediatR;

namespace ITHoot.Application.User.Queries
{
    public class GetUserClaimsByAccessTokenQuery : IRequest<UserAccessTokenClaimsResult>
    {
        public GetUserClaimsByAccessTokenQuery(string accessToken)
        {
            AccessToken = accessToken;
        }

        public string AccessToken { get; }
    }
}