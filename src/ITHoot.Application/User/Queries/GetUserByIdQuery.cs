﻿using ITHoot.Application.User.Responses;
using MediatR;

namespace ITHoot.Application.User.Queries
{
    public class GetUserByIdQuery : IRequest<UserResponse>
    {
        public GetUserByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}