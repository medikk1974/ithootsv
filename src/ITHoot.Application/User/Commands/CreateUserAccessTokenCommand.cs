﻿using ITHoot.Application.User.Responses;
using MediatR;

namespace ITHoot.Application.User.Commands
{
    public class CreateUserAccessTokenCommand : IRequest<UserAccessTokenResult>
    {
        public int UserId { get; set; }
        public int? ExpireDays { get; set; }
    }
}