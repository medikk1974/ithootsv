﻿using System.Threading.Tasks;
using ITHoot.Models.Entities.Localization;

namespace ITHoot.Application
{
    public interface IWorkContext
    {
        /// <summary>
        ///     Gets current user working language
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        Task<Language> GetWorkingLanguageAsync();

        /// <summary>
        ///     Sets current user working language
        /// </summary>
        /// <param name="language">Language</param>
        /// <returns>A task that represents the asynchronous operation</returns>
        Task SetWorkingLanguageAsync(Language language);
    }
}