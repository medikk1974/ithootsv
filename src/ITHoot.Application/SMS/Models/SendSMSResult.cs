﻿namespace ITHoot.Application.SMS.Models
{
    public class SendSMSResult
    {
        public bool Success { get; set; }
    }
}
