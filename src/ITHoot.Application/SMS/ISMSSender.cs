﻿using System.Threading.Tasks;
using ITHoot.Application.SMS.Models;

namespace ITHoot.Application.SMS
{
    public interface ISMSSender
    {
        Task<ValidationPhoneResult> ValidatePhoneAsync(string phoneNumber);
        Task<SendSMSResult> SendSMSConfirmationAsync(string toPhoneNumber, string code);
    }
}