﻿using System.Threading.Tasks;
using ITHoot.Application.SMS.Twilio.Models;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Lookups.V1;

namespace ITHoot.Application.SMS.Twilio.Interfaces
{
    public interface ITwilioSender
    {
        Task<PhoneNumberResource> ValidatePhoneAsync(string phoneNumber);
        Task<MessageResource> SendSMSAsync(TwilioSMS sms);
    }
}