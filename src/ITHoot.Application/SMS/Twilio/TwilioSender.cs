﻿using System;
using System.Threading.Tasks;
using ITHoot.Application.SMS.Twilio.Interfaces;
using ITHoot.Application.SMS.Twilio.Models;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Lookups.V1;

namespace ITHoot.Application.SMS.Twilio
{
    public class TwilioSender : ITwilioSender
    {
        public TwilioSender(string accountSID, string authToken)
        {
            TwilioClient.Init(accountSID, authToken);
        }

        public async Task<PhoneNumberResource> ValidatePhoneAsync(string phoneNumber)
        {
            try
            {
                return await PhoneNumberResource.FetchAsync(phoneNumber);
            }
            catch (Exception exception)
            {
                // todo log error

                return null;
            }
        }

        public async Task<MessageResource> SendSMSAsync(TwilioSMS sms)
        {
            try
            {
                return await MessageResource.CreateAsync(
                    body: sms.Body,
                    messagingServiceSid: sms.ServiceSid,
                    to: sms.To
                );
            }
            catch (Exception exception)
            {
                // todo log error

                return null;
            }
        }
    }
}
