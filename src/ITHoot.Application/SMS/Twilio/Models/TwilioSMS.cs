﻿using Twilio.Types;

namespace ITHoot.Application.SMS.Twilio.Models
{
    public class TwilioSMS
    {
        public string Body { get; set; }
        public string ServiceSid { get; set; }
        public PhoneNumber To { get; set; }
    }
}
