﻿using System;
using System.Collections.Generic;

namespace ITHoot.Application.SMS.Twilio.Models
{
    public class TwilioSendResponse
    {
        public string AccountSid { get; set; }
        public string ApiVersion { get; set; }
        public string Body { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateSent { get; set; }
        public DateTime DateUpdated { get; set; }
        public string Direction { get; set; }
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string From { get; set; }
        public string MessagingServiceSid { get; set; }
        public int NumMedia { get; set; }
        public int NumSegments { get; set; }
        public decimal Price { get; set; }
        public string PriceUnit { get; set; }
        public string Sid { get; set; }
        public string Status { get; set; }
        public IEnumerable<SubresourceUri> SubresourceUris { get; set; } = new List<SubresourceUri>();
        public string To { get; set; }
        public string Uri { get; set; }
    }

    public class SubresourceUri
    {
        public string Media { get; set; }
    }
}