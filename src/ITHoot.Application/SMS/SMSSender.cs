﻿using ITHoot.Application.Core.Settings;
using ITHoot.Application.SMS.Models;
using ITHoot.Application.SMS.Twilio;
using ITHoot.Application.SMS.Twilio.Interfaces;
using ITHoot.Application.SMS.Twilio.Models;
using MediatR;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using ITHoot.Application.Localization.Queries;
using ITHoot.Models;

namespace ITHoot.Application.SMS
{
    public class SMSSender : ISMSSender
    {
        private readonly SMSSenderSettings _smsSettings;
        private readonly ITwilioSender _twilio;
        private readonly IHostEnvironment _environment;
        private readonly IMediator _mediator;

        public SMSSender(
            SMSSenderSettings smsSettings,
            IHostEnvironment environment,
            IMediator mediator)
        {
            _smsSettings = smsSettings;
            _twilio = new TwilioSender(_smsSettings.AccountSID, _smsSettings.AuthToken);
            _environment = environment;
            _mediator = mediator;
        }

        public async Task<ValidationPhoneResult> ValidatePhoneAsync(string phoneNumber)
        {
            if (_environment.IsDevelopment())
            {
                return new ValidationPhoneResult { Success = true };
            }

            var response = await _twilio.ValidatePhoneAsync(phoneNumber);
            return new ValidationPhoneResult
            {
                // todo improve work with response
                Success = response != null
            };
        }

        public async Task<SendSMSResult> SendSMSConfirmationAsync(string toPhoneNumber, string code)
        {
            if (_environment.IsDevelopment())
            {
                return new SendSMSResult { Success = true };
            }

            var sms = new TwilioSMS
            {
                Body = await GetLocalizedText(Constants.Templates.SMSConfirmTextKey, code),
                ServiceSid = _smsSettings.ServiceSID,
                To = toPhoneNumber
            };

            var response = await _twilio.SendSMSAsync(sms);

            return new SendSMSResult
            {
                Success = response is { ErrorCode: null }
                          && !string.IsNullOrEmpty(response.Sid)
            };
        }

        private async Task<string> GetLocalizedText(string key, params object[] args)
        {
            var query = new GetResourceQuery(key);
            var template = await _mediator.Send(query);

            return string.Format(template, args);
        }
    }
}