﻿using AutoMapper;
using ITHoot.Application.Extensions;
using ITHoot.Application.Order.Queries;
using ITHoot.Application.Order.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Order.Handlers
{
    public class DriverGetOrderQueryHandler : IRequestHandler<DriverGetOrderQuery, DriverGetOrderResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Order, int> _orderRepository;
        public DriverGetOrderQueryHandler(
            IMapper mapper,
            IRepository<Models.Entities.Order, int> orderRepository)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public async Task<DriverGetOrderResult> Handle(DriverGetOrderQuery request, CancellationToken cancellationToken)
        {
            var order = await _orderRepository.Table
                //.Include(x => x.Vehicle)
                .Include(x => x.OrderRequest.Points)
                .Include(x => x.OrderRequest.Customer.Company)
                .Include(x => x.OrderRequest.Customer.User)
                .FirstOrDefaultAsync(x => x.Vehicle.AssignedDriver.UserId == request.UserId
                                    && x.Id == request.OrderId, cancellationToken);
            if (order == null)
                throw ValidationException.Build(nameof(request.OrderId), "Order not found");

            return _mapper.Map<DriverGetOrderResult>(order);
        }
    }
}
