﻿using AutoMapper;
using ITHoot.Application.Order.Queries;
using ITHoot.Application.Order.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Order.Handlers
{
    public class CarrierGetAllOrdersQueryHandler : IRequestHandler<CarrierGetAllOrdersQuery, CarrierGetAllOrdersResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Order, int> _orderRepository;

        public CarrierGetAllOrdersQueryHandler(
            IMapper mapper,
            IRepository<Models.Entities.Order, int> orderRepository)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public async Task<CarrierGetAllOrdersResult> Handle(CarrierGetAllOrdersQuery request, CancellationToken cancellationToken)
        {
            var orders = await _orderRepository.Table
                .Include(x => x.Vehicle)
                .Include(x => x.OrderRequest.Points)
                .Include(x => x.OrderRequest.Customer.Company)
                .Where(x => x.Vehicle.Carrier.UserId == request.UserId
                            && x.StatusTypes == request.StatusTypes)
                .ToListAsync(cancellationToken);

            return new CarrierGetAllOrdersResult
            {
                Items = _mapper.Map<IEnumerable<CarrierGetAllOrdersDetailResult>>(orders)
            };
        }
    }
}
