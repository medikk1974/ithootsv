﻿using System;
using System.Collections.Generic;
using AutoMapper;
using ITHoot.Application.Order.Queries;
using ITHoot.Application.Order.Responses;
using CsvHelper;
using CsvHelper.Configuration;
using MediatR;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Order.Handlers
{
    public class CarrierDownloadCsvQueryHandler : IRequestHandler<CarrierDownloadOrdersCsvQuery, CarrierDownloadOrdersCsvResult>
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        public CarrierDownloadCsvQueryHandler(
            IMediator mediator,
            IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<CarrierDownloadOrdersCsvResult> Handle(CarrierDownloadOrdersCsvQuery request, CancellationToken cancellationToken)
        {
            await using var stream = new MemoryStream();
            await using var writer = new StreamWriter(stream);
            var csvConfig = new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ";" };
            await using (var csv = new CsvWriter(writer, csvConfig))
            {
                var query = _mapper.Map<CarrierGetAllOrdersQuery>(request);
                var response = await _mediator.Send(query, cancellationToken);
                var items = _mapper.Map<IEnumerable<CarrierGetAllOrdersCsvResult>>(response.Items);

                await csv.WriteRecordsAsync(items, cancellationToken);
            }

            return new CarrierDownloadOrdersCsvResult
            {
                Item = stream.ToArray(),
                FileName = $"Orders-{DateTime.UtcNow:yy-MM-dd_HH-mm}.csv"
            };
        }
    }
}
