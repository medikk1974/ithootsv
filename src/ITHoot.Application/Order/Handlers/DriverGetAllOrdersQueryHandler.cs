﻿using System;
using AutoMapper;
using ITHoot.Application.Order.Queries;
using ITHoot.Application.Order.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Models;

namespace ITHoot.Application.Order.Handlers
{
    public class DriverGetAllOrdersQueryHandler : IRequestHandler<DriverGetAllOrdersQuery, DriverGetAllOrdersResult>
    {

        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Order, int> _orderRepository;
        public DriverGetAllOrdersQueryHandler(
            IMapper mapper,
            IRepository<Models.Entities.Order, int> orderRepository)
        {
            _mapper = mapper;
            _orderRepository = orderRepository;
        }

        public async Task<DriverGetAllOrdersResult> Handle(DriverGetAllOrdersQuery request, CancellationToken cancellationToken)
        {
            var query = _orderRepository.Table
                .Include(x => x.OrderRequest.Customer.Company)
                .Where(x => x.Vehicle.AssignedDriver.UserId == request.UserId);

            switch (request.Filter)
            {
                case Enums.DriverOrdersSelector.Today:
                    query = query.Where(x => x.OrderRequest.DeliveryAt.Date == DateTime.UtcNow.Date);
                    break;
                case Enums.DriverOrdersSelector.Coming:
                    query = query.Where(x => x.OrderRequest.DeliveryAt.Date > DateTime.UtcNow.Date);
                    break;
                case null: break;
                default:
                    throw new ArgumentOutOfRangeException($"Not implemented for type {request.Filter}");
            }

            var orders = await query.ToListAsync(cancellationToken);

            return new DriverGetAllOrdersResult
            {
                Items = _mapper.Map<IEnumerable<DriverGetAllOrdersDetailResult>>(orders)
            };
        }
    }
}
