﻿using ITHoot.Application.Order.Responses;
using MediatR;

namespace ITHoot.Application.Order.Queries
{
    public class DriverGetOrderQuery :IRequest<DriverGetOrderResult>
    {
        public int UserId { get; set; }
        public int OrderId { get; set; }
    }
}
