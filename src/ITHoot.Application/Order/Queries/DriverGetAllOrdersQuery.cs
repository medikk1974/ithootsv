﻿using ITHoot.Application.Order.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Order.Queries
{
    public class DriverGetAllOrdersQuery : IRequest<DriverGetAllOrdersResult>
    {
        public int UserId { get; set; }
        public Enums.DriverOrdersSelector? Filter { get; set; }
    }
}
