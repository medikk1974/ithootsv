﻿using ITHoot.Application.Order.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Order.Queries
{
    public class CarrierDownloadOrdersCsvQuery : IRequest<CarrierDownloadOrdersCsvResult>
    {
        public int UserId { get; set; }
        public Enums.OrderStatusTypes StatusTypes { get; set; }
    }
}
