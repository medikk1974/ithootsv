﻿using ITHoot.Application.Order.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Order.Queries
{
    public class CarrierGetAllOrdersQuery : IRequest<CarrierGetAllOrdersResult>
    {
        public int UserId { get; set; }
        public Enums.OrderStatusTypes StatusTypes { get; set; }

    }
}
