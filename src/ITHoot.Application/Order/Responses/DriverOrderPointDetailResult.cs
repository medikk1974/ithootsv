﻿namespace ITHoot.Application.Order.Responses
{
    public class DriverOrderPointDetailResult
    {
        public int Id { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }

        public string PlaceId { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
