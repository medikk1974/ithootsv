﻿namespace ITHoot.Application.Order.Responses
{
    public class CarrierDownloadOrdersCsvResult
    {
        public byte[] Item { get; set; }
        public string FileName { get; set; }
    }
}
