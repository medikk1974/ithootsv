﻿using ITHoot.Models;
using System;

namespace ITHoot.Application.Order.Responses
{
    public class CarrierGetAllOrdersDetailResult
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int VehicleId { get; set; }
        public string VehicleRegistrationNumber { get; set; }
        public double Distance { get; set; }
        public double Price { get; set; }

        public CarrierOrderPointDetailResult PointFrom { get; set; }
        public CarrierOrderPointDetailResult PointTo { get; set; }

        public DateTime DeliveryAt { get; set; }
        public Enums.OrderStatusTypes StatusTypes { get; set; }
    }
}
