﻿using System.Collections.Generic;

namespace ITHoot.Application.Order.Responses
{
    public class DriverGetAllOrdersResult
    {
        public IEnumerable<DriverGetAllOrdersDetailResult> Items { get; set; } = new List<DriverGetAllOrdersDetailResult>();
    }
}
