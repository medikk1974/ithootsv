﻿using System;

namespace ITHoot.Application.Order.Responses
{
    public class CarrierGetAllOrdersCsvResult
    {
        public string OrderNumber { get; set; }

        public string CustomerName { get; set; }

        public string VehicleRegistrationNumber { get; set; }

        public double Distance { get; set; }
        public double Price { get; set; }

        public CarrierOrderPointDetailResult PointFrom { get; set; }
        public CarrierOrderPointDetailResult PointTo { get; set; }

        public DateTime DeliveryAt { get; set; }
    }
}
