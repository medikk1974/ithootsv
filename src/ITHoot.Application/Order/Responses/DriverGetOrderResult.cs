﻿using System;

namespace ITHoot.Application.Order.Responses
{
    public class DriverGetOrderResult
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }

        public DriverOrderPointDetailResult PointFrom { get; set; }
        public DriverOrderPointDetailResult PointTo { get; set; }

        public DateTime DeliveryDate { get; set; }
    }
}
