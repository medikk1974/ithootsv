﻿using System.Collections.Generic;

namespace ITHoot.Application.Order.Responses
{
    public class CarrierGetAllOrdersResult
    {
        public IEnumerable<CarrierGetAllOrdersDetailResult> Items { get; set; } = new List<CarrierGetAllOrdersDetailResult>();
    }
}
