﻿using System;

namespace ITHoot.Application.Order.Responses
{
    public class DriverGetAllOrdersDetailResult
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public double Price { get; set; }
        public DateTime DeliveryAt { get; set; }
    }
}
