﻿namespace ITHoot.Application.Order.Responses
{
    public class CarrierOrderPointDetailResult
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
    }
}
