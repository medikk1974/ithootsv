﻿using ITHoot.Application.Extensions;
using ITHoot.Application.Order.Queries;
using ITHoot.Application.Order.Responses;

namespace ITHoot.Application.Order.Mappers
{
    public class OrderMapper : AppMapperBase
    {
        public OrderMapper()
        {
            CreateMap<Models.Entities.Order, CarrierGetAllOrdersDetailResult>()
                .Map(x => x.VehicleRegistrationNumber, x => x.Vehicle.RegistrationNumber)
                .Map(x => x.OrderNumber, x => $"{x.OrderRequest.Id}-{x.Id}")
                .IncludeMembers(r => r.OrderRequest);

            CreateMap<Models.Entities.OrderRequest, CarrierGetAllOrdersDetailResult>()
                .Map(x => x.CustomerName, x => x.Customer.Company.Name)
                .Map(x => x.PointFrom, x => x.Points.FirstBy(r => r.Order))
                .Map(x => x.PointTo, x => x.Points.LastBy(r => r.Order));

            CreateMap<Models.Entities.Point, CarrierOrderPointDetailResult>();

            CreateMap<Models.Entities.Point, DriverOrderPointDetailResult>();

            CreateMap<Models.Entities.Order, DriverGetAllOrdersDetailResult>()
                .Map(x => x.CompanyName, x => x.OrderRequest.Customer.Company.Name);

            CreateMap<Models.Entities.Order, DriverGetOrderResult>()
                .Map(x => x.PhoneNumber, x => x.OrderRequest.Customer.User.PhoneNumber)
                .IncludeMembers(r => r.OrderRequest);

            CreateMap<Models.Entities.OrderRequest, DriverGetOrderResult>()
                .Map(x => x.CustomerName, x => x.Customer.Company.Name)
                .Map(x => x.DeliveryDate, x => x.DeliveryAt)
                .Map(x => x.PointFrom, x => x.Points.FirstBy(r => r.Order))
                .Map(x => x.PointTo, x => x.Points.LastBy(r => r.Order));
            
            CreateMap<CarrierDownloadOrdersCsvQuery, CarrierGetAllOrdersQuery>();
        }
    }
}
