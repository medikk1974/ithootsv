﻿using ITHoot.Application.Contract.Responses;
using MediatR;
using System;
using System.Collections.Generic;

namespace ITHoot.Application.Contract.Commands
{
    public class AddContractCommand : IRequest<AddContractResult>
    {
        public int UserId { get; set; }
        public int CarrierId { get; set; }
        public string ContractNumber { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }

        public List<int> VehiclesId { get; set; } = new List<int>();
    }
}
