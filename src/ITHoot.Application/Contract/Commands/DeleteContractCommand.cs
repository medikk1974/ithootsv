﻿using ITHoot.Application.Contract.Responses;
using MediatR;

namespace ITHoot.Application.Contract.Commands
{
    public class DeleteContractCommand : IRequest<DeleteContractResult>
    {
        public int ContractId { get; set; }
        public int UserId { get; set; }
    }
}
