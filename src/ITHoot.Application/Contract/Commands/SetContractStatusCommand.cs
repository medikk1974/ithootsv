﻿using ITHoot.Application.Contract.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Contract.Commands
{
    public class SetContractStatusCommand : IRequest<SetContractStatusResult>
    {
        public int UserId { get; set; }
        public int Id { get; set; }
        public Enums.ContractStatusTypes StatusType { get; set; }
    }
}
