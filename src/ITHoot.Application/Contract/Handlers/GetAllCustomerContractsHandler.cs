﻿using AutoMapper;
using ITHoot.Application.Contract.Queries;
using ITHoot.Application.Contract.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Contract.Handlers
{
    public class GetAllCustomerContractsHandler : IRequestHandler<GetAllCustomerContractsQuery, AllContractResult<CustomerContractDetailsResult>>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Contract, int> _contractRepository;
        public GetAllCustomerContractsHandler(
            IRepository<Models.Entities.Contract, int> contractRepository,
            IMapper mapper)
        {
            _contractRepository = contractRepository;
            _mapper = mapper;
        }

        public async Task<AllContractResult<CustomerContractDetailsResult>> Handle(GetAllCustomerContractsQuery request, CancellationToken cancellationToken)
        {
            var q = await _contractRepository.Table
                .Include(x => x.Carrier)
                    .ThenInclude(x => x.Company)
                .Include(x => x.Vehicles)
                    .ThenInclude(x => x.Dimensions)
                .Include(x => x.Vehicles)
                    .ThenInclude(x => x.AssignedDriver)
                    .ThenInclude(x => x.User)
                .Where(x => x.Customer.UserId == request.UserId)
                .OrderBy(x=>x.StartAt)
                .ToListAsync(cancellationToken);

            return new AllContractResult<CustomerContractDetailsResult>
            {
                Items = _mapper.Map<IEnumerable<CustomerContractDetailsResult>>(q)
            };
        }
    }
}
