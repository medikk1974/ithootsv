﻿using ITHoot.Application.Contract.Commands;
using ITHoot.Application.Contract.Responses;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Repository;
using Microsoft.EntityFrameworkCore;

namespace ITHoot.Application.Contract.Handlers
{
    public class AddContractHandler : IRequestHandler<AddContractCommand, AddContractResult>
    {
        private readonly IRepository<Models.Entities.Contract, int> _contractRepository;
        private readonly IRepository<Models.Entities.Vehicle, int> _vehicleRepository;
        private readonly IRepository<Models.Entities.Customer, int> _customerRepository;
        private readonly IMapper _mapper;

        public AddContractHandler(
            IRepository<Models.Entities.Contract, int> contractRepository,
            IRepository<Models.Entities.Vehicle, int> vehicleRepository,
            IMapper mapper,
            IRepository<Models.Entities.Customer, int> customerRepository)
        {
            _contractRepository = contractRepository;
            _vehicleRepository = vehicleRepository;
            _mapper = mapper;
            _customerRepository = customerRepository;
        }

        public async Task<AddContractResult> Handle(AddContractCommand command, CancellationToken cancellationToken)
        {
            var vehicles = await _vehicleRepository.Table
                .Where(x => command.VehiclesId.Contains(x.Id)
                        && x.CarrierId == command.CarrierId)
                .ToListAsync(cancellationToken);
            //.GetListAsync(x => command.VehiclesId.Contains(x.Id));

            var customer = await _customerRepository.Table.FirstOrDefaultAsync(x => x.User.Id == command.UserId, cancellationToken);

            var contract = _mapper.Map<Models.Entities.Contract>(command);
            contract.CreatedAt = DateTime.UtcNow;
            contract.Customer = customer;
            contract.Vehicles = vehicles;

            await _contractRepository.CreateAsync(contract);

            return new AddContractResult();
        }
    }
}
