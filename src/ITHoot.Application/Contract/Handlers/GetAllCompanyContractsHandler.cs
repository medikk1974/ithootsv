﻿using AutoMapper;
using ITHoot.Application.Contract.Queries;
using ITHoot.Application.Contract.Responses;
using ITHoot.Models;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Contract.Handlers
{
    public class GetAllCompanyContractsHandler : IRequestHandler<GetAllCompanyContractsQuery, AllContractResult<CompanyContractDetailsResult>>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Contract, int> _contractRepository;
        public GetAllCompanyContractsHandler(
            IRepository<Models.Entities.Contract, int> contractRepository,
            IMapper mapper)
        {
            _contractRepository = contractRepository;
            _mapper = mapper;
        }

        public async Task<AllContractResult<CompanyContractDetailsResult>> Handle(GetAllCompanyContractsQuery request, CancellationToken cancellationToken)
        {
            var q = await _contractRepository.Table
                .Include(x => x.Customer)
                    .ThenInclude(x => x.Company)
                .Include(x => x.Vehicles)
                .Where(x => x.Carrier.UserId == request.UserId
                    && x.StatusType != Enums.ContractStatusTypes.Rejected)
                .OrderBy(x=>x.StartAt)
                .ToListAsync(cancellationToken);

            return new AllContractResult<CompanyContractDetailsResult>
            {
                Items = _mapper.Map<IEnumerable<CompanyContractDetailsResult>>(q)
            };
        }
    }
}
