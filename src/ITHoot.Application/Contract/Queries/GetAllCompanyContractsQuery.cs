﻿using ITHoot.Application.Contract.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Contract.Queries
{
    public class GetAllCompanyContractsQuery : IRequest<AllContractResult<CompanyContractDetailsResult>>
    {
        public Enums.ContractStatusTypes? StatusType { get; set; }
        public int UserId { get; set; }
    }
}
