﻿using ITHoot.Application.Contract.Commands;
using ITHoot.Application.Contract.Responses;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Contract.Mappers
{
    public class ContractMapper : AppMapperBase
    {
        public ContractMapper()
        {
            CreateMap<AddContractCommand, Models.Entities.Contract>();

            CreateMap<Models.Entities.Contract, CustomerContractDetailsResult>()
                .Map(x => x.CarrierName, x => x.Carrier.Company.Name);

            CreateMap<Models.Entities.Vehicle, ContractVehicleResult>()
                .Map(x => x.ServiceType, x => x.VehicleServiceType)
                .Map(x => x.AssignedDriver, x => x.AssignedDriver);


            CreateMap<Models.Entities.Contract, CompanyContractDetailsResult>()
                .Map(x => x.CustomerName, x => x.Customer.Company.Name);

            CreateMap<Models.Entities.Vehicle, VehicleNumberResult>();

            //CreateMap<Models.Entities.User, VehiclesDriverDetailsResult>()
        }
    }
}
