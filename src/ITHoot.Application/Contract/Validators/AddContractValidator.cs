﻿using ITHoot.Application.Contract.Commands;
using ITHoot.Repository;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Contract.Validators
{
    public class AddContractValidator : AbstractValidator<AddContractCommand>
    {
        private readonly IRepository<Models.Entities.Contract, int> _contractRepository;
        public AddContractValidator(IRepository<Models.Entities.Contract, int> contractRepository)
        {
            _contractRepository = contractRepository;

            RuleFor(x => x.UserId)
                .GreaterThan(0);

            RuleFor(x => x.CarrierId)
                .GreaterThan(0);

            RuleFor(x => x.EndAt)
                .GreaterThan(x => x.StartAt);

            RuleFor(x => x.VehiclesId)
                .NotEmpty();

            RuleFor(x => x)
                .NotEmpty()
                .MustAsync(ContractExistsAsync)
                .WithMessage("Contract number already exists");
        }

        private async Task<bool> ContractExistsAsync(AddContractCommand command, CancellationToken cancellationToken)
        {
            return !await _contractRepository.Table.AnyAsync(x =>
                x.ContractNumber == command.ContractNumber && x.Customer.UserId == command.UserId, cancellationToken);
        }
    }
}
