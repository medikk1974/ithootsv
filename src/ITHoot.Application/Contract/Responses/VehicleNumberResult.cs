﻿namespace ITHoot.Application.Contract.Responses
{
    public class VehicleNumberResult
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
    }
}
