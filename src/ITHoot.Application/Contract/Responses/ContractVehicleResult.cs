﻿using ITHoot.Application.Vehicle.Responses;
using ITHoot.Models;

namespace ITHoot.Application.Contract.Responses
{
    public class ContractVehicleResult
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public Enums.VehicleServiceTypes ServiceType { get; set; }
        public int MaxLoad { get; set; }
        public DimensionsResult Dimensions { get; set; }
        public VehiclesDriverDetailsResult AssignedDriver { get; set; }
    }
}
