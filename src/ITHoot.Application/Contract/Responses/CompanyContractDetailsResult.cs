﻿using ITHoot.Models;
using System;
using System.Collections.Generic;

namespace ITHoot.Application.Contract.Responses
{
    public class CompanyContractDetailsResult
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        //public string ContractNumber { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public Enums.ContractStatusTypes StatusType { get; set; }

        public IEnumerable<VehicleNumberResult> Vehicles { get; set; } = new List<VehicleNumberResult>();
    }
}
