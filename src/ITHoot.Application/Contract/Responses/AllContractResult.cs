﻿using System.Collections.Generic;

namespace ITHoot.Application.Contract.Responses
{
    public class AllContractResult<T>
    {
        public IEnumerable<T> Items { get; set; } = new List<T>();
    }
}
