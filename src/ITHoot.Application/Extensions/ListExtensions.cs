﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ITHoot.Application.Extensions
{
    public static class ListExtensions
    {
        public static TSource FirstBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return source
                .OrderBy(keySelector)
                .First();
        }

        public static TSource LastBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return source
                .OrderByDescending(keySelector)
                .First();
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> consumer)
        {
            foreach (var item in enumerable)
            {
                consumer(item);
            }
        }

        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (list is List<T> listT)
            {
                listT.AddRange(items);
            }
            else
            {
                foreach (var item in items)
                {
                    list.Add(item);
                }
            }
        }

        public static bool ItemsEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return first.OrderBy(r => r).SequenceEqual(second.OrderBy(r => r));
        }
    }
}
