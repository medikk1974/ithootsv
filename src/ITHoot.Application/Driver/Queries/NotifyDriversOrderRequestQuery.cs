﻿using System;
using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Queries
{
    public class NotifyDriversOrderRequestQuery : IRequest<NotifyDriversOrderRequestResult>
    {
        public int OrderRequestId { get; set; }
        public DateTime CreateAt { get; set; }
    }
}
