﻿using ITHoot.Application.Driver.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Driver.Queries
{
    public class GetAllCarrierDriversQuery : IRequest<GetAllCarrierDriversResult>
    {
        public int UserId { get; set; }
        public Enums.DriverStatusTypes? StatusType { get; set; }
    }
}