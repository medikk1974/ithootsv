﻿using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Queries
{
    public class GetDriverInfoQuery : IRequest<GetDriverInfoResult>
    {
        public int UserId { get; set; }
    }
}
