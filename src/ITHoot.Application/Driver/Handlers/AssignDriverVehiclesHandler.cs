﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Driver.Services.Interfaces;
using ITHoot.Application.Extensions;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class AssignDriverVehiclesHandler : IRequestHandler<AssignDriverVehiclesCommand, AssignDriverVehiclesResult>
    {
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;
        private readonly IDriverService _driverService;

        public AssignDriverVehiclesHandler(
            IDeletableRepository<Models.Entities.Driver, int> driverRepository,
            IDriverService driverService)
        {
            _driverRepository = driverRepository;
            _driverService = driverService;
        }

        public async Task<AssignDriverVehiclesResult> Handle(AssignDriverVehiclesCommand request, CancellationToken cancellationToken)
        {
            var driver = await _driverRepository.Table
                .Include(r => r.DriverVehicles)
                .Include(r => r.AssignedVehicle)
                .Include(r => r.Carrier)
                .ThenInclude(r => r.Vehicles)
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.DriverId, cancellationToken);

            if (driver == null)
            {
                throw ValidationException.Build(nameof(request.DriverId), "Driver not found");
            }

            driver.DriverVehicles.Clear();
            _driverService.AddDriverVehicles(driver, driver.Carrier.Vehicles, request.VehicleIds);

            await _driverRepository.SaveChangesAsync();

            return new AssignDriverVehiclesResult();
        }
    }
}