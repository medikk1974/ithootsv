﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class AssignVehicleHandler : IRequestHandler<AssignVehicleCommand, AssignVehicleResult>
    {
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;

        public AssignVehicleHandler(IDeletableRepository<Models.Entities.Driver, int> driverRepository)
        {
            _driverRepository = driverRepository;
        }

        public async Task<AssignVehicleResult> Handle(AssignVehicleCommand request, CancellationToken cancellationToken)
        {
            var driver = await _driverRepository.Table
                .Include(r => r.AssignedVehicle)
                .Include(r => r.DriverVehicles)
                .FirstOrDefaultAsync(r => r.UserId == request.UserId, cancellationToken);

            var vehicle = request.VehicleId.HasValue
                ? driver.DriverVehicles
                    .FirstOrDefault(r => r.Id == request.VehicleId)
                : null;

            if (request.VehicleId.HasValue && vehicle == null)
            {
                throw Extensions.ValidationException.Build(nameof(request.VehicleId), "Vehicle not found");
            }

            driver.AssignedVehicle = vehicle;
            
            await _driverRepository.SaveChangesAsync();

            return new AssignVehicleResult();
        }
    }
}