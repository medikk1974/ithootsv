﻿using AutoMapper;
using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Driver.Services.Interfaces;
using ITHoot.Application.Extensions;
using ITHoot.Application.User.Manager;
using ITHoot.Models;
using ITHoot.Models.Entities;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class AddDriverHandler : IRequestHandler<AddDriverCommand, AddDriverResult>
    {
        private readonly IMapper _mapper;
        private readonly IAppUserManager _userManager;
        private readonly IDriverService _driverService;
        private readonly IRepository<Carrier, int> _carrierRepository;

        public AddDriverHandler(
            IMapper mapper,
            IAppUserManager userManager,
            IDriverService driverService,
            IRepository<Carrier, int> carrierRepository)
        {
            _mapper = mapper;
            _userManager = userManager;
            _driverService = driverService;
            _carrierRepository = carrierRepository;
        }

        public async Task<AddDriverResult> Handle(AddDriverCommand request, CancellationToken cancellationToken)
        {
            using var tran = _carrierRepository.BeginTransaction();
            var user = _mapper.Map<Models.Entities.User>(request);
            user.CreatedAt = DateTime.UtcNow;
            user.LanguageId = (int)Constants.DefaultLanguage;

            var createdUser = await _userManager.CreateAsync(user, request.Password);
            if (!createdUser.Succeeded)
            {
                throw ValidationException.Build("User", createdUser.Errors);
            }

            var addRole = await _userManager.AddToRoleAsync(user, Enums.RoleTypes.Driver.ToString());
            if (!addRole.Succeeded)
            {
                throw ValidationException.Build("UserRole", addRole.Errors);
            }

            var driver = new Models.Entities.Driver
            {
                UserId = user.Id,
                CreatedAt = DateTime.UtcNow
            };

            var carrier = await _carrierRepository.Table
                .Include(r => r.Vehicles)
                .FirstOrDefaultAsync(r => r.UserId == request.UserId, cancellationToken);

            _driverService.AddDriverVehicles(driver, carrier.Vehicles, request.VehicleIds);

            carrier.Drivers.Add(driver);
            
            await _carrierRepository.SaveChangesAsync();
            
            tran.Commit();

            return new AddDriverResult();
        }
    }
}