﻿using AutoMapper;
using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Driver.Services.Interfaces;
using ITHoot.Application.Extensions;
using ITHoot.Application.User.Manager;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class EditDriverHandler : IRequestHandler<EditDriverCommand, EditDriverResult>
    {
        private readonly IMapper _mapper;
        private readonly IAppUserManager _userManager;
        private readonly IDriverService _driverService;
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;

        public EditDriverHandler(
            IMapper mapper,
            IAppUserManager userManager, IDriverService driverService,
            IDeletableRepository<Models.Entities.Driver, int> driverRepository)
        {
            _mapper = mapper;
            _userManager = userManager;
            _driverService = driverService;
            _driverRepository = driverRepository;
        }

        public async Task<EditDriverResult> Handle(EditDriverCommand request, CancellationToken cancellationToken)
        {
            using var tran = _driverRepository.BeginTransaction();

            var driver = await _driverRepository.Table
                .Include(r => r.Carrier)
                .ThenInclude(r => r.Vehicles)
                .Include(x => x.User)
                .Include(x => x.AssignedVehicle)
                .Include(r => r.DriverVehicles)
                .FirstOrDefaultAsync(x => x.Carrier.UserId == request.UserId
                                        && x.Id == request.DriverId, cancellationToken);

            if (driver == null)
            {
                throw ValidationException.Build(nameof(request.DriverId), "Driver not found");
            }

            driver.User = _mapper.Map(request, driver.User);
            driver.User.UpdatedAt = DateTime.UtcNow;
            driver.UpdatedAt = driver.User.UpdatedAt;

            var editEmail = await _userManager.SetEmailAsync(driver.User, request.Email);
            if (!editEmail.Succeeded)
            {
                throw ValidationException.Build(nameof(request.Email), editEmail.Errors);
            }

            driver.DriverVehicles.Clear();
            _driverService.AddDriverVehicles(driver, driver.Carrier.Vehicles, request.VehicleIds);

            await _driverRepository.UpdateAsync(driver);

            tran.Commit();

            return new EditDriverResult();
        }
    }
}
