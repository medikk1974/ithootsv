﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Driver.Handlers
{
    public class ChangeDriverStatusHandler : IRequestHandler<ChangeDriverStatusCommand, ChangeDriverStatusResult>
    {
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;

        public ChangeDriverStatusHandler(IDeletableRepository<Models.Entities.Driver, int> driverRepository)
        {
            _driverRepository = driverRepository;
        }

        public async Task<ChangeDriverStatusResult> Handle(ChangeDriverStatusCommand request, CancellationToken cancellationToken)
        {
            var driver = await _driverRepository.Table
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.DriverId, cancellationToken);

            if (driver == null)
            {
                throw ValidationException.Build(nameof(request.DriverId), "Driver not found");
            }

            driver.StatusType = request.StatusType;
            
            await _driverRepository.SaveChangesAsync();

            return new ChangeDriverStatusResult();
        }
    }
}