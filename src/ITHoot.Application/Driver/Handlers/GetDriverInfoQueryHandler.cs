﻿using AutoMapper;
using ITHoot.Application.Driver.Queries;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Extensions;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class GetDriverInfoQueryHandler : IRequestHandler<GetDriverInfoQuery, GetDriverInfoResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Driver, int> _driverRepository;
        public GetDriverInfoQueryHandler(
            IMapper mapper,
            IRepository<Models.Entities.Driver, int> driverRepository)
        {
            _mapper = mapper;
            _driverRepository = driverRepository;
        }

        public async Task<GetDriverInfoResult> Handle(GetDriverInfoQuery request, CancellationToken cancellationToken)
        {
            var driver = await _driverRepository.Table
                .Include(r => r.User)
                .FirstOrDefaultAsync(r => r.UserId == request.UserId, cancellationToken);

            if (driver == null)
            {
                throw ValidationException.Build(nameof(request.UserId), "Driver not found");
            }

            var result = _mapper.Map<GetDriverInfoResult>(driver);

            return result;
        }
    }
}
