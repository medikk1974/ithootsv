﻿using AutoMapper;
using ITHoot.Application.Driver.Queries;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Extensions;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class GetDriverHandler : IRequestHandler<GetDriverQuery, GetDriverResult>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Models.Entities.Driver, int> _driverRepository;

        public GetDriverHandler(IMapper mapper, IRepository<Models.Entities.Driver, int> driverRepository)
        {
            _mapper = mapper;
            _driverRepository = driverRepository;
        }

        public async Task<GetDriverResult> Handle(GetDriverQuery request, CancellationToken cancellationToken)
        {
            var driver = await _driverRepository.Table
                .Include(r => r.User)
                .Include(r => r.AssignedVehicle)
                .Include(r => r.DriverVehicles)
                .FirstOrDefaultAsync(r => r.Carrier.UserId == request.UserId
                                          && r.Id == request.DriverId, cancellationToken);

            if (driver == null)
            {
                throw ValidationException.Build(nameof(request.DriverId), "Driver not found");
            }

            var result = _mapper.Map<GetDriverResult>(driver);
            //_mapper.Map(driver.User, result);

            return result;
        }
    }
}