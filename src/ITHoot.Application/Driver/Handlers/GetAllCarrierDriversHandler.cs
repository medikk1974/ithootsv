﻿using AutoMapper;
using ITHoot.Application.Driver.Queries;
using ITHoot.Application.Driver.Responses;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class GetAllCarrierDriversHandler : IRequestHandler<GetAllCarrierDriversQuery, GetAllCarrierDriversResult>
    {
        private readonly IMapper _mapper;
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;

        public GetAllCarrierDriversHandler(
            IMapper mapper,
            IDeletableRepository<Models.Entities.Driver, int> driverRepository)
        {
            _mapper = mapper;
            _driverRepository = driverRepository;
        }

        public async Task<GetAllCarrierDriversResult> Handle(GetAllCarrierDriversQuery request, CancellationToken cancellationToken)
        {
            var q = _driverRepository.Table
                .Include(r => r.User)
                .Include(r => r.AssignedVehicle)
                .Include(r => r.DriverVehicles)
                .Where(r => r.Carrier.UserId == request.UserId);

            if (request.StatusType.HasValue)
            {
                q = q.Where(r => r.StatusType == request.StatusType);
            }

            var drivers = await q.ToListAsync(cancellationToken);
            
            return new GetAllCarrierDriversResult
            {
                Items = _mapper.Map<IEnumerable<GetDriverResult>>(drivers)
            };
        }
    }
}