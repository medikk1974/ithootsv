﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Extensions;
using ITHoot.Application.User.Manager;
using ITHoot.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Handlers
{
    public class SetDriverPasswordHandler : IRequestHandler<SetDriverPasswordCommand, SetDriverPasswordResult>
    {
        private readonly IAppUserManager _userManager;
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;

        public SetDriverPasswordHandler(IAppUserManager userManager,
            IDeletableRepository<Models.Entities.Driver, int> driverRepository)
        {
            _userManager = userManager;
            _driverRepository = driverRepository;
        }

        public async Task<SetDriverPasswordResult> Handle(SetDriverPasswordCommand command, CancellationToken cancellationToken)
        {
            var driver = await _driverRepository.Table
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Carrier.UserId == command.UserId
                                          && x.Id == command.DriverId, cancellationToken);

            if (driver == null)
            {
                throw ValidationException.Build(nameof(command.DriverId), "Driver not found");
            }

            var token = await _userManager.GeneratePasswordResetTokenAsync(driver.User);
            var resetPasswordResult = await _userManager.ResetPasswordAsync(driver.User, token, command.Password);

            if (!resetPasswordResult.Succeeded)
            {
                throw ValidationException.Build(nameof(command.Password), resetPasswordResult.Errors);
            }

            return new SetDriverPasswordResult();
        }
    }
}
