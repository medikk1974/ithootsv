﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Driver.Hubs;
using ITHoot.Application.Driver.Hubs.Models;
using ITHoot.Application.Driver.Queries;
using ITHoot.Application.Driver.Responses;
using ITHoot.Models;
using MediatR;
using Microsoft.AspNetCore.SignalR;

namespace ITHoot.Application.Driver.Handlers
{
    public class NotifyDriversOrderRequestHandler : IRequestHandler<NotifyDriversOrderRequestQuery, NotifyDriversOrderRequestResult>
    {
        private readonly IMapper _mapper;
        private readonly IHubContext<DriverHub> _hubContext;

        public NotifyDriversOrderRequestHandler(
            IMapper mapper,
            IHubContext<DriverHub> hubContext)
        {
            _mapper = mapper;
            _hubContext = hubContext;
        }

        public async Task<NotifyDriversOrderRequestResult> Handle(NotifyDriversOrderRequestQuery request, CancellationToken cancellationToken)
        {
            var message = _mapper.Map<DriverOrderRequestMessage>(request);

            await _hubContext.Clients
                .Group(Constants.ActiveDriversGroupName)
                .SendAsync("receiveMessage", message, cancellationToken);

            return new NotifyDriversOrderRequestResult();
        }
    }
}
