﻿using ITHoot.Application.Driver.Services.Interfaces;
using ITHoot.Application.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace ITHoot.Application.Driver.Services
{
    public class DriverService : IDriverService
    {
        public void AddDriverVehicles(Models.Entities.Driver driver, IEnumerable<Models.Entities.Vehicle> vehicles, IEnumerable<int> vehicleIds)
        {
            var selectedVehicles = vehicles
                .Where(r => vehicleIds
                    .Contains(r.Id));

            if (!selectedVehicles.Select(r => r.Id).ItemsEqual(vehicleIds))
            {
                throw ValidationException.Build(nameof(vehicleIds), "Driver not found");
            }

            foreach (var item in selectedVehicles)
            {
                driver.DriverVehicles.Add(item);
            }
        }
    }
}
