﻿using System.Collections.Generic;

namespace ITHoot.Application.Driver.Services.Interfaces
{
    public interface IDriverService
    {
        void AddDriverVehicles(Models.Entities.Driver driver, IEnumerable<Models.Entities.Vehicle> vehicles, IEnumerable<int> vehicleIds);
    }
}
