﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.Driver.Hubs.Models;
using ITHoot.Application.Driver.Queries;
using ITHoot.Application.Driver.Responses;
using ITHoot.Application.Extensions;

namespace ITHoot.Application.Driver.Mappers
{
    public class DriverMapper : AppMapperBase
    {
        public DriverMapper()
        {
            CreateMap<Models.Entities.Driver, GetDriverResult>()
                .IncludeMembers(r => r.User)
                .Map(m => m.DriverVehicles, r => r.DriverVehicles)
                .Map(m => m.AssignedVehicle, r => r.AssignedVehicle);

            CreateMap<Models.Entities.User, GetDriverResult>()
                .Ignore(r => r.Id)
                .Ignore(r => r.CreatedAt)
                .Ignore(r => r.StatusType)
                .Ignore(r => r.AssignedVehicle)
                .Ignore(r => r.DriverVehicles);

            CreateMap<Models.Entities.Vehicle, DriverVehicleResult>();

            CreateMap<EditDriverCommand, Models.Entities.User>()
                .Map(m => m.UserName, r => r.Email);
            
            CreateMap<AddDriverCommand, Models.Entities.User>()
                .Map(m => m.UserName, r => r.Email);

            CreateMap<Models.Entities.Driver, GetDriverInfoResult>()
                .IncludeMembers(r => r.User);

            CreateMap<Models.Entities.User, GetDriverInfoResult>();

            CreateMap<NotifyDriversOrderRequestQuery, DriverOrderRequestMessage>();
        }
    }
}
