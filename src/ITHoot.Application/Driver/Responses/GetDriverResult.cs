﻿using System;
using System.Collections.Generic;
using ITHoot.Models;

namespace ITHoot.Application.Driver.Responses
{
    public class GetDriverResult
    {
        public int Id { get; set; }
        public Enums.DriverStatusTypes StatusType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedAt { get; set; }

        public DriverVehicleResult AssignedVehicle { get; set; }
        public IEnumerable<DriverVehicleResult> DriverVehicles { get; set; } = new List<DriverVehicleResult>();
    }
}