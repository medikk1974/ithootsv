﻿namespace ITHoot.Application.Driver.Responses
{
    public class GetDriverInfoResult
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
