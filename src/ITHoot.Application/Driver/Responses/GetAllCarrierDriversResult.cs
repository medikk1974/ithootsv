﻿using System.Collections.Generic;

namespace ITHoot.Application.Driver.Responses
{
    public class GetAllCarrierDriversResult
    {
        public IEnumerable<GetDriverResult> Items { get; set; } = new List<GetDriverResult>();
    }
}
