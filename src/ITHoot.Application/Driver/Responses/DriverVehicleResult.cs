﻿namespace ITHoot.Application.Driver.Responses
{
    public class DriverVehicleResult
    {
        public int Id { get; set; }

        public int MaxLoad { get; set; }
        public string RegistrationNumber { get; set; }
    }
}
