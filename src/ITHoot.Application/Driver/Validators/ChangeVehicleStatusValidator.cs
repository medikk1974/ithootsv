﻿using ITHoot.Application.Driver.Commands;
using FluentValidation;

namespace ITHoot.Application.Driver.Validators
{
    public class ChangeDriverStatusValidator : AbstractValidator<ChangeDriverStatusCommand>
    {
        public ChangeDriverStatusValidator()
        {
            RuleFor(c => c.UserId)
                .GreaterThan(0);

            RuleFor(c => c.DriverId)
                .GreaterThan(0);

            RuleFor(c => c.StatusType)
                .IsInEnum();
        }
    }
}