﻿using ITHoot.Application.Driver.Commands;
using FluentValidation;

namespace ITHoot.Application.Driver.Validators
{
    public class SetDriverPasswordValidator : AbstractValidator<SetDriverPasswordCommand>
    {
        public SetDriverPasswordValidator()
        {
            RuleFor(x => x.Password)
                .NotEmpty();

            RuleFor(x => x.DriverId)
                .GreaterThan(0);
        }
    }
}
