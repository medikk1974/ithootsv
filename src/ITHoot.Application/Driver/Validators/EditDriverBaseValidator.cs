﻿using ITHoot.Application.Driver.Commands;
using FluentValidation;

namespace ITHoot.Application.Driver.Validators
{
    public class EditDriverBaseValidator : AbstractValidator<EditDriverBaseCommand>
    {
        public EditDriverBaseValidator()
        {
           RuleFor(x => x.Email)
                .EmailAddress()
                .NotEmpty();

            RuleFor(x => x.PhoneNumber)
                .NotEmpty();
        }
    }
}
