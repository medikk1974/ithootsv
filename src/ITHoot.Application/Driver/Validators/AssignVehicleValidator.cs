﻿using ITHoot.Application.Driver.Commands;
using FluentValidation;

namespace ITHoot.Application.Driver.Validators
{
    public class AssignVehicleValidator : AbstractValidator<AssignVehicleCommand>
    {
        public AssignVehicleValidator()
        {
            RuleFor(c => c.UserId)
                .GreaterThan(0);
        }
    }
}