﻿using ITHoot.Application.Driver.Commands;
using FluentValidation;

namespace ITHoot.Application.Driver.Validators
{
    public class AssignDriverVehiclesValidator : AbstractValidator<AssignDriverVehiclesCommand>
    {
        public AssignDriverVehiclesValidator()
        {
            RuleFor(c => c.UserId)
                .GreaterThan(0);

            RuleFor(c => c.DriverId)
                .GreaterThan(0);
        }
    }
}