﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Driver.Commands;
using ITHoot.Application.User.Manager;
using FluentValidation;

namespace ITHoot.Application.Driver.Validators
{
    public class AddDriverValidator : AbstractValidator<AddDriverCommand>
    {
        private readonly IAppUserManager _userManager;

        public AddDriverValidator(IAppUserManager userManager)
        {
            _userManager = userManager;

            RuleFor(r => r)
                .SetValidator(new EditDriverBaseValidator());

            RuleFor(x => x.Email)
                .MustAsync(ValidateEmailExistAsync)
                .WithMessage("Email address already exists");
            
            RuleFor(x=>x.PhoneNumber)
                .MustAsync(ValidatePhoneNumberExistAsync)
                .WithMessage("Phone number already exists");

            RuleFor(x => x.Password)
                .NotEmpty();
        }

        private async Task<bool> ValidateEmailExistAsync(string email, CancellationToken cancellationToken)
        {
            var existUser = await _userManager.FindByEmailAsync(email);
            return existUser == null;
        }

        private async Task<bool> ValidatePhoneNumberExistAsync(string phoneNumber, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByPhoneAsync(phoneNumber);
            return user == null;
        }
    }
}