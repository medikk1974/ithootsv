﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Application.User.Manager;
using ITHoot.Repository;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Validators
{
    public class EditDriverValidator : AbstractValidator<EditDriverCommand>
    {
        private readonly IAppUserManager _userManager;
        private readonly IDeletableRepository<Models.Entities.Driver, int> _driverRepository;

        public EditDriverValidator(IAppUserManager userManager,
            IDeletableRepository<Models.Entities.Driver, int> driverRepository)
        {
            _userManager = userManager;
            _driverRepository = driverRepository;

            RuleFor(r => r)
                .SetValidator(new EditDriverBaseValidator());

            RuleFor(x => x.DriverId)
                .GreaterThan(0);
            
            RuleFor(x => x)
                .MustAsync(ValidateEmailExistAsync)
                .WithMessage("Email address already exists");

            RuleFor(x => x)
                .MustAsync(ValidatePhoneNumberExistAsync)
                .WithMessage("Phone number already exists");
        }

        private async Task<bool> ValidateEmailExistAsync(EditDriverCommand command, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(command.Email);

            if (user == null)
                return true;

            return await _driverRepository.Table
                    .AnyAsync(x => x.UserId == user.Id
                        && x.Id == command.DriverId, cancellationToken);
        }

        private async Task<bool> ValidatePhoneNumberExistAsync(EditDriverCommand command, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByPhoneAsync(command.PhoneNumber);

            if (user == null)
                return true;

            return await _driverRepository.Table
                    .AnyAsync(x => x.UserId == user.Id
                        && x.Id == command.DriverId, cancellationToken);
        }
    }
}
