﻿using System.Collections.Generic;

namespace ITHoot.Application.Driver.Commands
{
    public class EditDriverBaseCommand
    {
        public int UserId { get; set; }
        public IEnumerable<int> VehicleIds { get; set; } = new List<int>();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}