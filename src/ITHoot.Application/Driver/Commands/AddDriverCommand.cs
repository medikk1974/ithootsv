﻿using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Commands
{
    public class AddDriverCommand : EditDriverBaseCommand, IRequest<AddDriverResult>
    {
        public string Password { get; set; }
    }
}