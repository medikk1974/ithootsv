﻿using ITHoot.Application.Driver.Responses;
using ITHoot.Models;
using MediatR;

namespace ITHoot.Application.Driver.Commands
{
    public class ChangeDriverStatusCommand : IRequest<ChangeDriverStatusResult>
    {
        public int UserId { get; set; }
        public int DriverId { get; set; }
        public Enums.DriverStatusTypes StatusType { get; set; }
    }
}
