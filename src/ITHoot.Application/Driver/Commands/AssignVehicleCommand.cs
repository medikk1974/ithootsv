﻿using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Commands
{
    public class AssignVehicleCommand : IRequest<AssignVehicleResult>
    {
        public int UserId { get; set; }
        public int? VehicleId { get; set; }
    }
}
