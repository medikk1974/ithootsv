﻿using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Commands
{
    public class EditDriverCommand : EditDriverBaseCommand, IRequest<EditDriverResult>
    {
        public int DriverId { get; set; }
    }
}