﻿using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Commands
{
    public class SetDriverPasswordCommand : IRequest<SetDriverPasswordResult>
    {
        public int DriverId { get; set; }
        public int UserId { get; set; }
        public string Password { get; set; }
    }
}
