﻿using System.Collections.Generic;
using ITHoot.Application.Driver.Responses;
using MediatR;

namespace ITHoot.Application.Driver.Commands
{
    public class AssignDriverVehiclesCommand : IRequest<AssignDriverVehiclesResult>
    {
        public int UserId { get; set; }
        public int DriverId { get; set; }

        public IEnumerable<int> VehicleIds { get; set; } = new List<int>();
    }
}
