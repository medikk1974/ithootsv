﻿using ITHoot.Application.Driver.Commands;
using ITHoot.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.OpenApi.Models;
using SignalRSwaggerGen.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ITHoot.Application.Driver.Hubs
{
    [Authorize(Policy = Constants.Policy.Driver)]
    [SignalRHub("/hubs/driver")]
    public class DriverHub : Hub
    {
        private readonly IMediator _mediator;

        public DriverHub(IMediator mediator)
        {
            _mediator = mediator;
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            // todo discuss what to do if the driver lost connect

            return base.OnDisconnectedAsync(exception);
        }

        [SignalRMethod("assignVehicle", OperationType.Put)]
        public async Task AssignVehicle([SignalRArg] int? vehicleId)
        {
            var command = new AssignVehicleCommand
            {
                UserId = GetLoggedUserId(),
                VehicleId = vehicleId
            };

            try
            {
                await _mediator.Send(command);
            }
            catch (Exception exception)
            {
                // todo add to log
                throw;
            }

            if (vehicleId.HasValue)
                await Groups.AddToGroupAsync(Context.ConnectionId, Constants.ActiveDriversGroupName);
            else
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, Constants.ActiveDriversGroupName);
        }

        private int GetLoggedUserId()
        {
            var userId = Context.User?.Claims
                .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (userId == null)
            {
                throw new KeyNotFoundException("UserId not in claims");
            }

            return int.Parse(userId.Value);
        }
    }
}