﻿using System;

namespace ITHoot.Application.Driver.Hubs.Models
{
    public class DriverOrderRequestMessage
    {
        public int OrderRequestId { get; set; }
        public DateTime CreateAt { get; set; }
    }
}
