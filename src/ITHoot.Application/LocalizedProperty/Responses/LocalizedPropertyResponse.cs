﻿using System;

namespace ITHoot.Application.LocalizedProperty.Responses
{
    public class LocalizedPropertyResponse
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int LanguageId { get; set; }

        public string LocaleKeyGroup { get; set; }
        public string LocaleKey { get; set; }
        public string LocaleValue { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}