﻿using MediatR;

namespace ITHoot.Application.LocalizedProperty.Queries
{
    public class GetLocalizedValueQuery : IRequest<string>
    {
        public GetLocalizedValueQuery(int languageId, int entityId, string localeKeyGroup, string localeKey)
        {
            LanguageId = languageId;
            EntityId = entityId;
            LocaleKeyGroup = localeKeyGroup;
            LocaleKey = localeKey;
        }

        public int LanguageId { get; }
        public int EntityId { get; }
        public string LocaleKeyGroup { get; }
        public string LocaleKey { get; }
    }
}