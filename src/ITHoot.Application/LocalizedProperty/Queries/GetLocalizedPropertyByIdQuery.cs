﻿using ITHoot.Application.LocalizedProperty.Responses;
using MediatR;

namespace ITHoot.Application.LocalizedProperty.Queries
{
    public class GetLocalizedPropertyByIdQuery : IRequest<LocalizedPropertyResponse>
    {
        public GetLocalizedPropertyByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}