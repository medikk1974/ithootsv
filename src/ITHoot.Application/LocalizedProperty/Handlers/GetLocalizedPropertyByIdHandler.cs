﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Caching.Interfaces;
using ITHoot.Application.LocalizedProperty.Queries;
using ITHoot.Application.LocalizedProperty.Responses;
using ITHoot.Repository;
using MediatR;

namespace ITHoot.Application.LocalizedProperty.Handlers
{
    public class
        GetLocalizedPropertyByIdHandler : IRequestHandler<GetLocalizedPropertyByIdQuery, LocalizedPropertyResponse>
    {
        private readonly ICacheKeyService _cacheKeyService;

        private readonly IRepository<Models.Entities.Localization.LocalizedProperty, int> _localizedPropertyRepository;

        private readonly IStaticCacheManager _staticCacheManager;

        public GetLocalizedPropertyByIdHandler(
            IRepository<Models.Entities.Localization.LocalizedProperty, int> localizedPropertyRepository,
            ICacheKeyService cacheKeyService,
            IStaticCacheManager staticCacheManager)
        {
            _localizedPropertyRepository = localizedPropertyRepository;
            _cacheKeyService = cacheKeyService;
            _staticCacheManager = staticCacheManager;
        }

        public Task<LocalizedPropertyResponse> Handle(GetLocalizedPropertyByIdQuery request,
            CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}