﻿using ITHoot.Application.Identity.Responses;
using MediatR;

namespace ITHoot.Application.Identity.Commands.Customer
{
    public class CustomerRegistrationCommand : IRequest<AuthenticationResult>
    {
        public string CompanyName { get; set; }
        public int CompanyCode { get; set; }
        public bool VATPayer { get; set; }
        public string VATCode { get; set; }
        public AddressCommand RegistrationAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
