﻿using ITHoot.Application.Identity.Responses;
using MediatR;

namespace ITHoot.Application.Identity.Commands.Driver
{
    public class DriverRegistrationCommand : IRequest<AuthenticationResult>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonalCode { get; set; }
        public string VATCode { get; set; }
        public AddressCommand RegistrationAddress { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }
}