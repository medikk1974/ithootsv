﻿using ITHoot.Application.Identity.Responses;
using MediatR;

namespace ITHoot.Application.Identity.Commands
{
    public class WebLoginCommand : IRequest<AuthenticationResult>
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}