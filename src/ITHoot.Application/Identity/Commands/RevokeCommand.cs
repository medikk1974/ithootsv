﻿using MediatR;

namespace ITHoot.Application.Identity.Commands
{
    public class RevokeCommand : IRequest<bool>
    {
        public int UserId { get; set; }
    }
}