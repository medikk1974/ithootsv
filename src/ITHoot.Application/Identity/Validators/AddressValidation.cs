﻿using ITHoot.Application.Identity.Commands;
using FluentValidation;

namespace ITHoot.Application.Identity.Validators
{
    public class AddressValidation : AbstractValidator<AddressCommand>
    {
        public AddressValidation()
        {
            RuleFor(x => x.Country)
                .NotEmpty();

            RuleFor(x => x.City)
                .NotEmpty();

            RuleFor(x => x.Street)
                .NotEmpty();

            RuleFor(x => x.Building)
                .NotEmpty();

            RuleFor(x => x.Apartment)
                .NotEmpty();
        }
    }
}
