﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Email;
using ITHoot.Application.Identity.Commands.Customer;
using ITHoot.Application.SMS;
using ITHoot.Application.User.Manager;
using ITHoot.Repository;
using FluentValidation;

namespace ITHoot.Application.Identity.Validators.Customer
{
    public class CustomerRegistrationValidation : AbstractValidator<CustomerRegistrationCommand>
    {
        private readonly IAppUserManager _userManager;
        private readonly IRepository<Models.Entities.Company, int> _companyRepository;
        private readonly IEmailSender _emailSender;
        private readonly ISMSSender _smsSender;

        public CustomerRegistrationValidation(
            IAppUserManager userManager,
            IRepository<Models.Entities.Company, int> companyRepository,
            IEmailSender emailSender,
            ISMSSender smsSender)
        {
            _userManager = userManager;
            _companyRepository = companyRepository;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _companyRepository = companyRepository;

            RuleFor(x => x)
                .NotEmpty()
                .MustAsync(ValidateCompanyExistAsync)
                .WithMessage("Company already exists");

            RuleFor(x => x.Email)
                .EmailAddress()
                .NotEmpty()
                .MustAsync(ValidateEmailExistAsync)
                .WithMessage("Email address already exists")
                .MustAsync(ValidateEmailAsync)
                .WithMessage("Email address is invalid");

            RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .MustAsync(ValidatePhoneAsync)
                .WithMessage("Phone number is invalid");
            RuleFor(x => x.PhoneNumber)
                .MustAsync(ValidatePhoneNumberExistAsync)
                .WithMessage("Phone number already exists");

            RuleFor(x => x.Password)
                .NotEmpty();

            RuleFor(x => x.RegistrationAddress)
                .SetValidator(new AddressValidation());
        }

        private async Task<bool> ValidateCompanyExistAsync(CustomerRegistrationCommand command, CancellationToken cancellationToken)
        {
            return !await _companyRepository
                .AnyAsync(r => r.Code == command.CompanyCode
                               || r.Name == command.CompanyName);
        }

        private async Task<bool> ValidateEmailExistAsync(string email, CancellationToken cancellationToken)
        {
            var existUser = await _userManager.FindByEmailAsync(email);
            return existUser == null;
        }

        private async Task<bool> ValidateEmailAsync(string email, CancellationToken cancellationToken)
        {
            var result = await _emailSender.ValidateEmailAsync(email);
            return result.Success;
        }

        private async Task<bool> ValidatePhoneAsync(string phoneNumber, CancellationToken cancellationToken)
        {
            var result = await _smsSender.ValidatePhoneAsync(phoneNumber);
            return result.Success;
        }

        private async Task<bool> ValidatePhoneNumberExistAsync(string phoneNumber, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByPhoneAsync(phoneNumber);
            return user == null;
        }
    }
}
