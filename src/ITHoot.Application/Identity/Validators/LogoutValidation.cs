﻿using ITHoot.Application.Identity.Commands;
using FluentValidation;

namespace ITHoot.Application.Identity.Validators
{
    public class LogoutValidation : AbstractValidator<LogoutCommand>
    {
        public LogoutValidation()
        {
            RuleFor(x => x.RefreshToken)
                .NotEmpty();
        }
    }
}