﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Email;
using ITHoot.Application.Identity.Commands.Driver;
using ITHoot.Application.SMS;
using ITHoot.Application.User.Manager;
using FluentValidation;

namespace ITHoot.Application.Identity.Validators.Driver
{
    public class DriverRegisterValidation : AbstractValidator<DriverRegistrationCommand>
    {
        private readonly IAppUserManager _userManager;
        private readonly IEmailSender _emailSender;
        private readonly ISMSSender _smsSender;
        
        public DriverRegisterValidation(
            IAppUserManager userManager,
            IEmailSender emailSender,
            ISMSSender smsSender)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _smsSender = smsSender;

            RuleFor(x => x.Email)
                .EmailAddress()
                .NotEmpty()
                .MustAsync(ValidateEmailExistAsync)
                .WithMessage("Email address already exists")
                .MustAsync(ValidateEmailAsync)
                .WithMessage("Email address is invalid");

            RuleFor(x => x.PhoneNumber)
                .NotEmpty()
                .MustAsync(ValidatePhoneAsync)
                .WithMessage("Phone number is invalid");

            RuleFor(x => x.Password)
                .NotEmpty();

            RuleFor(x => x.RegistrationAddress)
                .SetValidator(new AddressValidation());
        }

        private async Task<bool> ValidateEmailExistAsync(string email, CancellationToken cancellationToken)
        {
            var existUser = await _userManager.FindByEmailAsync(email);
            return existUser == null;
        }

        private async Task<bool> ValidateEmailAsync(string email, CancellationToken cancellationToken)
        {
            var result = await _emailSender.ValidateEmailAsync(email);
            return result.Success;
        }

        private async Task<bool> ValidatePhoneAsync(string phoneNumber, CancellationToken cancellationToken)
        {
            var result = await _smsSender.ValidatePhoneAsync(phoneNumber);
            return result.Success;
        }
    }
}