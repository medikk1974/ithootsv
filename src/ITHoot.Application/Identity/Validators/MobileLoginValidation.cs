﻿using ITHoot.Application.Identity.Commands;
using FluentValidation;

namespace ITHoot.Application.Identity.Validators
{
    public class MobileLoginValidation : AbstractValidator<MobileLoginCommand>
    {
        public MobileLoginValidation()
        {
            RuleFor(x => x.PhoneNumber)
                .NotEmpty();

            RuleFor(x => x.Password)
                .NotEmpty();
        }
    }
}