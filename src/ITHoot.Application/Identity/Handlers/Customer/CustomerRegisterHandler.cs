﻿using AutoMapper;
using ITHoot.Application.Identity.Commands.Customer;
using ITHoot.Application.Identity.Responses;
using ITHoot.Application.Identity.Services.Interfaces;
using ITHoot.Application.User.Manager;
using ITHoot.Models;
using ITHoot.Repository;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Identity.Handlers.Customer
{
    public class CustomerRegisterHandler : IRequestHandler<CustomerRegistrationCommand, AuthenticationResult>
    {
        private readonly IMapper _mapper;
        private readonly IAppUserManager _userManager;
        private readonly IIdentityService _identityService;
        private readonly IRepository<Models.Entities.Customer, int> _customerRepository;

        public CustomerRegisterHandler(
            IMapper mapper,
            IAppUserManager userManager,
            IIdentityService identityService,
            IRepository<Models.Entities.Customer, int> customerRepository)
        {
            _mapper = mapper;
            _identityService = identityService;
            _customerRepository = customerRepository;
            _userManager = userManager;
        }

        public async Task<AuthenticationResult> Handle(CustomerRegistrationCommand command, CancellationToken cancellationToken)
        {
            using var tran = _customerRepository.BeginTransaction();

            var user = _mapper.Map<Models.Entities.User>(command);
            user.CreatedAt = DateTime.UtcNow;
            user.LanguageId = (int)Constants.DefaultLanguage;

            var createdUser = await _userManager.CreateAsync(user, command.Password);
            if (!createdUser.Succeeded)
            {
                var message = string.Join(Environment.NewLine, createdUser.Errors.Select(x => x.Description));
                throw new Exception(message);
            }

            var addRole = await _userManager.AddToRoleAsync(user, Enums.RoleTypes.Customer.ToString());
            if (!addRole.Succeeded)
            {
                var message = string.Join(Environment.NewLine, addRole.Errors.Select(x => x.Description));
                throw new Exception(message);
            }

            var company = _mapper.Map<Models.Entities.Company>(command);
            company.CreatedAt = DateTime.UtcNow;

            company.Address = _mapper.Map<Models.Entities.Address>(command);
            company.Address.CreatedAt = DateTime.UtcNow;

            var customer = _mapper.Map<Models.Entities.Customer>(command);
            customer.CreatedAt = DateTime.UtcNow;
            customer.UserId = user.Id;
            customer.Company = company;
            await _customerRepository.CreateAsync(customer);

            tran.Commit();

            return await _identityService.GenerateAuthenticationResultAsync(user);
        }
    }
}
