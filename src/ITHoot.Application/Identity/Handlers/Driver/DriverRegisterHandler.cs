﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITHoot.Application.Identity.Commands.Driver;
using ITHoot.Application.Identity.Responses;
using ITHoot.Application.Identity.Services.Interfaces;
using ITHoot.Application.User.Manager;
using ITHoot.Models;
using ITHoot.Models.Entities;
using ITHoot.Repository;
using MediatR;

namespace ITHoot.Application.Identity.Handlers.Driver
{
    public class DriverRegisterHandler : IRequestHandler<DriverRegistrationCommand, AuthenticationResult>
    {
        private readonly IMapper _mapper;
        private readonly IAppUserManager _userManager;
        private readonly IIdentityService _identityService;
        private readonly IRepository<Carrier, int> _carrierRepository;

        public DriverRegisterHandler(
            IMapper mapper,
            IAppUserManager userManager,
            IIdentityService identityService,
            IRepository<Carrier, int> carrierRepository)
        {
            _mapper = mapper;
            _identityService = identityService;
            _carrierRepository = carrierRepository;
            _userManager = userManager;
        }

        public async Task<AuthenticationResult> Handle(DriverRegistrationCommand request, CancellationToken cancellationToken)
        {
            using var tran = _carrierRepository.BeginTransaction();

            var user = _mapper.Map<Models.Entities.User>(request);
            user.CreatedAt = DateTime.UtcNow;
            user.LanguageId = (int)Constants.DefaultLanguage;

            var createdUser = await _userManager.CreateAsync(user, request.Password);
            if (!createdUser.Succeeded)
            {
                var message = string.Join(Environment.NewLine, createdUser.Errors.Select(x => x.Description));
                throw new Exception(message);
            }

            var addRole = await _userManager.AddToRolesAsync(user, new List<string>()
            {
                Enums.RoleTypes.NaturalPerson.ToString(),
                Enums.RoleTypes.Driver.ToString()
            });
            if (!addRole.Succeeded)
            {
                var message = string.Join(Environment.NewLine, addRole.Errors.Select(x => x.Description));
                throw new Exception(message);
            }

            var bank = _mapper.Map<Bank>(request);
            bank.CreatedAt = DateTime.UtcNow;

            var driver = _mapper.Map<Models.Entities.Driver>(request);
            driver.UserId = user.Id;
            driver.CreatedAt = DateTime.UtcNow;

            var company = _mapper.Map<Models.Entities.Company>(request);
            company.CreatedAt = DateTime.UtcNow;
            company.Address.CreatedAt = DateTime.UtcNow;

            var carrier = _mapper.Map<Carrier>(request);
            carrier.CreatedAt = DateTime.UtcNow;
            carrier.CarrierType = Enums.CarrierTypes.Driver;
            carrier.UserId = user.Id;
            carrier.Banks.Add(bank);
            carrier.Drivers.Add(driver);
            carrier.Company = company;
            await _carrierRepository.CreateAsync(carrier);

            tran.Commit();

            return await _identityService.GenerateAuthenticationResultAsync(user);
        }
    }
}