﻿using ITHoot.Application.Identity.Commands;
using ITHoot.Application.Identity.Responses;
using ITHoot.Application.Identity.Services.Interfaces;
using ITHoot.Application.User.Manager;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ITHoot.Application.Identity.Handlers
{
    public class MobileLoginHandler : IRequestHandler<MobileLoginCommand, AuthenticationResult>
    {
        private readonly IAppUserManager _userManager;
        private readonly IIdentityService _identityService;

        public MobileLoginHandler(
            IAppUserManager userManager,
            IIdentityService identityService)
        {
            _userManager = userManager;
            _identityService = identityService;
        }

        public async Task<AuthenticationResult> Handle(MobileLoginCommand request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByPhoneAsync(request.PhoneNumber);
            if (user == null)
            {
                return new AuthenticationResult
                {
                    Errors = new[] { "User does not exist" }
                };
            }

            var userHasValidPassword = await _userManager.CheckPasswordAsync(user, request.Password);
            if (!userHasValidPassword)
            {
                return new AuthenticationResult
                {
                    Errors = new[] { "Incorrect password" }
                };
            }

            return await _identityService.GenerateAuthenticationResultAsync(user);
        }
    }
}