﻿using System.Threading;
using System.Threading.Tasks;
using ITHoot.Application.Identity.Commands;
using ITHoot.Application.Identity.Services.Interfaces;
using MediatR;

namespace ITHoot.Application.Identity.Handlers
{
    public class RevokeHandler : IRequestHandler<RevokeCommand, bool>
    {
        private readonly ITokenService _tokenService;

        public RevokeHandler(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        public async Task<bool> Handle(RevokeCommand request, CancellationToken cancellationToken)
        {
            await _tokenService.RevokeRefreshTokensAsync(request.UserId);
            return true;
        }
    }
}